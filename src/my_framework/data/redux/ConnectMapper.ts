export function arrayConnectMapper(reducer: any, array: string, loading: string = "loading", error: string = "error"): object {
    const obj = {};
    obj[loading] = reducer.loading;
    obj[error] = reducer.error;
    obj[array] = reducer.array;
    return obj;
}

export function objectConnectMapper(reducer: any, object: string, loading: string = "loading", error: string = "error"): object {
    const obj = {};
    obj[loading] = reducer.loading;
    obj[error] = reducer.error;
    obj[object] = reducer.object;
    return obj;
}

export function successConnectMapper(reducer: any, success: string = "success", loading: string = "loading", error: string = "error"): object {
    const obj = {};
    obj[loading] = reducer.loading;
    obj[error] = reducer.error;
    obj[success] = reducer.success;
    return obj;
}