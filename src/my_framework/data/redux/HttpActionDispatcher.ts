import URLs from "../../../utils/helpers/URLs";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";

export default class HttpActionDispatcher {
    protected urlRoute: string;
    protected action: string;
    protected data: any;
    protected options: any;
    protected type: HttpMethod;

    constructor(type: HttpMethod, urlRoute: string, action: string, data: any = {}, options: any = {}) {
        this.urlRoute = urlRoute;
        this.action = action;
        this.data = data;
        this.options = options;
        this.type = type;
    }

    public dispatch(dispatchFunc: (action: any) => void) {
        const url = URLs.getApiUrl(this.urlRoute);
        let action: any;
        if (this.type === HttpMethod.GET) {
            action = HttpGetAction(this.action, url, this.data, this.options);
        } else {
            action = HttpPostAction(this.action, url, this.data, this.options);
        }
        dispatchFunc(action);
    }

    static dispatch(dispatchFunc: (action: any) => void, type: HttpMethod, urlRoute: string, action: string, data: any = {}, options: any = {}) {
        const dispatcher = new HttpActionDispatcher(type, urlRoute, action, data, options);
        dispatcher.dispatch(dispatchFunc);
    }
}