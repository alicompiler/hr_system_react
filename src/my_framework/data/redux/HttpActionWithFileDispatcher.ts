import URLs from "../../../utils/helpers/URLs";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import HttpActionDispatcher from "./HttpActionDispatcher";

export default class HttpActionWithFileDispatcher extends HttpActionDispatcher {

    constructor(urlRoute: string, action: string, data: FormData, options: any = {}) {
        super(HttpMethod.POST, urlRoute, action, data);
        const headers = {headers: {"Content-Type": "multipart/form-data"}};
        this.options = {...options, ...headers};
        this.urlRoute = urlRoute;
        this.action = action;
        this.data = data;
        this.options = options;
    }

    public dispatch(dispatcher: (action: any) => void) {
        const url = URLs.getApiUrl(this.urlRoute);
        const action = HttpPostAction(this.action, url, this.data, this.options);
        dispatcher(action);
    }

    static dispatchWithFile(dispatchFunc: (action: any) => void, urlRoute: string, action: string, data: any = {}, options: any = {}) {
        const dispatcher = new HttpActionWithFileDispatcher(urlRoute, action, data, options);
        dispatcher.dispatch(dispatchFunc);
    }
}