import * as React from "react";
import {Switch} from "react-router-dom";


export default abstract class RouterComponent extends React.Component {

    render() {
        const routes = this.routes();
        return (
            <Switch>
                {
                    routes.map(this.renderRoute)
                }
            </Switch>
        )
    }

    private renderRoute = (RouteItem: any, index: number) => {
        return <RouteItem key={index}/>
    };

    protected abstract routes(): any[];

}

