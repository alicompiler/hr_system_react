import * as React from "react";
import {Route, Switch} from "react-router-dom";


export default abstract class ExactPathRouterComponent extends React.Component {

    render() {
        const routes = this.routes();
        const paths = Object.keys(routes);
        return (
            <Switch>
                {
                    paths.map(this.renderRoute)
                }
            </Switch>
        )
    }

    private renderRoute = (path: string, index: number) => {
        const component = this.routes()[path];
        const func = typeof component === "function" ? component : () => component;
        return <Route key={index} exact path={path} component={func}/>
    };

    protected abstract routes(): object;

}

