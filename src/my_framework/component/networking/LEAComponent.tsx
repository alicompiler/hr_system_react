import * as React from "react";
import {LinearProgress, Typography} from "@material-ui/core";

export interface LEAComponentProps {
    loading: boolean;
    error: boolean;
    dispatch: (action: any) => void;
}

export default abstract class LEAComponent<T extends LEAComponentProps> extends React.Component<T> {

    componentWillMount() {
        this.dispatchComponentOnWillMount();
    }

    protected dispatchComponentOnWillMount() {
    };

    render() {
        if (this.isLoading()) {
            return this.renderLoading();
        } else if (this.isError()) {
            return this.renderError();
        }
        if (this.shouldRenderNullBeforeRenderContent()) {
            return null;
        }
        return this.renderContent();
    }

    protected renderLoading = () => {
        return <LinearProgress color={"primary"}/>
    };

    protected renderError = () => {
        return <Typography variant={"subheading"}>Error Happened</Typography>
    };

    protected shouldRenderNullBeforeRenderContent = () => {
        return false;
    };

    protected isLoading(): boolean {
        return this.props.loading;
    }

    protected isError(): boolean {
        return this.props.error;
    }

    protected abstract renderContent(): any;
}