import * as React from "react";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import {LinearProgress, Typography} from "@material-ui/core";

export interface LESComponentProps {
    loading: boolean;
    error: boolean;
    success: boolean;
    dispatch: (action: any) => void;
}

export default abstract class LESComponent<T extends LESComponentProps> extends React.Component<T> {

    static defaultProps = {object: {}};

    componentWillMount() {
        this.dispatchComponentOnWillMount();
    }

    protected dispatchComponentOnWillMount() {
    };

    componentDidUpdate() {
        const props = this.getPropsForSuccessStateActionHandler();
        const handler = new SuccessStateActionHandler(props);
        this.setupSuccessStateActionHandler(handler);
        handler.handleSuccessResponse();
    }

    protected getPropsForSuccessStateActionHandler(): any {
        return this.props;
    };

    // noinspection JSUnusedLocalSymbols : this method could be override and handler will be used
    protected setupSuccessStateActionHandler(handler: SuccessStateActionHandler): any {
    };

    render() {
        if (this.isLoading()) {
            return this.renderLoading();
        } else if (this.isError()) {
            return this.renderError();
        }
        if (this.shouldRenderNullBeforeRenderContent()) {
            return null;
        }
        return this.renderContent();
    }

    protected renderLoading = () => {
        return <LinearProgress color={"primary"}/>
    };

    protected renderError = () => {
        return <Typography variant={"subheading"}>Error Happened</Typography>
    };

    protected shouldRenderNullBeforeRenderContent = () => {
        return false;
    };

    protected isLoading(): boolean {
        return this.props.loading;
    }

    protected isError(): boolean {
        return this.props.error;
    }

    protected abstract renderContent(): any;
}