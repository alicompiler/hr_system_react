import * as React from "react";
import {Button, Grid, IconButton} from "@material-ui/core";
import {Link} from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import ViewListIcon from "@material-ui/icons/ViewList";
import TextField from "../../../utils/ui/components/form/components/TextField";

interface Props {
    changeDisplayType?: boolean;
    onChangeDisplayType?: (type: string) => void;
    onSearch?: (query: string) => void;
    action?: boolean;
    actionTitle?: string;
    actionLink?: string;
}

export default class ActionBar extends React.Component<Props> {
    static defaultProps: Props;

    render() {
        return (
            <Grid container spacing={16}>

                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                    {
                        this.props.onSearch &&
                        <TextField
                            md={12} sm={12} lg={12}
                            adornment={<SearchIcon/>} adornmentPosition={"start"}
                            placeholder={"بحث..."} name={"search"} onEnter={this.props.onSearch}
                        />
                    }
                </Grid>

                <Grid item xs={12} sm={12} md={6} lg={6} xl={6} style={{textAlign: "left"}}>
                    <div className="search-bar-actions">
                        {
                            this.props.changeDisplayType &&
                            <>
                                <IconButton onClick={this.onSetDisplayTypeToList}><ViewListIcon/></IconButton>
                                <IconButton onClick={this.onSetDisplayTypeToGrid}><ViewModuleIcon/></IconButton>
                            </>
                        }
                        {
                            this.props.action &&
                            <Link to={(this.props.actionLink!)}>
                                <Button variant={"contained"} color={"primary"}>{this.props.actionTitle}</Button>
                            </Link>
                        }
                    </div>
                </Grid>

            </Grid>
        );
    }

    private onSetDisplayTypeToGrid = () => {
        this.props.onChangeDisplayType && this.props.onChangeDisplayType("grid")
    };

    private onSetDisplayTypeToList = () => {
        this.props.onChangeDisplayType && this.props.onChangeDisplayType("list")
    };
}


ActionBar.defaultProps = {
    actionTitle: "اضافة جديد"
};