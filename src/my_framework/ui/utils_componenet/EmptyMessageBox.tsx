import * as React from "react";
import {Paper} from "@material-ui/core";

interface Props {
    message?: string;
}

export default class EmptyMessageBox extends React.Component<Props> {

    static defaultProps = {message: "لا توجد بيانات"};

    render() {
        return (
            <Paper style={{padding: 16, textAlign: "center", backgroundColor: "#ffd740"}}>{this.props.message}</Paper>
        )
    }
}