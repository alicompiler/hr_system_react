import {Avatar} from "@material-ui/core";
import * as React from "react";

interface Props {
    image: string;
    title: string;
    subtitle?: string;
}

export default class AvatarWithTitle extends React.Component<Props> {
    render() {
        return (
            <div className="avatar-with-title">
                <Avatar src={this.props.image}/>
                <div className="title-container">
                    <span className="title">{this.props.title}</span>
                    <span className="subtitle">{this.props.subtitle}</span>
                </div>
            </div>
        )
    }
}