import * as React from "react";
import {IconButton, ListItem, ListItemText} from "@material-ui/core";
import {CloudDownload} from "@material-ui/icons";
import DeleteIcon from "@material-ui/icons/Delete";

interface Props {
    file: any;
    loading?: boolean;
    showDelete?: boolean;
    onDelete?: (value: any) => void;
    onDownload: (value: any) => void;
}


export default class FileListItem extends React.Component<Props> {

    static defaultProps = {showDelete: false};

    render() {
        return (
            <ListItem>
                <ListItemText>
                    <IconButton component={"a"} onClick={() => this.props.onDownload(this.props.file)}>
                        <CloudDownload/>
                    </IconButton>
                    {
                        this.props.showDelete &&
                        <IconButton disabled={this.props.loading} component={"a"}
                                    onClick={() => this.props.onDelete!(this.props.file)}>
                            <DeleteIcon/>
                        </IconButton>
                    }
                    {this.props.file.filename}
                </ListItemText>
            </ListItem>
        )
    }

}