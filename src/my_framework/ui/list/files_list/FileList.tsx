import * as React from "react";
import FileListItem from "./FileListItem";
import {List} from "@material-ui/core";

interface Props {
    files: any[];
    extractFile?: (item: any) => any;
    onDownload: (file: any) => void;
    onDelete: (file: any, index: number) => void;
    loading?: boolean;
    showDelete?: boolean;
}

export default class FileList extends React.Component<Props> {

    static defaultProps: any;

    render() {
        return (
            <List>
                {
                    this.props.files.map((item: any, index: number) => {
                        const file = this.props.extractFile!(item);
                        return <FileListItem key={index} file={file} loading={this.props.loading}
                                             showDelete={this.props.showDelete}
                                             onDownload={this.props.onDownload}
                                             onDelete={() => this.props.onDelete(file, index)}/>
                    })
                }
            </List>
        )
    }
}

// noinspection JSUnusedGlobalSymbols
FileList.defaultProps = {
    extractFile: (file: any) => file
};
