export enum AcceptanceStatus {
    WAITING = 1,
    DONE,
    REJECTED
}