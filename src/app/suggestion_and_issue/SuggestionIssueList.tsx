import * as React from "react";
import {Card, IconButton, List, Typography} from "@material-ui/core";
import AvatarWithTitle from "../../my_framework/ui/utils_componenet/AvatarWithTitle";
import URLs, {API_ROUTES} from "../../utils/helpers/URLs";
import {Delete} from "@material-ui/icons";
import HttpActionDispatcher from "../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../lib/redux++/action/HttpMethod";
import {Actions} from "../../bootstrap/actions";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../lib/redux++/action/HttpResetAction";
import {RemoveItemAtIndexFetchArrayAction} from "../../lib/redux++/action/FetchArrayActions";
import Session from "../../utils/helpers/Session";

interface Props {
    items: any[];
    loading: boolean;
    error: boolean;
    success: boolean;
    dispatch: (action: any) => void;
}

class SuggestionIssueList extends React.Component<Props> {
    private currentIndex = -1;

    componentDidUpdate(): void {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.DELETE_SUGGESTION));
        handler.afterSuccess = () => {
            if (this.currentIndex == -1) {
                return;
            }
            this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.FETCH_RECENT_SUGGESTION_OR_ISSUE, this.currentIndex));
        };
        handler.handleSuccessResponse();
    }

    render() {
        const session = Session.getUserObject();
        return <List>
            {
                this.props.items.map((item, index) => {
                    return <Card key={index} style={{padding: 16, marginBottom: 16}}>
                        <div style={{display: 'flex', justifyContent: 'space-between'}}>
                            <AvatarWithTitle image={URLs.getImage("profile_images", item.image)}
                                             subtitle={item.time}
                                             title={item.name}/>
                            <div>
                                {
                                    session.employeeId == item.employee_id &&
                                    <IconButton onClick={() => this.delete(item.id, index)}>
                                        <Delete/>
                                    </IconButton>
                                }
                            </div>
                        </div>
                        <br/>
                        <Typography style={{marginBottom: 8, whiteSpace: 'pre-line'}}
                                    variant={"body1"}>{item.message}</Typography>
                    </Card>
                })
            }
        </List>
    }

    private delete = (id: any, index: number) => {
        this.currentIndex = index;
        const data = {suggestionId: id};
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.SUGGESTION_AND_ISSUE.DELETE,
            Actions.DELETE_SUGGESTION, data).dispatch(this.props.dispatch);
    }
}

export default connect((store: any) => {
    return {
        loading: store.DeleteSuggestion.loading,
        error: store.DeleteSuggestion.error,
        success: store.DeleteSuggestion.success
    }
})(SuggestionIssueList)