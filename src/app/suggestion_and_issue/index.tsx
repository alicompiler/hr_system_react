import React from "react";
import {Button, Divider, LinearProgress, Typography} from "@material-ui/core";
import Form from "../../utils/ui/components/form/Form";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../utils/other/SuccessStateActionHandler";
import TextField from "../../utils/ui/components/form/components/TextField";
import HttpResetAction from "../../lib/redux++/action/HttpResetAction";
import {Actions} from "../../bootstrap/actions";
import HttpActionDispatcher from "../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../utils/helpers/URLs";
import SuggestionIssueList from "./SuggestionIssueList";

interface Props {
    loading: boolean;
    error: boolean;
    success: boolean;
    loadingItems: boolean;
    errorLoadingItems: boolean;
    items: any[];
    dispatch: (action: any) => void;
}

class SuggestionAndIssue extends Form<Props> {

    componentWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET, API_ROUTES.SUGGESTION_AND_ISSUE.recent, Actions.FETCH_RECENT_SUGGESTION_OR_ISSUE)
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.SEND_SUGGESTION_OR_ISSUE));
            this.clearValues();
        };
        handler.handleSuccessResponse()
    }

    render() {
        return <div>
            <Typography variant={"headline"}>هل لديك اقتراحات للنظام ؟ هل تواجهه مشكلة ؟</Typography>
            <Divider/>
            <TextField
                ref={(ref: any) => this.pushElementRef(ref)}
                name={"content"}
                placeholder={"رسالة"}
                xs={12} md={12} lg={12}
                multiline rows={6}
                validationRules={{length: {minimum: 10}}}
            />
            <Button disabled={this.props.loading} onClick={this.onSave} variant={"contained"}
                    color={"primary"}>ارسال</Button>
            <br/><br/>
            {
                this.props.loading && <LinearProgress color={"secondary"}/>
            }

            <br/><br/>
            {
                (!this.props.loadingItems && this.props.items.length > 0) &&
                <>
                    <Divider/><br/>
                    <SuggestionIssueList items={this.props.items}/>
                    <br/>
                </>
            }
        </div>
    }

    private onSave = () => {
        if (!this.validate()) {
            return;
        }
        HttpActionDispatcher
            .dispatch(this.props.dispatch, HttpMethod.POST, API_ROUTES.SUGGESTION_AND_ISSUE.send, Actions.SEND_SUGGESTION_OR_ISSUE, this.getValues())
    }
}

export default connect((store: any) => {
    return {
        loading: store.SuggestionAndIssue.loading,
        error: store.SuggestionAndIssue.error,
        success: store.SuggestionAndIssue.success,
        loadingItems: store.RecentSuggestionAndIssue.loading,
        errorLoadingItems: store.RecentSuggestionAndIssue.error,
        items: store.RecentSuggestionAndIssue.array
    }
})(SuggestionAndIssue)