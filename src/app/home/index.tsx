import * as React from "react";
import HomeAudienceInfo from "./HomeAudienceInfo";
import {connect} from "react-redux";
import URLs, {API_ROUTES} from "../../utils/helpers/URLs";
import {HttpGetAction, HttpPostAction} from "../../utils/redux++wrapper/Actions";
import {Actions} from "../../bootstrap/actions";
import RecentAnnouncementsList from "./RecentAnnouncementsList";
import {ReduxStore} from "../../bootstrap/store";
import Loading from "../components/Loading";
import {Paper, Typography} from "@material-ui/core";

interface Props {
    loadingAudience: boolean;
    errorAudience: boolean;
    audienceInfo: any;
    shifts: any[];
    loadingAnnouncements: boolean;
    errorAnnouncements: boolean;
    announcements: any[];
    dispatch: (action: any) => void;
    loadingProfileInfo: boolean;
    errorProfileInfo: boolean;
    profileInfo: any;
}

class Home extends React.Component<Props> {


    componentDidMount() {
        this.dispatchFetchAudienceInfoAction();
        this.dispatchFetchRecentAnnouncementAction();
        this.dispatchFetchMyProfileInfoAction();
    }

    private dispatchFetchAudienceInfoAction() {
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE.MY_AUDIENCE);
        const action = HttpGetAction(Actions.FETCH_MY_AUDIENCE, url, {});
        this.props.dispatch(action);
    }

    private dispatchFetchRecentAnnouncementAction = () => {
        const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.RECENT_FOR_HOME_PAGE);
        const action = HttpGetAction(Actions.FETCH_RECENT_ANNOUNCEMENTS_FOR_HOME_PAGE, url);
        this.props.dispatch(action);
    };

    private dispatchFetchMyProfileInfoAction = () => {
        const url = URLs.getApiUrl(API_ROUTES.PROFILE.MY_INFO);
        const action = HttpGetAction(Actions.FETCH_MY_PROFILE, url);
        this.props.dispatch(action);
    };

    render() {
        if (this.props.loadingProfileInfo || this.props.loadingAnnouncements || this.props.loadingAudience) {
            return <Loading loading={true} message={"جاري تحميل البيانات..."}/>
        }
        return (
            <div>
                {
                    this.props.profileInfo.isBirthday ?
                        <div style={{textAlign: 'center'}}>
                            <Typography align={"center"} variant={"headline"}>يوم ميلاد سعيد</Typography>
                            <br/>
                            <img style={{width: "80%", maxWidth: 480}} alt={""} src={"/images/happy_birthday.svg"}/>
                        </div>
                        :
                        <div style={{textAlign: 'center'}}>
                            <Typography align={"center"} variant={"headline"}>مرحبا</Typography>
                            <br/>
                            <img style={{width: "80%", maxWidth: 480}} alt={""} src={"/images/welcome"}/>
                        </div>
                }
                <br/><br/>
                {
                    this.renderEmployeesWithBirthDate()
                }
                <HomeAudienceInfo shifts={this.props.shifts} audienceInfo={this.props.audienceInfo}/>
                <br/><br/>
                <RecentAnnouncementsList announcements={this.props.announcements}
                                         onAnnouncementVisible={this.onAnnouncementVisible}/>
            </div>
        )
    }

    onAnnouncementVisible = (announcementId: number, firstTime: boolean, visibility: boolean, alreadyWatched: boolean) => {
        if (firstTime && !alreadyWatched) {
            const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.WATCH);
            const data = {announcementId: announcementId};
            this.props.dispatch(HttpPostAction(Actions.SET_ANNOUNCEMENT_TO_BE_WATCHED, url, data));
        }
    };

    private renderEmployeesWithBirthDate = () => {
        if (this.props.profileInfo.employeesWithBirthDate) {
            if (this.props.profileInfo.employeesWithBirthDate.length == 1) {
                return <Paper style={{padding: 16}}>
                    يصادف اليوم ميلاد
                    {' ' + this.props.profileInfo.employeesWithBirthDate[0].name}
                    <br/>
                    كل عام وانت بخير
                </Paper>
            } else if (this.props.profileInfo.employeesWithBirthDate.length > 1) {
                let employees = '';
                this.props.profileInfo.employeesWithBirthDate
                    .map((emp: any) => {
                        employees = employees + emp.name + " , "
                    });
                return <Paper style={{padding: 16}}>
                    يصادف اليوم ميلاد
                    {' ' + employees.substring(0, employees.length - 2)}
                    <br/>
                    كل عام و انتم بخير
                </Paper>
            }
        }
        return null;
    }

}

export default connect((store: ReduxStore) => {
    return {
        loadingAudience: store.MyAudience.loading,
        errorAudience: store.MyAudience.error,
        audienceInfo: store.MyAudience.object.audience,
        shifts: store.MyAudience.object.shifts,

        loadingAnnouncements: store.HomeAnnouncements.loading,
        errorAnnouncements: store.HomeAnnouncements.error,
        announcements: store.HomeAnnouncements.array,

        loadingProfileInfo: store.MyProfile.loading,
        errorProfileInfo: store.MyProfile.error,
        profileInfo: store.MyProfile.object,
    }
})(Home)