import * as React from "react";
import {Divider, Grid, Typography} from "@material-ui/core";
import TextField from "../../utils/ui/components/form/components/TextField";

interface Props {
    shifts: any[];
    audienceInfo: any;
}

export default class HomeAudienceInfo extends React.Component<Props> {

    render() {
        if (!this.props.audienceInfo) {
            return null;
        }

        return (
            <div>
                <Typography variant={"subheading"}>معلومات الحظور و الغياب ليوم امس : </Typography>
                <Divider/>
                <br/>

                {
                    this.renderAudienceInfo()
                }
                <br/>
                {
                    this.renderShifts()
                }

            </div>
        )
    }

    private renderAudienceInfo = (): any => {
        return <div>
            <TextField
                label={"التاريخ"}
                value={this.props.audienceInfo.date}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"دقائق اضافية"}
                value={this.props.audienceInfo.overtime}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"دقائق التاخير"}
                value={this.props.audienceInfo.latetime}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"الغيابات"}
                value={this.props.audienceInfo.absenceShift}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            {
                this.props.audienceInfo.absenceShift > 0 &&
                <TextField
                    label={"الغيابات (عدد الشفتات)"}
                    value={this.props.audienceInfo.absenceShift}
                    xs={12} sm={12} md={6} lg={6}
                    readOnly
                />
            }
        </div>;
    };

    private renderShifts = (): any => {
        return (
            <div>
                <Typography variant={"subheading"}>البصمات : </Typography>
                {
                    this.props.shifts.map((item: any, index: number) =>
                        <Grid key={index} container spacing={16}>
                            <TextField
                                label={"الدخول"}
                                value={item.login}
                                xs={12} sm={12} md={6} lg={6}
                                readOnly
                            />
                            <TextField
                                label={"الخروج"}
                                value={item.logout}
                                xs={12} sm={12} md={6} lg={6}
                                readOnly
                            />
                        </Grid>
                    )
                }
            </div>
        )
    }
}