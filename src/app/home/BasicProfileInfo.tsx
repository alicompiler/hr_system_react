import * as React from "react";
import TextField from "../../utils/ui/components/form/components/TextField";
import {Button, Divider, Grid, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

interface Props {
    info: any;
}

export default class BasicProfileInfo extends React.Component<Props> {

    render() {
        return (
            <div>
                <Typography variant={"subheading"}>معلومات حسابي : </Typography>
                <Divider/>
                <br/>
                <Grid container spacing={16}>
                    <TextField
                        label={"الاسم الكامل"}
                        value={this.props.info.name}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                    <TextField
                        label={"الهاتف"}
                        value={this.props.info.phone}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                    <TextField
                        label={"البريد الالكتروني"}
                        value={this.props.info.email}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                    <TextField
                        label={"العنوان الوضيفي"}
                        value={this.props.info.jobTitle}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                    <TextField
                        label={"القسم"}
                        value={this.props.info.department}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                </Grid>
                <br/>
                <Link to={"/profile"}>
                    <Button variant={"contained"} color={"primary"}>بروفايلي</Button>
                </Link>
            </div>
        )
    }

}