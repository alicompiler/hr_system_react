import * as React from "react";
import {Divider, Typography} from "@material-ui/core";
import AnnouncementList from "../announcement/AnnouncementList/AnnouncementList";

interface Props {
    announcements: any[];
    onAnnouncementVisible: (id: number, firstTime: boolean, visibility: boolean, alreadyWatched: boolean) => void;
}

export default class RecentAnnouncementsList extends React.Component<Props> {

    render() {
        if (!this.props.announcements || this.props.announcements.length == 0) {
            return null;
        }

        return (
            <div>
                <Typography variant={"subheading"}>اخر الاعلانات : </Typography>
                <Divider/>
                <br/>
                <AnnouncementList announcements={this.props.announcements}
                                  onVisible={this.props.onAnnouncementVisible}/>
            </div>
        )
    }
}