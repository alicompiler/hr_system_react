import {ReactElement} from "react";

export interface IMainModelManager {
    title?: string;

    mainLoadUrl?: string;
    search?: boolean;
    searchUrl?: string;

    displayTypeOptions?: boolean;
    changeDisplayTypeOnResize?: boolean;
    defaultDisplayType?: "gird" | "table";

    createNewButton?: boolean;
    createNewRoute?: string;

    gridItemRender?: (item: any, index: number) => ReactElement<any>;

    tableColumns?: any[];
    tableOptions?: any;
    tableDataItemToArray?: (item: any) => any[];
    tableTitle?: string;

    onRowSelected?: (data: any) => void;

    mainAction?: string;
    searchAction?: string;
    reducerName?: string;

}