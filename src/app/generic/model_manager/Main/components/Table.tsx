import "../../../../../styles/common/table_and_grid.css"
import MUITableComponent from "../../../../../utils/ui/components/table/mui-table/MUITableComponent";
import Loading from "../../../../components/Loading";
import Error from "../../../../components/Error";
import * as React from "react";


interface Props {
    data: any[][];
    loading: boolean;
    onRowSelected: (data: any[]) => void;
    error?: boolean | null;
    columns: any[];
    options: any;
    title: string;
    pointerCursor?: boolean;
    itemToArrayCallback: (item: any) => any[];
}

export default class Table extends MUITableComponent<Props> {

    static defaultProps: any = {
        pointerCursor: true
    };

    render() {
        if (this.props.loading) {
            return <Loading loading message={"جاري تحميل البيانات..."}/>
        } else if (this.props.error) {
            return <Error message={"هناك خطا ، تحقق  من الاتصال بالانترنت"}/>
        }
        return super.render();
    }

    getColumns(): any[] {
        // noinspection JSUnusedGlobalSymbols
        return this.props.columns;
    };

    getData(): any[][] {
        return this.props.data.map((item: any) => this.props.itemToArrayCallback(item));
    };

    getOptions(): object {
        let options = super.getOptions();
        options["onRowClick"] = (data: any) => {
            this.props.onRowSelected(data);
        };
        options = {...options, ...this.props.options};
        return options;
    }

    isPointerCursor(): boolean {
        return this.props.pointerCursor!;
    }

    getTitle(): string {
        return this.props.title;
    }


}