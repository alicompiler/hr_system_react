import * as React from "react";
import {ReactElement} from "react";
import {Grid as MaterialGrid} from "@material-ui/core";
import Loading from "../../../../components/Loading";
import Error from "../../../../components/Error";


interface Props {
    data: any[];
    loading: boolean;
    error?: boolean | null;
    gridItemRender?: (item: any, index: number) => ReactElement<any>
}

export default class Grid extends React.Component<Props> {
    render() {
        if (this.props.loading) {
            return <Loading loading message={"جاري تحميل البيانات..."}/>
        } else if (this.props.error) {
            return <Error message={"هناك خطا ، تحقق  من الاتصال بالانترنت"}/>
        }

        return (
            <MaterialGrid spacing={16} container>
                {
                    this.props.data.map((item, index) => {
                        return <MaterialGrid item key={index} xs={12} sm={6} md={4} lg={4} xl={4}>
                            {
                                this.props.gridItemRender && this.props.gridItemRender(item, index)
                            }
                        </MaterialGrid>
                    })
                }
            </MaterialGrid>
        )
    }
}