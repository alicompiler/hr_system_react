import * as React from "react";

import {getScreenSizeType} from "../../../../utils/helpers/ScreenSizeUtils";
import {connect} from "react-redux";
import {HttpGetAction} from "../../../../utils/redux++wrapper/Actions";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import ActionBar from "../../../components/ActionBar";
import {RouteComponentProps} from "react-router";
import {IMainModelManager} from "../ModelManager";
import Header from "../../../components/Header";
import Grid from "./components/Grid";
import Table from "./components/Table";

import "./../../../../styles/common/table_and_grid.css";
import "./../../../../styles/common/search_bar.css";

interface State {
    displayType: "grid" | "table";
}

interface Props extends IMainModelManager, NetworkingState {
    items: any[];
    route?: RouteComponentProps;
    dispatch: (action: any) => void;
}

class Main extends React.Component<Props, State> {

    static defaultProps: Props;

    constructor(props: Props) {
        super(props);
        this.state = {displayType: (props.defaultDisplayType as "grid" | "table")}
    }

    componentWillMount() {
        const url = this.props.mainLoadUrl as string;
        this.props.dispatch(HttpGetAction(this.props.mainAction as string, url));
        window.addEventListener("resize", this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize)
    }

    onResize = () => {
        if (this.props.changeDisplayTypeOnResize)
            this.changeDisplayType(this.state.displayType);
    };

    changeDisplayType = (type: "grid" | "table") => {
        if (getScreenSizeType() === "mobile" || getScreenSizeType() === "tablet") {
            type = "grid";
        }
        this.setState({displayType: type})
    };

    render() {
        return (
            <div>
                <Header title={this.props.title as string}/>
                <ActionBar
                    action={this.props.createNewButton}
                    actionLink={this.props.createNewRoute}
                    changeDisplayType={this.props.displayTypeOptions} onChangeDisplayType={this.changeDisplayType}
                    onSearch={this.props.search ? this.onSearch : undefined}
                />
                <br/><br/>
                {
                    this.state.displayType === "grid" ? this.grid() : this.table()
                }

            </div>
        );
    }

    table = () => {
        return (
            <Table onRowSelected={this.props.onRowSelected!} loading={this.props.loading}
                   error={this.props.error}
                   data={this.props.items}
                   columns={this.props.tableColumns as any[]}
                   options={this.props.tableOptions}
                   itemToArrayCallback={this.props.tableDataItemToArray as (item: any) => any[]}
                   title={this.props.title as string}/>
        )
    };

    grid = () => {
        return (
            <Grid loading={this.props.loading}
                  error={this.props.error} data={this.props.items}
                  gridItemRender={this.props.gridItemRender}/>
        )
    };


    private onSearch = (query: string) => {
        const url = this.props.searchUrl as string;
        this.props.dispatch(HttpGetAction(this.props.searchAction!, url, {query: query}));
    };

}

Main.defaultProps = {
    items: [],
    displayTypeOptions: true,
    changeDisplayTypeOnResize: true,
    search: true,
    defaultDisplayType: "table",
    createNewButton: true,
    dispatch: () => undefined,
    loading: false,
    error: null,
    tableOptions: {},

    mainAction: "__GENERIC_MAIN_FETCH",
    searchAction: "__GENERIC_MAIN_FETCH",
    reducerName: "__GENERIC_REDUCER",
};

export default connect((store: any, props: any) => {
    const REDUCER = props.reducerName ? props.reducerName : "__GENERIC_REDUCER";
    return {
        loading: store[REDUCER].loading,
        error: store[REDUCER].error,
        items: store[REDUCER].array
    };
})(Main);