import * as React from "react";
import { Button } from "@material-ui/core";

export default class RulesMain extends React.Component {


    render() {
        return <div style={{ maxWidth: 400 }}>

            <Button onClick={() => {
                const url = "/files/blog.pdf";
                window.open(url, '_blank');
            }} variant={"contained"} fullWidth color={"primary"}>مدونة السلوك الوظيفي</Button>

            <br /><br />

            <Button onClick={() => {
                const url = "/files/leave.pdf";
                window.open(url, '_blank');
            }} variant={"contained"} fullWidth color={"primary"}>قانون الاجازات</Button>



            <br /><br />

            <Button onClick={() => {
                const url = "/files/dispatch.pdf";
                window.open(url, '_blank');
            }} variant={"contained"} fullWidth color={"primary"}>قانون الايفاد</Button>



            <br /><br />

            <Button onClick={() => {
                const url = "/files/social.pdf";
                window.open(url, '_blank');
            }} variant={"contained"} fullWidth color={"primary"}>قانون السوشل ميديا</Button>

            <br /> <br />

            <Button onClick={() => {
                const url = "/files/covid-19.pdf";
                window.open(url, '_blank');
            }} variant={"contained"} fullWidth color={"primary"}>قانون COVID-19</Button>

        </div>
    }


}