import * as React from "react";
import {Route, Switch} from "react-router";
import CreateProjectIdeaForm from "./Form";
import Session from "../../utils/helpers/Session";
import ProjectIdeaListContainer from "./List/ProjectIdeaListContainer";


interface Props {
}

export default class ProjectIdea extends React.Component<Props> {
    render() {
        const session = Session.getUserObject();
        return (
            <Switch>
                {
                    session.userType === "admin" ?
                        <Route exact path={"/project-idea"}
                               component={() => <ProjectIdeaListContainer reducer={"ProjectIdeas"}/>}/>
                        :
                        <Route exact path={"/project-idea"}
                               component={() => <ProjectIdeaListContainer reducer={"MyProjectIdeas"}/>}/>
                }
                <Route exact path={"/project-idea/create"} component={() => <CreateProjectIdeaForm/>}/>
            </Switch>
        )
    }
}