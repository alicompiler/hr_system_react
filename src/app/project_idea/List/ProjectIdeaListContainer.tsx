import * as React from "react";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import ProjectIdeaList from "./ProjectIdeaList";
import {Button, LinearProgress, Typography} from "@material-ui/core";
import ErrorIcon from "@material-ui/core/SvgIcon/SvgIcon";
import EmptyList from "../../components/EmptyList";
import Session from "../../../utils/helpers/Session";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import Header from "../../components/Header";
import {Link} from "react-router-dom";

interface Props {
    reducer: string;
    loading: boolean;
    error: boolean;
    ideas: any[];
    showLoading?: boolean;
    showError?: boolean;
    loadingMessage?: string;
    errorMessage?: string;
    showEmpty?: boolean;
    emptyMessage?: string;
    dispatch: (action: any) => void;
}

class ProjectIdeaListContainer extends React.Component<Props> {
    static defaultProps = {
        showLoading: false,
        showError: false,
        loadingMessage: 'جاري تحميل البيانات...',
        errorMessage: 'حصلت مشكلة خلال تحميل البيانات',
        showEmpty: false,
        emptyMessage: 'لا توجد اي نتائج'
    };

    componentDidMount() {
        const session = Session.getUserObject();
        if (session.userType === "admin") {
            new HttpActionDispatcher(HttpMethod.GET, API_ROUTES.PROJECT_IDEA.ALL, Actions.FETCH_PROJECT_IDEAS)
                .dispatch(this.props.dispatch)
        } else {
            new HttpActionDispatcher(HttpMethod.GET, API_ROUTES.PROJECT_IDEA.MINE, Actions.FETCH_MY_PROJECT_IDEAS)
                .dispatch(this.props.dispatch)
        }
    }

    render() {
        return <div>
            <Header title={"المقترحات و الافكار الموجهه الى الادارة"}/>
            <div style={{textAlign: 'left'}}>
                <Link to={"/project-idea/create"}>
                    <Button variant={"contained"} color={"primary"}>
                        كتابة مقترح او فكرة جديدة
                    </Button>
                </Link>
            </div>
            {this.renderContent()}
        </div>
    }

    renderContent = () => {
        if (this.props.loading && this.props.showLoading) {
            return <div>
                <LinearProgress color={"primary"}/>
                <Typography variant={"subheading"}>{this.props.loadingMessage}</Typography>
            </div>
        } else if (this.props.error && this.props.showError) {
            return <div>
                <div style={{textAlign: 'center'}}>
                    <ErrorIcon style={{fontSize: 24, color: '#E00'}}/>
                    <Typography variant={"subheading"}>{this.props.errorMessage}</Typography>
                </div>
            </div>
        } else {
            if (this.props.ideas.length == 0 && this.props.showEmpty) {
                return <EmptyList message={this.props.emptyMessage}/>
            }
            return (
                <ProjectIdeaList ideas={this.props.ideas}/>
            )
        }
    }
}

export default connect((store: ReduxStore, props: any) => {
    return {
        loading: store[props.reducer].loading,
        error: store[props.reducer].error,
        ideas: store[props.reducer].array
    }
})(ProjectIdeaListContainer)