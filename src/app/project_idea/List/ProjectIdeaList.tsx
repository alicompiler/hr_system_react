import * as React from "react";
import ProjectIdeaItem from "./ProjectIdea/ProjectIdeaItem";

interface Props {
    ideas: any[];
}

export default class ProjectIdeaList extends React.Component<Props> {

    render() {
        return (
            <div>
                {
                    this.props.ideas.map((item: any, index: number) => {
                        return <ProjectIdeaItem id={item.id}
                                                key={index}
                                                employeeId={item.employee_id}
                                                description={item.description}
                                                date={item.date}
                                                employeeName={item.employeeName}
                                                employeeImage={item.employeeImage}
                                                replay={item.replay}
                                                replayDate={item.replayDate}
                                                file={item.file}/>
                    })
                }
            </div>
        )
    }

}