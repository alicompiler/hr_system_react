import * as React from "react";
import AvatarWithTitle from "../../../components/AvatarWithTitle";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Button, Grid, IconButton, Paper, Typography} from "@material-ui/core";
import ProjectIdeaReplayFormContainer from "./ReacIdeaReplayContainer";
import DeleteIcon from "@material-ui/icons/Delete";
import {connect} from "react-redux";
import {ReduxStore} from "../../../../bootstrap/store";
import HttpActionDispatcher from "../../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../../lib/redux++/action/HttpMethod";
import {Actions} from "../../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../../lib/redux++/action/HttpResetAction";
import Session from "../../../../utils/helpers/Session";

interface Props {
    id: number;
    employeeId: number;
    description: string;
    date: string;
    employeeName: string;
    employeeImage: string;
    replay: string;
    replayDate: string;
    file: string;
    deleting: boolean;
    errorDeleting: boolean;

    deleted: boolean;
    dispatch: (action: any) => void;
}

class ProjectIdeaItem extends React.Component<Props, any> {
    state = {
        showReplayForm: !!this.props.replay
    };

    private setToBeDeleted: boolean = false;

    componentDidUpdate() {
        const props = {dispatch: this.props.dispatch, success: this.props.deleted, error: this.props.errorDeleting};
        const handler = new SuccessStateActionHandler(props);
        handler.afterSuccess = () => this.props.dispatch(HttpResetAction(Actions.DELETE_PROJECT_IDEA));
        handler.afterError = () => this.setToBeDeleted = false;
    }

    render() {
        if (this.props.deleted && this.setToBeDeleted) {
            return null;
        }
        const session = Session.getUserObject();
        return (
            <Grid item md={8} lg={8} sm={12} style={{padding: '40px 0', marginBottom: 20}}>

                <Paper style={{padding: 16, marginBottom: 16}}>
                    <div className={"announcement-header"}>
                        <AvatarWithTitle image={URLs.getImage("profile_images", this.props.employeeImage)}
                                         title={this.props.employeeName}
                                         subtitle={this.props.date}/>
                        <div className={"left-part"}>
                            {
                                session.employeeId == this.props.employeeId &&
                                <IconButton disabled={this.props.deleting}
                                            onClick={this.onDelete}><DeleteIcon/></IconButton>
                            }
                        </div>
                    </div>
                    <br/>
                    <Typography variant={"body1"}>{this.props.description}</Typography>
                    <br/>
                    {
                        this.props.file &&
                        <>
                            <Button onClick={this.onDownload} variant={"outlined"} color={"secondary"}>تحميل الملف
                                المرفق</Button>
                            <br/>
                        </>
                    }
                    <br/>
                    {
                        this.state.showReplayForm ?
                            <>
                                <ProjectIdeaReplayFormContainer id={this.props.id}
                                                                replay={this.props.replay}
                                                                replayDate={this.props.replayDate}/>
                                <br/>
                                <Button onClick={() => this.setState({showReplayForm: false})} variant={"contained"}
                                        color={"primary"}>اخفاء</Button>
                                <br/>
                            </>
                            :
                            <Button onClick={() => this.setState({showReplayForm: true})} variant={"contained"}
                                    color={"primary"}>كتابة رد</Button>
                    }
                </Paper>
            </Grid>

        )
    }

    private onDelete = () => {
        this.setToBeDeleted = true;
        const data = {projectIdeaId: this.props.id};
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.PROJECT_IDEA.DELETE, Actions.DELETE_PROJECT_IDEA, data)
            .dispatch(this.props.dispatch);
    };

    private onDownload = () => {
        const url = URLs.getApiUrl(API_ROUTES.PROJECT_IDEA.DOWNLOAD.with(this.props.id, Session.getUserObject().session));
        window.open(url, "_blank");
    }

}

export default connect((store: ReduxStore) => {
    return {
        deleting: store.DeleteProjectIdea.loading,
        errorDeleting: store.DeleteProjectIdea.error,
        deleted: store.DeleteProjectIdea.success
    }
})(ProjectIdeaItem);