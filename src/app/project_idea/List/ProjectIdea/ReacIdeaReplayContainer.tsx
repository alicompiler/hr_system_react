import * as React from "react";
import {connect} from "react-redux";
import {ReduxStore} from "../../../../bootstrap/store";
import ProjectIdeaReplayForm from "./ProjectIdeaReplayForm";
import HttpActionDispatcher from "../../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Actions} from "../../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../../lib/redux++/action/HttpResetAction";

interface Props {
    id: number;
    loading: boolean;
    error: boolean;
    success: boolean;
    replay: string;
    replayDate: string;
    dispatch: (action: any) => void;
}

class ProjectIdeaReplayFormContainer extends React.Component<Props> {

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.REPLAY_ON_PROJECT_IDEA));
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <ProjectIdeaReplayForm replay={this.props.replay} replayDate={this.props.replayDate}
                                   loading={this.props.loading} onSubmit={this.onSubmit}/>
        )
    }

    private onSubmit = (values: any) => {
        values["projectIdeaId"] = this.props.id;
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.PROJECT_IDEA.REPLAY, Actions.REPLAY_ON_PROJECT_IDEA, values)
            .dispatch(this.props.dispatch);
    }
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.ReplayOnProjectIdea.loading,
        error: store.ReplayOnProjectIdea.error,
        success: store.ReplayOnProjectIdea.success
    }
})(ProjectIdeaReplayFormContainer)