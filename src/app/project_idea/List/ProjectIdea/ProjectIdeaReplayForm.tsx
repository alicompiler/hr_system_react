import * as React from "react";
import TextField from "../../../../utils/ui/components/form/components/TextField";
import Form from "../../../../utils/ui/components/form/Form";
import {Button, Typography} from "@material-ui/core";

interface Props {
    loading: boolean;
    replay: string;
    replayDate: string;
    onSubmit: (values: any) => void;
}

export default class ProjectIdeaReplayForm extends Form<Props> {

    render() {
        return (
            <div>
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"replay"}
                    value={this.props.replay ? this.props.replay : ''}
                    placeholder={"اكتب الرد هنا"}
                    xs={12} md={6} lg={6}
                    rows={5} maxRows={5} multiline
                    validationRules={{length: {minimum: 6}}}
                />
                {
                    this.props.replayDate &&
                    <>
                        <Typography variant={"body1"}>تاريخ الرد : {this.props.replayDate}</Typography>
                        <br/>
                    </>
                }
                <Button onClick={this.onSubmit} variant={"outlined"} disabled={this.props.loading}>ارسال الرد</Button>
            </div>
        )
    }

    private onSubmit = () => {
        if (!this.validate()) {
            return;
        }
        this.props.onSubmit(this.getValues());
    }

}