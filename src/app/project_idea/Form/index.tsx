import * as React from "react";
import ProjectIdeaForm from "./ProjectIdeaForm";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import Form from "../../../utils/ui/components/form/Form";

interface Props {
    loading: boolean;
    error: boolean;
    success: boolean;
    dispatch: (action: any) => void;
}

class CreateProjectIdeaForm extends React.Component<Props> {

    private form: Form<any> | null;

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => {
            this.props.dispatch(HttpResetAction(Actions.CREATE_PROJECT_IDEA));
        };
        handler.afterSuccess = () => this.form && this.form.clearValues();
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <ProjectIdeaForm ref={ref => this.form = ref} onSubmit={this.onSubmit} loading={this.props.loading}/>
        )
    }

    private onSubmit = (data: FormData) => {
        new HttpActionWithFileDispatcher(API_ROUTES.PROJECT_IDEA.CREATE, Actions.CREATE_PROJECT_IDEA, data)
            .dispatch(this.props.dispatch);
    };
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.CreateProjectIdea.loading,
        error: store.CreateProjectIdea.error,
        success: store.CreateProjectIdea.success
    }
})(CreateProjectIdeaForm);