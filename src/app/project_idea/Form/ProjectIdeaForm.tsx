import * as React from "react";
import TextField from "../../../utils/ui/components/form/components/TextField";
import Form from "../../../utils/ui/components/form/Form";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import {Button, Divider, LinearProgress, Typography} from "@material-ui/core";
import {objectToFormData} from "../../../utils/helpers/DataUtils";

interface Props {
    loading: boolean;
    onSubmit: (values: any) => void;
}

export default class ProjectIdeaForm extends Form<Props, any> {
    state = {
        attachment: null
    };

    render() {
        return (
            <div>
                <Typography variant={"headline"}>ارسال مقترح او فكرة لمشروع</Typography>
                <Divider/>

                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"description"}
                    placeholder={"اكتب التفاصيل هنا"}
                    xs={12} md={12} lg={12}
                    rows={10} multiline maxRows={10}
                    validationRules={{length: {minimum: 6}}}
                />

                <FileInput title={"الملفات المرفقة"} onChange={file => this.setState({attachment: file})}/>

                <br/><br/>

                <Button disabled={this.props.loading} onClick={this.onSubmit} variant={"contained"}
                        color={"primary"}>ارسال</Button>
                <br/><br/>
                {
                    this.props.loading &&
                    <LinearProgress color={"primary"}/>
                }
                <br/>
            </div>
        )
    }

    private onSubmit = () => {
        if (!this.validate()) {
            return;
        }
        const data = objectToFormData(this.getValues());
        if (this.state.attachment != null)
            data.append("attachment", this.state.attachment as any);
        this.props.onSubmit(data);
    }

}