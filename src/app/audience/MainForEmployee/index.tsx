import * as React from "react";
import Header from "../../components/Header";
import MonthlyEmployeeAudience from "../Reports/MonthlyEmployeeAudience";
import Session from "../../../utils/helpers/Session";
import {getDateAsString} from "../../../utils/helpers/DateHelper";

interface Props {
    route: any;
}

export default class index extends React.Component<Props> {

    render() {
        const employeeId = Session.getUserObject().employeeId;
        const endDate = getDateAsString(new Date());
        const startDate = getDateAsString(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));
        console.log(employeeId, endDate, startDate);
        return (
            <div>
                <Header title={"الحضور و الانصراف"}/>
                <MonthlyEmployeeAudience employeeId={employeeId} end={endDate} start={startDate}/>
            </div>
        )
    }
}