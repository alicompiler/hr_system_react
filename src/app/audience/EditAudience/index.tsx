import * as React from "react";
import AudienceTypeInfo from "./AudienceTypeInfo";
import EditAudienceForm from "./EditAudienceForm";
import {Divider, LinearProgress} from "@material-ui/core";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";

interface Props extends NetworkingState {
    id: any;
    audience: any;
    dispatch: (action: any) => void;
}

class EditAudience extends React.Component<Props> {

    componentWillMount() {
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE.single(this.props.id));
        const action = HttpGetAction(Actions.FETCH_AUDIENCE, url);
        this.props.dispatch(action);
    }

    render() {
        if (this.props.loading) {
            return <LinearProgress color={"primary"}/>
        }
        if (Object.keys(this.props.audience).length == 0)
            return null;
        return (
            <div>
                <AudienceTypeInfo audienceType={this.props.audience.audienceType}
                                  registeredShifts={this.props.audience.audienceShifts}/>
                <br/>
                <Divider/>
                <br/>
                <EditAudienceForm loading={this.props.loading} id={this.props.id} audience={this.props.audience.info}/>
                <br/>
            </div>
        )
    }
}

export default connect((store: any) => {
    return {
        loading: store.AudienceInfo.loading,
        error: store.AudienceInfo.error,
        audience: store.AudienceInfo.object
    }
})(EditAudience);