import * as React from "react";
import {Typography} from "@material-ui/core";
import ShiftsDetails from "./ShiftsDetails";

interface Props {
    audienceType: any;
    registeredShifts: any[];
}

export default class AudienceTypeInfo extends React.Component<Props> {

    render() {
        return (
            <div>
                <Typography variant={"subheading"}>Audience Type : {this.props.audienceType.name}</Typography>
                <ShiftsDetails shifts={this.props.audienceType.shifts}/>
                <br/><br/>
                <Typography variant={"subheading"}>Registered Shifts</Typography>
                <ShiftsDetails shifts={this.props.registeredShifts}/>
            </div>
        )
    }

}