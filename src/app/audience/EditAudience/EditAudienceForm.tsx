import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import Select from "../../../utils/ui/components/form/components/Select";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {Button, LinearProgress} from "@material-ui/core";
import {connect} from "react-redux";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props {
    loadingEmployees: boolean;
    errorEmployees: boolean;
    loadingAudienceType: boolean;
    errorAudienceType: boolean;
    employees: any[];
    audienceTypes: any[];
    audience: any;
    dispatch: (action: any) => void;
    //-----------------------------
    editing: boolean,
    errorEditing: boolean,
    successEditing: boolean,
    id: number,
    loading: boolean,
}

class EditAudienceForm extends Form<Props> {


    componentDidMount() {
        const employeeListUrl = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        this.props.dispatch(HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, employeeListUrl));
        const audienceTypesUrl = URLs.getApiUrl(API_ROUTES.AUDIENCE_TYPE.ALL);
        this.props.dispatch(HttpGetAction(Actions.FETCH_AUDIENCE_TYPES, audienceTypesUrl));
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => this.props.dispatch(HttpResetAction(Actions.EDIT_AUDIENCE));
        handler.handleSuccessResponse();
    }

    render() {

        if (this.props.loadingAudienceType || this.props.loadingEmployees) {
            return <LinearProgress color={"primary"}/>
        }

        return (
            <div>
                <Select
                    disabled
                    readOnly={true}
                    placeholder={"Employee"}
                    xs={12} sm={12} md={6} lg={6}
                    value={this.props.audience.employee_id}
                    options={this.getEmployeesOptions()}
                    style={{marginTop: 16}}
                />
                <Select
                    name={"audienceTypeId"}
                    disabled
                    placeholder={"Audience Type"}
                    xs={12} sm={12} md={6} lg={6}
                    value={this.props.audience.audience_type_id}
                    options={this.getAudienceTypesOptions()}
                    afterChange={(e: any) => this.setState({currentAudienceType: e.target.value})}
                    style={{marginTop: 16}}
                />
                <TextField
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"overtime"}
                    placeholder={"Overtime"}
                    label={"Overtime"}
                    value={this.props.audience.overtime}
                    xs={12} sm={12} md={6} lg={6}
                    type={"number"}
                    validationRules={{numericality: true}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"latetime"}
                    placeholder={"Latetime"}
                    label={"Latetime"}
                    value={this.props.audience.latetime}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{numericality: true}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"absenceShift"}
                    placeholder={"Absence Count"}
                    label={"Absence Count"}
                    value={this.props.audience.absenceShift}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{numericality: true}}
                />
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"suspected"}
                    value={this.props.audience.suspected}
                    placeholder={"State"}
                    xs={12} sm={12} md={6} lg={6}
                    options={this.getStateOptions()}
                    style={{marginTop: 16}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"notes"}
                    placeholder={"Notes"}
                    label={"Notes"}
                    value={this.props.audience.notes}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6}
                    multiline rows={3} rowsMax={3}
                />
                <br/>
                <div>
                    <Button disabled={this.props.loading} color={"primary"} variant={"contained"}
                            onClick={this.onSave}>SAVE</Button>
                    <br/>
                    {
                        this.props.loading && <LinearProgress color={"primary"}/>
                    }
                    <br/>
                    {this.props.editing && <LinearProgress color={"secondary"}/>}
                </div>
            </div>
        )
    }


    private onSave = () => {
        let isValid: boolean = this.validate();
        if (!isValid)
            return;
        const values = this.getValues();
        values["audienceId"] = this.props.id;
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE.EDIT);
        const action = HttpPostAction(Actions.EDIT_AUDIENCE, url, values);
        this.props.dispatch(action);
    };

    private getEmployeesOptions = () => {
        return this.props.employees.map((item: any) => ({text: item.name, value: item.id}));
    };

    private getAudienceTypesOptions = () => {
        return this.props.audienceTypes.map((item: any) => ({text: item.name, value: item.id}));
    };

    private getStateOptions = () => {
        return [
            {text: "DONE", value: 0},
            {text: "REVIEW NEEDED", value: 1}
        ]
    };

}

export default connect((store: any) => {
    return {
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error,
        employees: store.EmployeeList.array,
        //====================================================
        loadingAudienceType: store.FetchAudienceTypes.loading,
        errorAudienceType: store.FetchAudienceTypes.error,
        audienceTypes: store.FetchAudienceTypes.array,
        //====================================================
        editing: store.EditAudience.loading,
        errorEditing: store.EditAudience.error,
        successEditing: store.EditAudience.success,
    }
})(EditAudienceForm)