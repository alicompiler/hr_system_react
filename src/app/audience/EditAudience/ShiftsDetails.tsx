import * as React from "react";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {IFormElement} from "../../../utils/ui/components/form/FormElement";
import {Grid} from "@material-ui/core";
import Form from "../../../utils/ui/components/form/Form";

interface Props {
    shifts: any[],
    editable?: boolean;
}

export default class ShiftsDetails extends Form<Props> {

    render() {
        return (
            <div>
                {
                    this.props.shifts.map(this.renderShift)
                }
            </div>
        )
    }

    private renderShift = (shift: any, index: number) => {
        return <Grid container spacing={16} key={index}>
            {
                this.renderField(shift.login, shift, "login")
            }
            {
                this.renderField(shift.logout, shift, "logout")
            }
        </Grid>
    };

    private renderField = (value: any, shift: any, type: any) => {
        const tag = {...shift, type: type};
        return (
            <TextField
                ref={(ref: IFormElement | null) => ref && ref.changeValue(value)}
                readOnly={!this.props.editable}
                name={String(shift.shift_id)}
                placeholder={""}
                xs={12} md={6} lg={6}
                tag={tag}
                validationRules={{timeHHmm: {allowEmpty: true}}}
            />
        )
    };

}