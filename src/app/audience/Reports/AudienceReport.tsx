import MUITableComponent from "../../../utils/ui/components/table/mui-table/MUITableComponent";
import {RouteComponentProps} from "react-router";
import * as React from "react";

interface Props {
    data: any[];
    route?: RouteComponentProps;
    canEdit?: boolean;
}

export default class AudienceReportTable extends MUITableComponent<Props> {

    getColumns(): any[] {
        return [
            {
                name: "#",
                options: {
                    customBodyRender: (data: any) => {
                        return <a href={`/audience/edit/${data}`} target={"_blank"}>
                            {data}
                        </a>
                    },
                    display: this.props.canEdit ? 'true' : 'false'
                }
            },
            {
                name: "الموظف",
                options: {
                    customBodyRender: (data: any) => {
                        return <span ref={ref => {
                            if (ref && data[1] === 1) {
                                ref.style.setProperty('color', 'red', 'important');
                            }
                        }}>
                        {data[0]}
                    </span>
                    }
                }
            },
            "التاريخ",
            "الوقت الاضافي",
            "وقت التاخير",
            "الغيابات",
            {
                "name": "",
                options: {display: 'false'}
            }
        ];
    }

    getOptions(): object {
        let options = super.getOptions();

        // if (this.props.canEdit) {
        //     options["onRowClick"] = (data: any) => {
        //         const id = data[5];
        //         if (id === "-")
        //             return;
        //         this.props.route && this.props.route.history.push(`/audience/edit/${id}`)
        //     };
        // }
        return options;

    }

    getData(): any[][] {
        return this.props.data.map((item: any) => {
            return [
                item.audienceId ? item.audienceId : "-",
                [item.name, item.suspected],
                item.date ? item.date : "-",
                item.overtime != undefined && item.overtime != null ? item.overtime : "-",
                item.latetime != undefined && item.latetime != null ? item.latetime : "-",
                item.absenceShift != undefined && item.absenceShift != null ? item.absenceShift : "-",
                item.audienceId ? item.audienceId : "-"
            ]
        })
    }

    getTitle(): string {
        return "جدول الحضور اليومي";
    }

}