import * as React from "react";
import LEAComponent, {LEAComponentProps} from "../../../../my_framework/component/networking/LEAComponent";
import HttpActionDispatcher from "../../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Actions} from "../../../../bootstrap/actions";
import AudienceReportTable from "../AudienceReport";
import {connect} from "react-redux";
import {ReduxStore} from "../../../../bootstrap/store";
import {RouteComponentProps} from "react-router";

interface Props extends LEAComponentProps {
    data: any[];
    route?: RouteComponentProps;
    canEdit?: boolean;
    date: string;
}

class AudienceByDate extends LEAComponent<Props> {

    dispatchComponentOnWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET,
            API_ROUTES.AUDIENCE.AUDIENCE_BY_DATE.with(this.props.date), Actions.FETCH_AUDIENCE_BY_DATE)
    }

    protected renderContent(): any {
        return <AudienceReportTable canEdit={this.props.canEdit} route={this.props.route} data={this.props.data}/>
    }

}

export default connect((store: ReduxStore) => {
    return {
        loading: store.AudienceByDate.loading,
        error: store.AudienceByDate.error,
        data: store.AudienceByDate.array
    }
})(AudienceByDate)