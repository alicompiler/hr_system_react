import * as React from "react";
import LEAComponent, {LEAComponentProps} from "../../../../my_framework/component/networking/LEAComponent";
import HttpActionDispatcher from "../../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Actions} from "../../../../bootstrap/actions";
import AudienceReportTable from "../AudienceReport";
import {connect} from "react-redux";
import {ReduxStore} from "../../../../bootstrap/store";

interface Props extends LEAComponentProps {
    data: any[];
    start: string;
    end: string;
}

class MonthlyAudience extends LEAComponent<Props> {

    dispatchComponentOnWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET,
            API_ROUTES.AUDIENCE.MONTHLY.with(this.props.start, this.props.end), Actions.FETCH_MONTHLY_AUDIENCE_REPORT);
    }

    protected renderContent(): any {
        return <AudienceReportTable data={this.props.data}/>
    }

}

export default connect((store: ReduxStore) => {
    return {
        loading: store.MonthlyAudienceReport.loading,
        error: store.MonthlyAudienceReport.error,
        data: store.MonthlyAudienceReport.array
    }
})(MonthlyAudience)