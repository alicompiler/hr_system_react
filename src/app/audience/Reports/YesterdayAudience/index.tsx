import * as React from "react";
import LEAComponent, {LEAComponentProps} from "../../../../my_framework/component/networking/LEAComponent";
import HttpActionDispatcher from "../../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Actions} from "../../../../bootstrap/actions";
import AudienceReportTable from "../AudienceReport";
import {connect} from "react-redux";
import {ReduxStore} from "../../../../bootstrap/store";
import {RouteComponentProps} from "react-router";

interface Props extends LEAComponentProps {
    data: any[];
    route?: RouteComponentProps;
    canEdit?: boolean;
}

class YesterdayAudience extends LEAComponent<Props> {

    dispatchComponentOnWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET,
            API_ROUTES.AUDIENCE.YESTERDAY_REPORT, Actions.FETCH_YESTERDAY_AUDIENCE_REPORT)
    }

    protected renderContent(): any {
        return <AudienceReportTable canEdit={this.props.canEdit} route={this.props.route} data={this.props.data}/>
    }

}

export default connect((store: ReduxStore) => {
    return {
        loading: store.DailyAudienceReport.loading,
        error: store.DailyAudienceReport.error,
        data: store.DailyAudienceReport.array
    }
})(YesterdayAudience)