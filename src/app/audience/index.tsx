import * as React from "react";
import {Route, RouteComponentProps, Switch} from "react-router-dom";
import ImportDataFromHTML from "./InputFromHTML/ImportDataFromHTML";
import ShiftOff from "./OffShiftOffDay/ShiftOff/ShiftOff";
import MainForAdmin from "./MainForAdmin/index";
import MainForEmployee from "./MainForEmployee/index";
import Session from "../../utils/helpers/Session";
import DayOff from "./OffShiftOffDay/DayOff/DayOff";
import ExceptionShiftOff from "./OffShiftOffDay/ExceptionShiftOff/ExceptionShiftOff";
import EditAudience from "./EditAudience";

export default class Audience extends React.Component {
    render() {
        const session = Session.getUserObject();
        return (
            <Switch>
                {
                    session.userType === "admin" &&
                    <Route exact path={"/audience"} component={(route: any) => <MainForAdmin route={route}/>}/>
                }
                {
                    session.userType === "employee" &&
                    <Route exact path={"/audience"} component={(route: any) => <MainForEmployee route={route}/>}/>
                }
                {
                    session.userType === "admin" &&
                    <Route exact path={"/audience/edit/:id(\\d+)"}
                           component={(route: RouteComponentProps) => <EditAudience id={route.match.params["id"]}/>}/>
                }
                {
                    session.userType === "admin" &&
                    <>
                        <Route exact path={"/audience/import"} component={() => <ImportDataFromHTML/>}/>
                        <Route exact path={"/audience/day-off"} component={() => <DayOff/>}/>
                        <Route exact path={"/audience/shift-off"} component={() => <ShiftOff/>}/>
                        <Route exact path={"/audience/exception-shift-off"} component={() => <ExceptionShiftOff/>}/>
                    </>
                }
            </Switch>
        );
    }
}
