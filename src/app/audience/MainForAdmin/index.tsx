import * as React from "react";
import Header from "../../components/Header";
import ActionBar from "../../../my_framework/ui/utils_componenet/ActionBar";
import {Button, Divider, Grid} from "@material-ui/core";
import Form from "../../../utils/ui/components/form/Form";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import Select, {ISelectOption} from "../../../utils/ui/components/form/components/Select";
import EmployeeListToSelectOptionsAdapter from "../../employee/utils/EmployeeListToSelectOptionsAdapter";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {dispatchFetchEmployeeListAction} from "../../employee/utils/employeeActions";
import YesterdayAudience from "../Reports/YesterdayAudience";
import MonthlyAudience from "../Reports/MonthlyAudience";
import MonthlyEmployeeAudience from "../Reports/MonthlyEmployeeAudience";
import AudienceByDate from "../Reports/AudienceByDate";
import {Link} from "react-router-dom";

interface Props extends NetworkingState {
    route: any;
    employees: any[];
    dispatch: (action: any) => void;
}

interface State {
    type: "daily" | "monthly" | "monthly-employee" | "by-date";
    fromDate: string;
    toDate: string;
    employeeId: number;
    fromDateEmployee: string;
    toDateEmployee: string;
    date: string;
}

class index extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            type: "daily",
            fromDate: "",
            toDate: "",
            date: "",
            employeeId: -1,
            fromDateEmployee: "",
            toDateEmployee: ""
        };
    }

    componentWillMount() {
        dispatchFetchEmployeeListAction(this.props.dispatch);
    }

    render() {
        return (
            <div>
                <Header title={"الحضور و الانصراف"}/>
                <ActionBar changeDisplayType={false} action={true}
                           actionLink={"/audience/import"} actionTitle={"استيراد بيانات"}/>
                <div style={{textAlign: 'left'}}>
                    <Link to={'/audience/day-off'}>
                        <Button style={{margin: 6}} onClick={() => this.changeType("daily")} variant={"contained"}>يوم
                            عطلة</Button>
                    </Link>
                    <Link style={{margin: 6}} to={'/audience/shift-off'}>
                        <Button onClick={() => this.changeType("daily")} variant={"contained"}>شفت عطلة</Button>
                    </Link>
                    <Link style={{margin: 6}} to={'/audience/exception-shift-off'}>
                        <Button onClick={() => this.changeType("daily")} variant={"contained"}>عطلة استثائية</Button>
                    </Link>
                </div>
                <br/><br/>
                <div>
                    <Button onClick={() => this.changeType("daily")}
                            variant={"contained"}>الحظور اليومي</Button>
                </div>
                <br/>
                <div>
                    <Grid container spacing={16}>
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"date"}
                            placeholder={"تقرير حسب تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                    </Grid>
                    <Button onClick={() => this.changeType("by-date")} variant={"contained"}>الحظور حسب تاريخ</Button>
                </div>
                <br/>
                <div>
                    <Grid container spacing={16}>
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"fromDate"}
                            placeholder={"من تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"toDate"}
                            placeholder={"الى تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                    </Grid>
                    <Button onClick={() => this.changeType("monthly")} variant={"contained"}>الحظور الشهري</Button>
                </div>
                <br/>
                <div>
                    <Grid container spacing={16}>
                        <Select
                            ref={(ref: any) => this.pushElementRef(ref)}
                            name={"employee"}
                            placeholder={"الموظف"}
                            xs={12} sm={12} md={4} lg={4}
                            options={this.employeesOptions()}
                            style={{marginTop: 16, marginBottom: 16}}
                        />
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"fromDateEmployee"}
                            placeholder={"من تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"toDateEmployee"}
                            placeholder={"الى تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                    </Grid>
                    <Button onClick={() => this.changeType("monthly-employee")} disabled={this.props.loading}
                            variant={"contained"}>الحظور الشهري</Button>
                </div>

                <br/><br/>
                <Divider/>
                <br/><br/>
                {
                    this.renderAudienceResult()
                }
            </div>
        )
    }

    private renderAudienceResult = () => {
        switch (this.state.type) {
            case "daily" :
                return <YesterdayAudience canEdit={true} route={this.props.route}/>;
            case "by-date":
                return <AudienceByDate canEdit={true} route={this.props.route} date={this.state.date}/>
            case "monthly":
                return <MonthlyAudience end={this.state.toDate}
                                        start={this.state.fromDate}/>;
            case "monthly-employee":
                return <MonthlyEmployeeAudience employeeId={this.state.employeeId} end={this.state.toDate}
                                                start={this.state.fromDate}/>;
            default:
                return null;
        }
    };

    private employeesOptions = (): ISelectOption[] => {
        const adapter = new EmployeeListToSelectOptionsAdapter(this.props.employees);
        return adapter.toSelectOptions();
    };

    private changeType = (type: "daily" | "monthly" | "monthly-employee" | "by-date") => {
        if (type === "daily") {
            this.setState({type: type});
        } else if (type === "by-date") {
            const dateElement = this.getFieldByName("date");
            dateElement && dateElement.validate();
            if (dateElement && dateElement.isValid()) {
                const date = dateElement.getState();
                this.setState({"type": type, "date": date});
            }
        } else if (type === "monthly") {
            const startElement = this.getFieldByName("fromDate");
            const endElement = this.getFieldByName("toDate");
            endElement && endElement.validate();
            startElement && startElement.validate();
            if (startElement && endElement) {
                startElement.validate();
                endElement.validate();
                if (startElement.isValid() && endElement.isValid()) {
                    const start = startElement.getState();
                    const end = endElement.getState();
                    this.setState({type: "monthly", fromDate: start, toDate: end});
                }
            }
        } else if (type === "monthly-employee") {
            const startElement = this.getFieldByName("fromDateEmployee");
            const endElement = this.getFieldByName("toDateEmployee");
            const employeeElement = this.getFieldByName("employee");
            endElement && endElement.validate();
            startElement && startElement.validate();
            employeeElement && employeeElement.validate();
            if (startElement && endElement && employeeElement) {
                startElement.validate();
                endElement.validate();
                employeeElement.validate();
                if (startElement.isValid() && endElement.isValid() && employeeElement.isValid()) {
                    const start = startElement.getState();
                    const end = endElement.getState();
                    const employeeId = employeeElement.getState();
                    this.setState({type: "monthly-employee", fromDate: start, toDate: end, employeeId: employeeId});
                }
            }
        }
    }
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.EmployeeList.loading,
        error: store.EmployeeList.error,
        employees: store.EmployeeList.array,
    }
})(index)