import * as React from "react";
import DayOffForm from "./DayOffForm";
import {Divider} from "@material-ui/core";
import DayOffTable from "./DayOffTable";

interface Props {
}

export default class DayOff extends React.Component<Props> {

    private dayOffTable: any;

    render() {
        return (
            <div>
                <DayOffForm onSave={(dayOff: any) => this.dayOffTable.getWrappedInstance().addNewDayOff(dayOff)}/>
                <br/>
                <Divider/>
                <br/>
                <DayOffTable ref={ref => this.dayOffTable = ref}/>
            </div>
        )
    }

}