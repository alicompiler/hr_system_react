import * as React from "react";
import Table from "../../../generic/model_manager/Main/components/Table";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {Button, LinearProgress} from "@material-ui/core";
import {connect} from "react-redux";
import {HttpGetAction, HttpPostAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import {
    AddItemAtStartFetchArrayAction,
    RemoveItemAtIndexFetchArrayAction
} from "../../../../lib/redux++/action/FetchArrayActions";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    daysOff: any[];
    deleting: boolean;
    errorDeleting: boolean;
    success: boolean;
}

class DayOffTable extends React.Component<Props> {

    private isDelete: boolean = false;
    private deletedItemIndex: number;

    componentDidMount() {
        this.loadDaysOff();
    }

    componentDidUpdate() {
        if (this.isDelete && (this.props.success || this.props.error)) {
            const props = {
                dispatch: this.props.dispatch,
                loading: this.props.deleting,
                success: this.props.success,
                error: this.props.errorDeleting
            };
            const handler = new SuccessStateActionHandler(props);
            handler.afterSuccess = () => {
                this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.FETCH_DAYS_OFF, this.deletedItemIndex));
            };
            handler.handleSuccessResponse();
            this.isDelete = false;
        }
    }

    public loadDaysOff = () => {
        const url = URLs.getApiUrl(API_ROUTES.DAY_OFF.DAYS_OFF);
        const action = HttpGetAction(Actions.FETCH_DAYS_OFF, url);
        this.props.dispatch(action);
    };

    public addNewDayOff = (dayOff: any) => {
        this.props.dispatch(AddItemAtStartFetchArrayAction(Actions.FETCH_DAYS_OFF, {id: dayOff.id, date: dayOff.date}))
    };

    render() {
        return (
            <>
                {
                    this.props.deleting && <LinearProgress color={"secondary"}/>
                }
                <Table
                    data={this.props.daysOff}
                    loading={this.props.loading}
                    error={this.props.error}
                    onRowSelected={() => undefined}
                    columns={this.getColumns()}
                    options={{rowHover: false}}
                    title={"Day-Off Table"}
                    itemToArrayCallback={(item) => [item.id, item.date, item.id]}
                />
            </>
        )
    }

    private getColumns = () => {
        return [
            "ID",
            "Date",
            {
                name: "Action",
                options: {
                    customBodyRender: (dayOffId: any, meta: any) => {
                        const index = meta.rowIndex;
                        return <Button disabled={this.props.deleting} variant={"contained"}
                                       onClick={() => this.deleteDayOff(dayOffId, index)}
                                       className={"button error"}>DELETE</Button>
                    }
                }
            }
        ]
    };

    private deleteDayOff = (id: number, index: number) => {
        this.isDelete = true;
        this.deletedItemIndex = index;
        const url = URLs.getApiUrl(API_ROUTES.DAY_OFF.DELETE);
        const action = HttpPostAction(Actions.DELETE_DAY_OFF, url, {dayOffId: id});
        this.props.dispatch(action);
    }
}

export default connect((store: any) => {
    return {
        loading: store.FetchDayOff.loading,
        error: store.FetchDayOff.error,
        daysOff: store.FetchDayOff.array,
        success: store.DeleteDayOff.success,
        deleting: store.DeleteDayOff.loading,
        errorDeleting: store.DeleteDayOff.error
    }
}, null, null, {withRef: true})(DayOffTable);