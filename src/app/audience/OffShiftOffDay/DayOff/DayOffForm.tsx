import * as React from "react";
import Form from "../../../../utils/ui/components/form/Form";
import DatePicker from "../../../../utils/ui/components/form/components/DatePicker";
import {Button, Divider, LinearProgress, Typography} from "@material-ui/core";
import {HttpPostAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import HttpResetAction from "../../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    success: boolean;
    newDayOff: any;
    onSave: (dateOff: any) => void;
}

class DayOffForm extends Form<Props> {

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.clearValues();
            this.props.dispatch(HttpResetAction(Actions.ADD_DAY_OFF));
            this.props.onSave(this.props.newDayOff);
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <Typography variant={"headline"}>ADD NEW DAY-OFF</Typography>
                <Divider/>
                <DatePicker
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"Date"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />
                <Button disabled={this.props.loading} onClick={this.onSave} variant={"contained"}
                        color={"primary"}>SAVE</Button>
                <br/><br/>
                {
                    this.props.loading && <LinearProgress color={"secondary"}/>
                }
            </div>
        )
    }

    private onSave = () => {
        const date = this.getValues()["date"];
        if (date) {
            const url = URLs.getApiUrl(API_ROUTES.DAY_OFF.CREATE);
            const action = HttpPostAction(Actions.ADD_DAY_OFF, url, {date: date});
            this.props.dispatch(action);
        }
    }

}

export default connect((store: any) => {
    return {
        loading: store.AddDayOff.loading,
        error: store.AddDayOff.error,
        newDayOff: store.AddDayOff.object.dayOff,
        success: store.AddDayOff.object.success
    }
})(DayOffForm)