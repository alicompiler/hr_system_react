import * as React from "react";
import {Divider, LinearProgress} from "@material-ui/core";
import ShiftOffForm from "./ShiftOffForm";
// import ShiftOffTable from "./ShiftOffTable";
import {connect} from "react-redux";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {HttpGetAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import ShiftOffTable from "./ShiftOffTable";

interface Props extends NetworkingState {
    audienceTypes: any[];
    dispatch: (action: any) => void;
}

class ShiftOff extends React.Component<Props> {

    private table: any;

    componentDidMount() {
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE_TYPE.ALL);
        const action = HttpGetAction(Actions.FETCH_AUDIENCE_TYPES, url);
        this.props.dispatch(action);
    }

    render() {
        return (
            <div>
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }
                <ShiftOffForm onSave={(shiftOff: any) => this.table.getWrappedInstance().addNewAddon(shiftOff)}
                              audienceTypes={this.props.audienceTypes}/>
                <br/>
                <Divider/>
                <br/>
                <ShiftOffTable ref={(ref: any) => this.table = ref}/>
            </div>
        )
    }
}

export default connect((store: any) => {
    return {
        loading: store.FetchAudienceTypes.loading,
        error: store.FetchAudienceTypes.error,
        audienceTypes: store.FetchAudienceTypes.array
    }
})(ShiftOff)