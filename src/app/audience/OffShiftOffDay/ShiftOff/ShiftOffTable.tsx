import * as React from "react";
import Table from "../../../generic/model_manager/Main/components/Table";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {Button, LinearProgress} from "@material-ui/core";
import {connect} from "react-redux";
import {HttpGetAction, HttpPostAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import {
    AddItemAtStartFetchArrayAction,
    RemoveItemAtIndexFetchArrayAction
} from "../../../../lib/redux++/action/FetchArrayActions";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    shiftsOff: any[];
    deleting: boolean;
    errorDeleting: boolean;
    success: boolean;
}

class ShiftOffTable extends React.Component<Props> {

    private isDelete: boolean = false;
    private deletedItemIndex: number;

    componentDidMount() {
        this.loadShiftsOff();
    }

    componentDidUpdate() {
        if (this.isDelete && (this.props.success || this.props.error)) {
            const props = {
                dispatch: this.props.dispatch,
                loading: this.props.deleting,
                success: this.props.success,
                error: this.props.errorDeleting
            };
            const handler = new SuccessStateActionHandler(props);
            handler.afterSuccess = () => {
                this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.FETCH_SHIFT_OFF, this.deletedItemIndex));
            };
            handler.handleSuccessResponse();
            this.isDelete = false;
        }
    }

    public loadShiftsOff = () => {
        const url = URLs.getApiUrl(API_ROUTES.SHIFT_OFF.SHIFT_OFF);
        const action = HttpGetAction(Actions.FETCH_SHIFT_OFF, url);
        this.props.dispatch(action);
    };

    public addNewShiftOff = (shiftOff: any) => {
        this.props.dispatch(AddItemAtStartFetchArrayAction(Actions.FETCH_SHIFT_OFF, {
            id: shiftOff.id,
            date: shiftOff.date,
            shift_id: shiftOff.shift_id,
            audienceType: "-",
            shiftType: "-"
        }))
    };

    render() {

        return (
            <>
                {
                    this.props.deleting && <LinearProgress color={"secondary"}/>
                }
                <Table
                    data={this.props.shiftsOff}
                    loading={this.props.loading}
                    error={this.props.error}
                    onRowSelected={() => undefined}
                    columns={this.getColumns()}
                    options={{rowHover: false}}
                    pointerCursor={false}
                    title={"Shift-Off Table"}
                    itemToArrayCallback={this.itemToArrayCallback}
                />
            </>
        )
    }

    private itemToArrayCallback = (item: any) => {
        return [
            item.id,
            item.shift_id,
            item.shiftType == 1 ? "Morning Shift" : (item.shiftType === 2 ? "Afternoon Shift" : "-"),
            item.audienceType,
            item.date,
            item.id
        ]
    };

    private getColumns = () => {
        return [
            "ID",
            "Shift ID",
            "Shift Type",
            "Audience Type",
            "Date",
            {
                name: "Action",
                options: {
                    customBodyRender: (shiftOffId: any, meta: any) => {
                        const index = meta.rowIndex;
                        return <Button disabled={this.props.deleting} variant={"contained"}
                                       onClick={() => this.deleteShiftOff(shiftOffId, index)}
                                       className={"button error"}>DELETE</Button>
                    }
                }
            }
        ]
    };

    private deleteShiftOff = (id: number, index: number) => {
        this.isDelete = true;
        this.deletedItemIndex = index;
        const url = URLs.getApiUrl(API_ROUTES.SHIFT_OFF.DELETE);
        const action = HttpPostAction(Actions.DELETE_SHIFT_OFF, url, {shiftOffId: id});
        this.props.dispatch(action);
    }
}

export default connect((store: any) => {
    return {
        loading: store.FetchShiftOff.loading,
        error: store.FetchShiftOff.error,
        shiftsOff: store.FetchShiftOff.array,
        success: store.DeleteShiftOff.success,
        deleting: store.DeleteShiftOff.loading,
        errorDeleting: store.DeleteShiftOff.error
    }
}, null, null, {withRef: true})(ShiftOffTable);