import * as React from "react";
import Form from "../../../../utils/ui/components/form/Form";
import Select, {ISelectOption} from "../../../../utils/ui/components/form/components/Select";
import DatePicker from "../../../../utils/ui/components/form/components/DatePicker";
import {Button, LinearProgress} from "@material-ui/core";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import {HttpPostAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    audienceTypes: any[];
    employees: any[],
    success: boolean;
    exceptionShiftOff: any;
    dispatch: (action: any) => void;
    onSave: (shiftOff: any) => void;
}

interface State {
    currentAudienceType: number;
    shiftsOptions: any[];
}

class ExceptionShiftOffForm extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {currentAudienceType: -1, shiftsOptions: []}
    }

    private getAudienceTypesOptions() {
        const audienceTypes: ISelectOption[] = [];
        for (let i = 0; i < this.props.audienceTypes.length; i++) {
            audienceTypes.push({text: this.props.audienceTypes[i].name, value: this.props.audienceTypes[i].id});
        }
        return audienceTypes;
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.clearValues();
            this.props.dispatch(HttpResetAction(Actions.ADD_EXCEPTION_SHIFT_OFF));
            this.props.onSave(this.props.exceptionShiftOff);
        };
        handler.handleSuccessResponse();
    }

    render() {
        const audienceTypeOptions = this.getAudienceTypesOptions();
        const employees: any[] = this.getEmployees();
        return (
            <div>

                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"employeeId"}
                    placeholder={"Employee"}
                    xs={12} sm={12} md={6} lg={6}
                    options={employees}
                    style={{marginTop: 16}}
                />

                <Select
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"audienceType"}
                    placeholder={"Audience Type"}
                    xs={12} sm={12} md={6} lg={6}
                    options={audienceTypeOptions}
                    afterChange={(e: any) => this.onAudienceTypeChange(e)}
                    style={{marginTop: 16}}
                />
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"shiftId"}
                    placeholder={"Shift"}
                    xs={12} sm={12} md={6} lg={6}
                    options={this.state.shiftsOptions}
                    style={{marginTop: 16}}
                />
                <DatePicker
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"Date"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />

                <br/>

                <Button variant={"contained"} color={"primary"} onClick={this.onSave}>
                    SAVE
                </Button>

                <br/><br/>
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }

            </div>
        )
    }


    private onAudienceTypeChange = (e: any) => {
        if (!e || !e.target) {
            this.setState({shiftsOptions: []});
            return;
        }
        const shifts: ISelectOption[] = [];
        for (let i = 0; i < this.props.audienceTypes.length; i++) {
            const item = this.props.audienceTypes[i];
            if (item.id == e.target.value) {
                item.shifts.forEach((item: any) => {
                    const text = item.shiftType == 1 ? "Morning" : "Afternoon";
                    shifts.push({text: text, value: item.id});
                });
                break;
            }
        }
        this.setState({shiftsOptions: shifts});
    };

    private getEmployees() {
        const employees: ISelectOption[] = [];
        this.props.employees.forEach((item: any) => {
            employees.push({text: item.name, value: item.id});
        });
        return employees;
    }

    private onSave = () => {
        const values = this.getValues();
        const url = URLs.getApiUrl(API_ROUTES.EXCEPTION_SHIFT_OFF.CREATE);
        const action = HttpPostAction(Actions.ADD_EXCEPTION_SHIFT_OFF, url, values);
        this.props.dispatch(action);
    }
}

export default connect((store: any) => {
    return {
        loading: store.AddExceptionShiftOff.loading,
        error: store.AddExceptionShiftOff.error,
        success: store.AddExceptionShiftOff.object.success,
        exceptionShiftOff: store.AddExceptionShiftOff.object.exceptionShiftOff
    }
}, null, null, {withRef: true})(ExceptionShiftOffForm);