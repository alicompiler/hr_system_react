import * as React from "react";
import {Divider, LinearProgress} from "@material-ui/core";
import {connect} from "react-redux";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {HttpGetAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import ExceptionShiftOffTable from "./ExceptionShiftOffTable";
import ExceptionShiftOffForm from "./ExceptionShiftOffForm";

interface Props extends NetworkingState {
    audienceTypes: any[];
    employees: any[],
    loadingEmployees: boolean,
    errorEmployees: boolean;
    dispatch: (action: any) => void;
}

class ExceptionShiftOff extends React.Component<Props> {

    private table: any;

    componentDidMount() {
        this.dispatchFetchAudienceTypeAction();
        this.dispatchFetchEmployeeList();
    }

    private dispatchFetchEmployeeList() {
        const url = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        const action = HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, url);
        this.props.dispatch(action);
    }

    private dispatchFetchAudienceTypeAction() {
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE_TYPE.ALL);
        const action = HttpGetAction(Actions.FETCH_AUDIENCE_TYPES, url);
        this.props.dispatch(action);
    }

    render() {
        return (
            <div>
                {
                    (this.props.loading || this.props.loadingEmployees) && <LinearProgress color={"primary"}/>
                }
                <ExceptionShiftOffForm
                    employees={this.props.employees}
                    onSave={(shiftOff: any) => this.table.getWrappedInstance().addNewAddon(shiftOff)}
                    audienceTypes={this.props.audienceTypes}/>
                <br/>
                <Divider/>
                <br/>
                <ExceptionShiftOffTable ref={(ref: any) => this.table = ref}/>
            </div>
        )
    }
}

export default connect((store: any) => {
    return {
        loading: store.FetchAudienceTypes.loading,
        error: store.FetchAudienceTypes.error,
        audienceTypes: store.FetchAudienceTypes.array,
        employees: store.EmployeeList.array,
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error
    }
})(ExceptionShiftOff)