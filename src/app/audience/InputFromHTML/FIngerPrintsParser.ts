export default class FingerPrintsParser {

    private readonly html: string;
    private readonly items: any[];

    constructor(html: string = "") {
        this.html = html;
        this.items = [];
    }

    public parse(): boolean {
        try {
            this.parseHTML();
            return true;
        } catch (e) {
            return false;
        }
    }

    private parseHTML() {
        const tableBody = this.getTableBody();
        if (tableBody) {
            this.parseFingerPrintsFromTableBody(tableBody);
        }
    }

    public getFingerPrints() {
        return this.items;
    }

    private parseFingerPrintsFromTableBody(tableBody: any) {
        for (let i = 0; i < tableBody.children.length; i++) {
            const values = this.getAllTableCells(tableBody, i);
            if (values) {
                this.addNewFingerPrint(values);
            }
        }
    }

    private addNewFingerPrint(values: any) {
        const name = values[0].innerText;
        let shifts = String(values[3].innerHTML).split("<br>");
        shifts.splice(-1, 1);
        const fingerPrints = this.getFingerPrintsForEmployee(shifts);
        this.items.push([name, shifts, fingerPrints]);
    }

    private getFingerPrintsForEmployee(shifts: string[]) {
        const fingerPrints: string[] = [];
        if (shifts.length == 0)
            return fingerPrints;
        for (let i = 0; i < shifts.length; i++) {
            const f = shifts[i].split("-");
            const firstFingerPrint = f[0].trim();
            const secondFingerPrint = f[1].trim() == "" ? null : f[1].trim();
            fingerPrints.push(firstFingerPrint.substr(0, 5));
            secondFingerPrint && fingerPrints.push(secondFingerPrint.substr(0, 5));
        }
        return fingerPrints;
    }

    private getAllTableCells(tableBody: any, i: number) {
        const row = tableBody.children.item(i);
        return row && row.getElementsByTagName("td");
    }

    private getTableBody() {
        const parser = new DOMParser();
        const doc = parser.parseFromString(this.html, "text/html");
        const tables = doc.getElementsByTagName("tbody");
        return tables.item(0);
    }
}