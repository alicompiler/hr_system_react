import * as React from "react";
import Table from "../../generic/model_manager/Main/components/Table";

interface Props {
    fingerPrints: any[];
}

export default class FingerPrintsTable extends React.Component<Props> {

    render() {
        return (
            <Table
                data={this.props.fingerPrints}
                columns={this.getColumns()}
                loading={false}
                onRowSelected={() => undefined}
                options={{}}
                title={"البيانات المستوردة"}
                itemToArrayCallback={(item) => item}
            />
        )
    }

    private renderShiftColumn = (item: any) => {
        return item.map((item: any, index: number) => <span key={index}>{item}<br/></span>)
    };

    private renderFingerPrintsColumn = (items: any[]) => {
        return items.map((item: any, index: number) => <span
            key={index}>{item}{index !== items.length - 1 ? " / " : ""}</span>)
    };

    private getColumns = () => {
        return [
            "الاسم",
            {
                name: "الورديات",
                options: {
                    customBodyRender: this.renderShiftColumn
                }
            },
            {
                name: "البصمات",
                options: {
                    customBodyRender: this.renderFingerPrintsColumn
                }
            },
        ];
    }

}


