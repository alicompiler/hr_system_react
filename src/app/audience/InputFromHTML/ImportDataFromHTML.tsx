import * as React from "react";
import TextField from "../../../utils/ui/components/form/components/TextField";
import Form from "../../../utils/ui/components/form/Form";
import {Button, LinearProgress, Typography} from "@material-ui/core";
import FingerPrintsTable from "./FingerPrintsTable";
import FingerPrintsParser from "./FIngerPrintsParser";
import If from "../../../lib/react-lang/If";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import {HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    success: boolean;
}

interface State {
    fingerPrints: any[];
    error: boolean;
}

class ImportDataFromHTML extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {fingerPrints: [], error: false};
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.SEND_AUDIENCE_DATA));
            this.clearValues();
            this.setState({error: false, fingerPrints: []});
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <DatePicker
                    ref={ref => this.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"Input Your HTML Here"}
                    xs={12} sm={12} md={6} xl={6} lg={6}
                    validationRules={{datetime: {dateOnly: true}}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"html"}
                    placeholder={"Input Your HTML Here"}
                    xs={12}
                    style={{background: "#EEE", padding: 8}}
                    validationRules={{length: {minimum: 0}}}
                    rows={10} multiline rowsMax={10}
                />
                <Button onClick={this.onParse} variant={"contained"} color={"primary"} size={"large"}>استيراد</Button>
                <br/><br/>
                <If condition={this.state.error}>
                    <Typography variant={"headline"} color={"error"}>
                        NOT VALID INPUT
                    </Typography>
                </If>
                <FingerPrintsTable fingerPrints={this.state.fingerPrints}/>
                <br/>
                <Button disabled={this.props.loading} onClick={this.onSend} variant={"contained"} color={"primary"}
                        size={"large"}>ارسال
                    البيانات</Button>
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }
            </div>
        )
    }

    onParse = () => {
        this.setState({error: false, fingerPrints: []}, () => {
            const html = this.getValues()["html"] || "";
            const parser = new FingerPrintsParser(html);
            const success = parser.parse();
            const fingerPrints = parser.getFingerPrints();
            this.setState({fingerPrints: fingerPrints, error: !success})
        });
    };

    onSend = () => {
        if (!this.validate()) {
            return;
        }
        const data = {
            fingerPrints: this.state.fingerPrints,
            date: this.getValues()["date"]
        };
        const url = URLs.getApiUrl(API_ROUTES.AUDIENCE.SEND_IMPORT_DATA);
        const action = HttpPostAction(Actions.SEND_AUDIENCE_DATA, url, data);
        this.props.dispatch(action);
    }
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.SendAudience.loading,
        error: store.SendAudience.error,
        success: store.SendAudience.success,
    }
})(ImportDataFromHTML);