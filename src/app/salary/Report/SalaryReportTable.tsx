import * as React from "react";
import Table from "../../generic/model_manager/Main/components/Table";

interface Props {
    data: any[];
    loading: boolean;
    error: boolean | null;
}

export default class SalaryReportTable extends React.Component<Props> {
    render() {
        return (
            <Table
                data={this.props.data}
                loading={this.props.loading}
                error={this.props.error}
                onRowSelected={() => undefined}
                columns={this.getColumns()}
                options={{rowHover: false}}
                pointerCursor={false}
                title={"جدول الرواتب"}
                itemToArrayCallback={this.itemToArrayCallback}
            />
        )
    }

    private itemToArrayCallback = (item: any) => {
        const overTimeMoneyPerMinute = 5 / 60;
        const latetimeMoneyPerMinute = 10 / 60;
        const absenceMoney = 25;
        const total = item.salary + item.bonus - item.deduction
            + (overTimeMoneyPerMinute * item.overtime)
            - (latetimeMoneyPerMinute * item.latetime)
            - (item.absenceShift * absenceMoney);
        return [
            item.name,
            item.salary,
            item.bonus ? item.bonus : "0",
            item.deduction ? item.deduction : "0",
            item.overtime ? item.overtime : "0",
            item.latetime ? item.latetime : "0",
            item.absenceShift ? item.absenceShift : "0",
            total
        ]
    };

    private getColumns = () => {
        return [
            "الموظف",
            "الراتب",
            "الاضافات",
            "الخصومات",
            "الوقت الاضافي",
            "وقت التاخير",
            "الغيابات",
            "المجموع",
        ]
    };
}