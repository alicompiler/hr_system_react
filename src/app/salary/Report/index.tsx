import * as React from "react";
import Header from "../../components/Header";
import {Button, Divider, Grid} from "@material-ui/core";
import {ReduxStore} from "../../../bootstrap/store";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import Form from "../../../utils/ui/components/form/Form";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {dispatchFetchEmployeeListAction} from "../../employee/utils/employeeActions";
import SalaryReportTable from "./SalaryReportTable";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    report: any[];
}

class SalaryReport extends Form<Props> {

    componentWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET, API_ROUTES.SALARY.MY_ADDONS, Actions.FETCH_MY_SALARY_ADDONS);
        dispatchFetchEmployeeListAction(this.props.dispatch)
    }

    render() {
        return (
            <div>
                <Header title={"الرواتب"}/>
                <div>
                    <Grid container spacing={16}>
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"fromDate"}
                            placeholder={"من تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"toDate"}
                            placeholder={"الى تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                    </Grid>
                    <Button onClick={() => this.gotoAddonsResultPage()} variant={"contained"}>بحث</Button>
                </div>
                <br/>
                <Divider/>
                <br/>
                <div className={"salary-report-table"}>
                    <SalaryReportTable data={this.props.report} loading={this.props.loading} error={this.props.error}/>
                </div>
            </div>
        )
    }

    private gotoAddonsResultPage = () => {
        if (!this.validate()) {
            return;
        }
        const urlRoute = API_ROUTES.SALARY.SALARY_REPORT.with(this.getValues()["fromDate"], this.getValues()["toDate"]);
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET, urlRoute, Actions.FETCH_SALARY_REPORT);
    };
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.SalaryReport.loading,
        error: store.SalaryReport.error,
        report: store.SalaryReport.array,
    }
})(SalaryReport)
