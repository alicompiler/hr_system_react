import * as React from "react";
import Header from "../../components/Header";
import {Button, Divider, Grid} from "@material-ui/core";
import {ReduxStore} from "../../../bootstrap/store";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import Form from "../../../utils/ui/components/form/Form";
import {RouteComponentProps} from "react-router";
import Select, {ISelectOption} from "../../../utils/ui/components/form/components/Select";
import EmployeeListToSelectOptionsAdapter from "../../employee/utils/EmployeeListToSelectOptionsAdapter";
import Session from "../../../utils/helpers/Session";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {dispatchFetchEmployeeListAction} from "../../employee/utils/employeeActions";
import AddonsTable from "../AddOns/AddonsTable";
import {Link} from "react-router-dom";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    addons: any[];
    loadingEmployees: any[];
    errorEmployees: any[];
    employees: any[];
    fromDate: any;
    toDate: any;
    route: RouteComponentProps
}

class MyAddonsBetweenDates extends Form<Props> {

    componentWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET,
            API_ROUTES.SALARY.MY_ADDONS_BETWEEN_DATE.with(this.props.fromDate, this.props.toDate), Actions.FETCH_MY_SALARY_ADDONS_BETWEEN_DATES);
        dispatchFetchEmployeeListAction(this.props.dispatch)
    }

    render() {
        return (
            <div>
                <Header title={"الرواتب"}/>
                {
                    Session.getUserObject().userType === "admin" &&
                    <>
                        <div style={{textAlign: "left"}}>
                            <Link to={"/salary/add-ons"}>
                                <Button style={{marginLeft: 16}} color={"primary"} variant={"contained"}>الاضافات و
                                    الخصومات</Button>
                            </Link>
                            <Link to={"/salary/report"}>
                                <Button color={"primary"} variant={"contained"}>تقرير الرواتب</Button>
                            </Link>
                        </div>
                        <br/><br/>
                    </>
                }
                <div>
                    <Grid container spacing={16}>
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"fromDate"}
                            placeholder={"من تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                        <DatePicker
                            ref={ref => this.pushElementRef(ref)}
                            name={"toDate"}
                            placeholder={"الى تاريخ"}
                            xs={12} sm={12} md={4} lg={4}
                            validationRules={{presence: true, datetime: {dateOnly: true}}}
                        />
                    </Grid>
                    <Button onClick={this.gotoAddonsResultPage} variant={"contained"}>بحث</Button>
                </div>
                <br/>
                {
                    Session.getUserObject().userType === "admin" &&
                    <div>
                        <Grid container spacing={16}>
                            <Select
                                ref={(ref: any) => this.pushElementRef(ref)}
                                name={"employeeId"}
                                placeholder={"الموظف"}
                                xs={12} sm={12} md={4} lg={4}
                                options={this.employeesOptions()}
                                style={{marginTop: 16, marginBottom: 16}}
                            />
                            <DatePicker
                                ref={ref => this.pushElementRef(ref)}
                                name={"fromDateEmployee"}
                                placeholder={"من تاريخ"}
                                xs={12} sm={12} md={4} lg={4}
                                validationRules={{presence: true, datetime: {dateOnly: true}}}
                            />
                            <DatePicker
                                ref={ref => this.pushElementRef(ref)}
                                name={"toDateEmployee"}
                                placeholder={"الى تاريخ"}
                                xs={12} sm={12} md={4} lg={4}
                                validationRules={{presence: true, datetime: {dateOnly: true}}}
                            />
                        </Grid>
                        <Button onClick={this.gotoAddonsResultPageForEmployee} disabled={this.props.loading}
                                variant={"contained"}>بحث</Button>
                    </div>
                }
                <br/>
                <Divider/>
                <br/>
                <AddonsTable addOns={this.props.addons} loading={this.props.loading} error={this.props.error}/>
            </div>
        )
    }

    private employeesOptions = (): ISelectOption[] => {
        const adapter = new EmployeeListToSelectOptionsAdapter(this.props.employees);
        return adapter.toSelectOptions();
    };

    private gotoAddonsResultPage = () => {

        const fromDateField: any = this.getFieldByName("fromDate");
        const toDateField: any = this.getFieldByName("toDate");
        fromDateField.validate();
        toDateField.validate();
        if (!fromDateField.isValid() || !toDateField.isValid()) {
            return;
        }
        const fromDate = fromDateField.getState();
        const toDate = toDateField.getState();
        const route = `/salary/addons/${fromDate}/${toDate}`;
        this.props.route.history.push(route);
    };

    private gotoAddonsResultPageForEmployee = () => {
        const fromDateField: any = this.getFieldByName("fromDateEmployee");
        const toDateField: any = this.getFieldByName("toDateEmployee");
        const employeeField: any = this.getFieldByName("employeeId");
        employeeField.validate();
        fromDateField.validate();
        toDateField.validate();
        if (!fromDateField.isValid() ||
            !toDateField.isValid() ||
            !employeeField.isValid()) {
            return;
        }
        const fromDate = fromDateField.getState();
        const toDate = toDateField.getState();
        const employeeId = employeeField.getState();
        const route = `/salary/addons/${employeeId}/${fromDate}/${toDate}`;
        this.props.route.history.push(route);
    }

}

export default connect((store: ReduxStore) => {
    return {
        loading: store.MySalaryAddonsBetweenDates.loading,
        error: store.MySalaryAddonsBetweenDates.error,
        addons: store.MySalaryAddonsBetweenDates.array,
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error,
        employees: store.EmployeeList.array,
    }
})(MyAddonsBetweenDates)
