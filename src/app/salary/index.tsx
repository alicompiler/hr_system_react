import * as React from "react";
import {Route} from "react-router-dom";
import SalaryAddons from "./AddOns";
import SalaryMain from "./Main"
import MyAddonsBetweenDates from "./Main/MyAddonsBetweenDates";
import AddonsForEmployee from "./Main/AddonsForEmployee";
import SalaryReport from "./Report";
import Session from "../../utils/helpers/Session";

interface Props {
    route: any;
}

export default class Salary extends React.Component<Props> {
    render() {
        const session = Session.getUserObject();
        return (
            <>
                <Route exact path={"/salary"} component={(route: any) => <SalaryMain route={route}/>}/>
                {
                    session.userType === "admin" &&
                    <>
                        <Route exact path={"/salary/add-ons"} component={() => <SalaryAddons/>}/>
                        <Route exact path={"/salary/report"} component={() => <SalaryReport/>}/>
                        <Route exact path={"/salary/addons/:id(\\d+)/:fromDate/:toDate"}
                               component={(route: any) => <AddonsForEmployee fromDate={route.match.params.fromDate}
                                                                             toDate={route.match.params.toDate}
                                                                             employeeId={route.match.params.id}
                                                                             route={route}/>}/>
                    </>
                }

                <Route exact path={"/salary/addons/:fromDate/:toDate"}
                       component={(route: any) => <MyAddonsBetweenDates fromDate={route.match.params.fromDate}
                                                                        toDate={route.match.params.toDate}
                                                                        route={route}/>}/>
            </>
        );
    }
}
