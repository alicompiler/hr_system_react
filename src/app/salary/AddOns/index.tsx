import * as React from "react";
import AddOnForm from "./AddOnForm";
import {connect} from "react-redux";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import {Divider} from "@material-ui/core";
import SalaryAddonTable from "./SalaryAddonTable";

interface Props {
    employees: any[],
    loadingEmployees: boolean,
    errorEmployees: boolean;
    dispatch: (action: any) => void;
}

class SalaryAddons extends React.Component<Props> {

    componentDidMount() {
        this.dispatchFetchEmployeeList();
    }

    private dispatchFetchEmployeeList() {
        const url = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        const action = HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, url);
        this.props.dispatch(action);
    }

    render() {
        return (
            <div>
                <AddOnForm employees={this.props.employees}/>
                <br/><br/>
                <Divider/>
                <br/><br/>
                <SalaryAddonTable/>
            </div>
        )
    }
}

export default connect((store: any) => {
    return {
        employees: store.EmployeeList.array,
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error
    }
})(SalaryAddons)