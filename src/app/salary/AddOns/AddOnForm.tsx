import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import TextField from "../../../utils/ui/components/form/components/TextField";
import Select, {ISelectOption} from "../../../utils/ui/components/form/components/Select";
import {IFormElement} from "../../../utils/ui/components/form/FormElement";
import {Button, LinearProgress} from "@material-ui/core";
import {HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    employees: any[];
    dispatch: (action: any) => void;
    success: boolean;
}

interface State {
    displayOther: boolean;
}

class AddOnForm extends Form<Props, State> {

    private categoryRef: IFormElement | null;

    constructor(props: Props) {
        super(props);
        this.state = {displayOther: false};
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => this.props.dispatch(HttpResetAction(Actions.SAVE_ADDON));
        handler.handleSuccessResponse();
    }

    render() {
        const categoryInputStyle = this.state.displayOther ? {} : {display: "none"};
        return (
            <div>
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"employee_id"}
                    placeholder={"Employee"}
                    xs={12} sm={12} md={6} lg={6}
                    options={this.employeesOptions()}
                    style={{marginTop: 16}}
                />
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"type"}
                    placeholder={"Type"}
                    xs={12} sm={12} md={6} lg={6}
                    options={TYPE_OPTIONS}
                    style={{marginTop: 16}}
                />
                <DatePicker
                    ref={ref => this.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"Date"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"amount"}
                    placeholder={"Amount"}
                    xs={12} md={6} lg={6}
                    type={"number"}
                    validationRules={{length: {minimum: 0}}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"note"}
                    placeholder={"Note"}
                    xs={12} md={6} lg={6}
                    rows={5} maxRows={5} multiline
                    style={{background: "#EEE", padding: 8}}
                    validationRules={{length: {minimum: 0}}}
                />
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"categoryOptions"}
                    placeholder={"Category"}
                    xs={12} sm={12} md={6} lg={6}
                    options={CATEGORY_OPTIONS}
                    style={{marginTop: 16}}
                    validationRules={{length: {minimum: 3}}}
                    afterChange={this.onOptionChange}
                />
                <TextField
                    ref={ref => this.categoryRef = ref}
                    name={"category"}
                    placeholder={"Other"}
                    xs={12} md={6} lg={6}
                    style={categoryInputStyle}
                    validationRules={{length: {minimum: 3}}}
                />
                <br/>
                <Button onClick={this.onSave} variant={"contained"} color={"primary"}>SAVE</Button>
                <br/><br/>
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }
            </div>
        )
    }

    private onOptionChange = (e: any) => {
        const value = e.target.value;
        if (value === "اخرى") {
            this.setState({displayOther: true});
            this.categoryRef && this.categoryRef.changeValue("");
            return;
        }
        this.setState({displayOther: false});
        this.categoryRef && this.categoryRef.changeValue(value);
    };

    private employeesOptions = (): ISelectOption[] => {
        return this.props.employees.map((item: any) => ({text: item.name, value: item.id}));
    };

    private onSave = () => {
        if (!this.categoryRef) {
            return;
        }
        if (!this.validate() && !this.categoryRef.isValid(null)) {
            return;
        }
        const values = this.getValues();
        values["category"] = this.categoryRef ? this.categoryRef.getState() : "";
        console.log(values);
        const url = URLs.getApiUrl(API_ROUTES.SALARY.ADD_ON.CREATE);
        const action = HttpPostAction(Actions.SAVE_ADDON, url, values);
        this.props.dispatch(action);
    }

}

export default connect((store: any) => {
    return {
        loading: store.SaveAddon.loading,
        error: store.SaveAddon.error,
        success: store.SaveAddon.success,
    }
})(AddOnForm)

const CATEGORY_OPTIONS: ISelectOption[] = [
    {text: "ايفادات", value: "ايفادات"},
    {text: "ساعات اضافية", value: "ساعات اضافية"},
    {text: "استقطاع سلفة", value: "استقطاع سلفة"},
    {text: "غرامة مالية", value: "غرامة مالية"},
    {text: "عقوبة", value: "عقوبة"},
    {text: "مكافئة", value: "مكافئة"},
    {text: "اخرى", value: "اخرى"},
];

const TYPE_OPTIONS: ISelectOption[] = [
    {text: "اضافات", value: 1},
    {text: "استقطاعات", value: -1}
];