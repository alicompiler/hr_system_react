import * as React from "react";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import {RemoveItemAtIndexFetchArrayAction} from "../../../lib/redux++/action/FetchArrayActions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Button, Dialog, DialogActions, DialogTitle, LinearProgress} from "@material-ui/core";
import Table from "../../generic/model_manager/Main/components/Table";

interface Props extends NetworkingState {
    dispatch: (action: any) => void;
    addOns: any[];
    deleting: boolean;
    errorDeleting: boolean;
    success: boolean;
}


interface State {
    open: boolean;
    id: number,
    index: number;
}

class SalaryAddonTable extends React.Component<Props, State> {

    private isDelete: boolean = false;
    private deletedItemIndex: number;

    constructor(props: Props) {
        super(props);
        this.state = {open: false, id: -1, index: -1};
    }

    componentDidMount() {
        this.loadAddons();
    }

    componentDidUpdate() {
        if (this.isDelete && (this.props.success || this.props.error)) {
            const props = {
                dispatch: this.props.dispatch,
                loading: this.props.deleting,
                success: this.props.success,
                error: this.props.errorDeleting
            };
            const handler = new SuccessStateActionHandler(props);
            handler.afterSuccess = () => {
                this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.FETCH_ADD_ON, this.deletedItemIndex));
            };
            handler.handleSuccessResponse();
            this.isDelete = false;
        }
    }

    public loadAddons = () => {
        const url = URLs.getApiUrl(API_ROUTES.SALARY.ADD_ON.ALL);
        const action = HttpGetAction(Actions.FETCH_ADD_ON, url);
        this.props.dispatch(action);
    };

    render() {
        return (
            <>
                {
                    this.props.deleting && <LinearProgress color={"secondary"}/>
                }
                <Table
                    data={this.props.addOns}
                    loading={this.props.loading}
                    error={this.props.error}
                    onRowSelected={() => undefined}
                    columns={this.getColumns()}
                    options={{rowHover: false}}
                    pointerCursor={false}
                    title={"Add-Ons Table"}
                    itemToArrayCallback={this.itemToArrayCallback}
                />
                {
                    this.confirmDialog()
                }
            </>
        )
    }

    private itemToArrayCallback = (item: any) => {
        return [
            item.type === 1 ? "اضافات" : "استقطاعات",
            item.employeeName,
            item.date,
            item.amount,
            item.category,
            item.note,
            item.id
        ]
    };

    private getColumns = () => {
        return [
            "-/+",
            "الموظف",
            "التاريخ",
            "المبلغ",
            "النوع",
            "الملاحظات",
            {
                name: "العملية",
                options: {
                    customBodyRender: (id: any, meta: any) => {
                        const index = meta.rowIndex;
                        return <Button disabled={this.props.deleting} variant={"contained"}
                                       onClick={() => this.setState({open: true, id, index})}
                                       className={"button error"}>حدف</Button>
                    }
                }
            }
        ]
    };

    private confirmDialog = () => {
        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"هل انت متاكد من الحذف ؟"}</DialogTitle>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        CANCEL
                    </Button>
                    <Button onClick={() => {
                        this.deleteAddon();
                        this.handleClose();
                    }} color="primary" autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        )
    };

    private handleClose = () => {
        this.setState({open: false, id: -1, index: -1})
    };

    private deleteAddon = () => {
        this.isDelete = true;
        this.deletedItemIndex = this.state.index;
        const url = URLs.getApiUrl(API_ROUTES.SALARY.ADD_ON.DELETE);
        const action = HttpPostAction(Actions.DELETE_ADDON, url, {addOnId: this.state.id});
        this.props.dispatch(action);
    }
}

export default connect((store: any) => {
    return {
        loading: store.FetchAddons.loading,
        error: store.FetchAddons.error,
        addOns: store.FetchAddons.array,
        success: store.DeleteAddon.success,
        deleting: store.DeleteAddon.loading,
        errorDeleting: store.DeleteAddon.error
    }
}, null, null, {withRef: true})(SalaryAddonTable);