import * as React from "react";
import Table from "../../generic/model_manager/Main/components/Table";

interface Props {
    addOns: any[];
    loading: boolean;
    error: boolean | null;
}

export default class AddonsTable extends React.Component<Props> {
    render() {
        return (
            <Table
                data={this.props.addOns}
                loading={this.props.loading}
                error={this.props.error}
                onRowSelected={() => undefined}
                columns={this.getColumns()}
                options={{rowHover: false}}
                pointerCursor={false}
                title={"جدول الاضافات و الخصومات"}
                itemToArrayCallback={this.itemToArrayCallback}
            />
        )
    }

    private itemToArrayCallback = (item: any) => {
        return [
            item.type === 1 ? "اضافة" : "خصم",
            item.employeeName,
            item.date,
            item.amount,
            item.category,
            item.note
        ]
    };

    private getColumns = () => {
        return [
            "-/+",
            "الموظف",
            "التاريخ",
            "المبلغ",
            "النوع",
            "الملاحظات",
        ]
    };
}