import * as React from "react";
import SalaryRiseCreateForm from "./SalaryRiseCreateForm";
import {connect} from "react-redux";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import Form from "../../../utils/ui/components/form/Form";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";

interface Props {
    employees: any[];
    dispatch: (action: any) => void;
    loading: boolean;
    error: boolean;
    success: boolean;
    loadingEmployees: boolean;
    errorEmployees: boolean;
}

class SalaryRiseCreateFormContainer extends React.Component<Props> {

    private form: Form<any> | null = null;

    componentWillMount() {
        const employeeListUrl = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        this.props.dispatch(HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, employeeListUrl));
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.CREATE_SALARY_RISE));
        handler.afterSuccess = () => {
            if (this.form) {
                this.form.clearValues();
            }
        };
        handler.handleSuccessResponse();
    }


    render() {
        return (
            <SalaryRiseCreateForm ref={ref => this.form = ref}
                                  loading={this.props.loading} employeeLoading={this.props.loadingEmployees}
                                  onSubmit={this.onSubmit} employees={this.props.employees}/>
        )
    }

    private onSubmit = (values: any) => {
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.SALARY_RISE.CREATE, Actions.CREATE_SALARY_RISE, values).dispatch(this.props.dispatch)
    }
}


export default connect((store: any) => {
    return {
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error,
        employees: store.EmployeeList.array,

        loading: store.CreateSalaryRise.loading,
        error: store.CreateSalaryRise.error,
        success: store.CreateSalaryRise.success,
    }
})(SalaryRiseCreateFormContainer)