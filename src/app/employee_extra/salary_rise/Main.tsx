import * as React from "react";
import SalaryRiseCreateFormContainer from "./SalaryRiseCreateFormContainer";
import {Divider} from "@material-ui/core";
import SalaryRiseList from "./SalaryRiseList";
import Header from "../../components/Header";

interface Props {
}

export default class Main extends React.Component<Props> {

    render() {
        return (
            <div>
                <Header title={"العلاوات"}/>
                <SalaryRiseCreateFormContainer/>
                <br/><br/>
                <Divider/>
                <br/>
                <SalaryRiseList showLoading showEmpty showError/>
                <br/>
            </div>
        )
    }

}