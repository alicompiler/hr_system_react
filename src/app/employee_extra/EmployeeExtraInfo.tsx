import * as React from "react";
import Header from "../components/Header";
import SalaryRiseList from "./salary_rise/SalaryRiseList";
import AppreciationLetterList from "./appreciation_letter/AppreciationLetterList";
import EmployeeRatingList from "./rating/List";

interface Props {
}

export default class EmployeeExtraInfo extends React.Component<Props> {

    render() {
        return (
            <div>
                <Header title={"العلاوات"}/>
                <SalaryRiseList showLoading showEmpty showError/>
                <br/><br/><br/>
                <Header title={"كتب الشكر"}/>
                <AppreciationLetterList showLoading showEmpty showError/>
                <br/><br/><br/>
                <Header title={"تقيمات الموظفين"}/>
                <EmployeeRatingList showLoading showEmpty showError/>
                <br/><br/><br/>
            </div>
        )
    }

}