import * as React from "react";
import TextField from "../../../utils/ui/components/form/components/TextField";
import Form from "../../../utils/ui/components/form/Form";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import Select from "../../../utils/ui/components/form/components/Select";
import {Button, LinearProgress} from "@material-ui/core";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import {objectToFormData} from "../../../utils/helpers/DataUtils";

interface Props {
    employees: any[];
    employeeLoading: boolean;
    loading: boolean;
    onSubmit: (values: any) => void;
}

export default class AppreciationLetterCreateForm extends Form<Props> {

    state = {
        file: null
    };

    render() {
        return (
            <div>
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    disabled={this.props.employeeLoading}
                    name={"employee_id"}
                    placeholder={"Employee"}
                    xs={12} sm={12} md={6} lg={6}
                    options={this.getEmployeesOptions()}
                    style={{marginTop: 16}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"title"}
                    placeholder={"العنوان"}
                    xs={12} md={6} lg={6}
                    validationRules={{length: {minimum: 3}}}
                />
                <DatePicker
                    ref={ref => this.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"التاريخ"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"note"}
                    placeholder={"الملاحظات"}
                    xs={12} md={8} lg={8}
                    multiline rows={5} maxRows={5}
                    validationRules={{length: {minimum: 0}}}
                />

                <br/>
                <FileInput title={"الملفات المرفقة"} onChange={(file) => this.setState({file: file})}/>

                <br/>
                <Button onClick={this.onSubmit} color={"primary"} variant={"contained"} style={{minWidth: 120}}>
                    حفظ
                </Button>

                <br/><br/>
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }
            </div>
        )
    }

    private onSubmit = () => {
        if (!this.validate()) {
            return;
        }
        const data = objectToFormData(this.getValues());
        if (this.state.file != null)
            data.append("attachment", this.state.file as any);
        this.props.onSubmit(data);
    };

    private getEmployeesOptions = () => {
        return this.props.employees.map((item: any) => ({text: item.name, value: item.id}));
    };
}