import * as React from "react";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import {Button, Card, Grid, LinearProgress, Typography} from "@material-ui/core";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import Session from "../../../utils/helpers/Session";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {RemoveItemAtIndexFetchArrayAction} from "../../../lib/redux++/action/FetchArrayActions";
import ErrorIcon from "@material-ui/core/SvgIcon/SvgIcon";
import EmptyList from "../../components/EmptyList";

interface Props {
    loading: boolean;
    error: boolean;
    list: any[];
    showLoading?: boolean;
    showError?: boolean;
    loadingMessage?: string;
    errorMessage?: string;
    showEmpty?: boolean;
    emptyMessage?: string;
    errorDeleting: boolean;
    deleting: boolean;
    deleted: boolean;
    dispatch: (action: any) => void;
}

class AppreciationLetterList extends React.Component<Props> {

    private currentIndex: number = -1;

    static defaultProps = {
        showLoading: false,
        showError: false,
        loadingMessage: 'جاري تحميل البيانات...',
        errorMessage: 'حصلت مشكلة خلال تحميل البيانات',
        showEmpty: false,
        emptyMessage: 'لا توجد اي نتائج'
    };

    componentWillMount() {
        new HttpActionDispatcher(HttpMethod.GET, API_ROUTES.APPRECIATION_LETTER.ALL, Actions.FETCH_APPRECIATION_LETTER)
            .dispatch(this.props.dispatch);
    }

    componentDidUpdate() {
        const props = {dispatch: this.props.dispatch, error: this.props.errorDeleting, success: this.props.deleted};
        const handler = new SuccessStateActionHandler(props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.DELETE_APPRECIATION_LETTER));
        handler.afterSuccess = () => {
            if (this.currentIndex == -1)
                return;
            this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.FETCH_APPRECIATION_LETTER, this.currentIndex));
            this.currentIndex = -1;
        };
        handler.handleSuccessResponse();
    }

    render() {
        if (this.props.loading && this.props.showLoading) {
            return <div>
                <LinearProgress color={"primary"}/>
                <Typography variant={"subheading"}>{this.props.loadingMessage}</Typography>
            </div>
        } else if (this.props.error && this.props.showError) {
            return <div>
                <div style={{textAlign: 'center'}}>
                    <ErrorIcon style={{fontSize: 24, color: '#E00'}}/>
                    <Typography variant={"subheading"}>{this.props.errorMessage}</Typography>
                </div>
            </div>
        } else {
            if (this.props.list.length == 0 && this.props.showEmpty) {
                return <EmptyList message={this.props.emptyMessage}/>
            }
            return this.renderContent();
        }
    }

    renderContent() {
        const session = Session.getUserObject();
        return (
            <div className={"employee-extra-list"}>
                {
                    this.props.list.map((item: any, index: number) => {
                        return <Card key={index} style={{padding: 16, marginBottom: 16}}>
                            <Grid item sm={12} md={6} key={index} className={"employee-extra-list-item"}>
                                <div>
                                    <span>الموظف </span>
                                    :
                                    <b>{item.employeeName}</b>
                                </div>
                                <div>
                                    <span>العنوان </span>
                                    :
                                    <b>{item.title}</b>
                                </div>
                                <div>
                                    <span>التاريخ </span>
                                    :
                                    <b>{item.date}</b>
                                </div>
                                <br/>
                                {
                                    item.note &&
                                    <div>
                                        <Typography variant={"body1"}>الملاحظات</Typography>
                                        <b>{item.note}</b>
                                    </div>
                                }

                                {
                                    item.file &&
                                    <>
                                        <br/>
                                        <Button onClick={() => this.download(item.id)} variant={"outlined"}
                                                color={"secondary"}>
                                            تحميل الملف المرفق
                                        </Button>
                                        <br/>
                                    </>
                                }

                                {
                                    session.userType === "admin" &&
                                    <div>
                                        <br/>
                                        <Button disabled={this.props.deleting}
                                                onClick={() => this.delete(item.id, index)} variant={"outlined"}
                                                color={"primary"}>حذف</Button>
                                    </div>
                                }
                            </Grid>
                        </Card>
                    })
                }
            </div>
        )
    }

    private download = (id: any) => {
        const url = URLs.getApiUrl(API_ROUTES.APPRECIATION_LETTER.DOWNLOAD.with(id, Session.getUserObject().session));
        window.open(url, "_blank");
    };

    private delete = (id: number, index: number) => {
        this.currentIndex = index;
        const data = {appreciationLetterId: id};
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.APPRECIATION_LETTER.DELETE, Actions.DELETE_APPRECIATION_LETTER, data).dispatch(this.props.dispatch);
    }


}


export default connect((store: ReduxStore) => {
    return {
        loading: store.AppreciationLetterList.loading,
        error: store.AppreciationLetterList.error,
        list: store.AppreciationLetterList.array,
        deleting: store.DeleteAppreciationLetter.loading,
        errorDeleting: store.DeleteAppreciationLetter.error,
        deleted: store.DeleteAppreciationLetter.success
    }
})(AppreciationLetterList);