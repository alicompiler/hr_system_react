import * as React from "react";
import AppreciationLetterCreateForm from "./AppreciationLetterCreateForm";
import {connect} from "react-redux";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import Form from "../../../utils/ui/components/form/Form";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";

interface Props {
    employees: any[];
    dispatch: (action: any) => void;
    loading: boolean;
    error: boolean;
    success: boolean;
    loadingEmployees: boolean;
    errorEmployees: boolean;
}

class AppreciationLetterCreateFormContainer extends React.Component<Props> {

    private form: Form<any> | null = null;

    componentWillMount() {
        const employeeListUrl = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        this.props.dispatch(HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, employeeListUrl));
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.CREATE_APPRECIATION_LETTER));
        handler.afterSuccess = () => {
            if (this.form) {
                this.form.clearValues();
            }
        };
        handler.handleSuccessResponse();
    }


    render() {
        return (
            <AppreciationLetterCreateForm ref={ref => this.form = ref}
                                          loading={this.props.loading} employeeLoading={this.props.loadingEmployees}
                                          onSubmit={this.onSubmit} employees={this.props.employees}/>
        )
    }

    private onSubmit = (values: any) => {
        new HttpActionWithFileDispatcher(API_ROUTES.APPRECIATION_LETTER.CREATE,
            Actions.CREATE_APPRECIATION_LETTER,
            values).dispatch(this.props.dispatch)
    }
}


export default connect((store: any) => {
    return {
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error,
        employees: store.EmployeeList.array,

        loading: store.CreateAppreciationLetter.loading,
        error: store.CreateAppreciationLetter.error,
        success: store.CreateAppreciationLetter.success,
    }
})(AppreciationLetterCreateFormContainer)