import * as React from "react";
import {Divider} from "@material-ui/core";
import AppreciationLetterCreateFormContainer from "./AppreciationLetterCreateFormContainer";
import AppreciationLetterList from "./AppreciationLetterList";
import Header from "../../components/Header";

interface Props {
}

export default class Main extends React.Component<Props> {

    render() {
        return (
            <div>
                <Header title={"كتب الشكر"}/>
                <AppreciationLetterCreateFormContainer/>
                <br/><br/>
                <Divider/>
                <br/>
                <AppreciationLetterList showLoading showEmpty showError/>
                <br/>
            </div>
        )
    }

}