import * as React from "react";
import CreateFormContainer from "./CreateFormContainer";
import {Divider} from "@material-ui/core";
import List from "./List";
import Header from "../../components/Header";

interface Props {
}

export default class Main extends React.Component<Props> {

    render() {
        return (
            <div>
                <Header title={"التقيمات"}/>
                <CreateFormContainer/>
                <br/><br/>
                <Divider/>
                <br/>
                <List showLoading showEmpty showError/>
                <br/>
            </div>
        )
    }

}