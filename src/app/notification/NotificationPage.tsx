import * as React from "react";
import NotificationList from "./List/NotificationList";
import Header from "../components/Header";

export default class NotificationPage extends React.Component {
    render() {
        return <div>
            <Header title={"الاشعارات"}/>
            <NotificationList showLoading showError showEmpty/>
        </div>
    }
}