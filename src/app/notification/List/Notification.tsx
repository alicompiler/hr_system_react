import * as React from "react";
import {Button, Card, CardContent, Typography} from "@material-ui/core";
import VS from "react-visibility-sensor";
import Axios from "axios";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {Link} from "react-router-dom";
import Session from "../../../utils/helpers/Session";

interface Props {
    id: any;
    title: string;
    detail: string;
    date: string;
    goto: string;
    type: any;
    viewed: any;
}

export default class Notification extends React.Component<Props, any> {
    state = {
        loading: false,
        setAsVisible: false
    };

    render() {
        const style = {backgroundColor: this.props.viewed ? '#FFF' : (this.props.id ? '#F8BBD0' : '#FFECB3')};
        return <VS onChange={this.onVisibilityChange}>
            <Card style={{padding: 16, marginBottom: 16, ...style}}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {this.props.title}
                    </Typography>
                    <Typography component="p">
                        {this.props.detail}
                    </Typography>
                    <br/>
                    {
                        this.renderGotoButton()
                    }
                </CardContent>
            </Card>
        </VS>
    }

    private renderGotoButton = () => {
        let button = null;
        switch (this.props.type) {
            case 1:
                button = <Link to={"/leave-requests"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 2:
                button = <Link to={"/loan"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 3:
                button = <Link to={"/leave-requests/" + this.props.goto}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 4:
                button = <Link to={"/loan/" + this.props.goto}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 5:
                button = <Link to={"/announcements/unwatched"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 6:
                button = <Link to={"/audience"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 7:
                button = <Link to={"/salary"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 8:
                button = <Link to={"/profile"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 9:
                button = <Link to={"/project-idea"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 10:
                button = <Link to={"/polls"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 11:
                button = <Link to={this.props.goto}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
            case 12:
                button = <Link to={"/loan"}>
                    <Button variant={"outlined"} color={"primary"}>
                        التفاصيل
                    </Button>
                </Link>;
                break;
        }
        return button;
    };

    private onVisibilityChange = (isVisible: boolean) => {
        if (this.props.id && isVisible && !this.state.loading && !this.state.setAsVisible) {
            this.setState({loading: true}, () => {
                const url = URLs.getApiUrl(API_ROUTES.NOTIFICATION.SET_VISIBLE);
                Axios.post(url, {notificationId: this.props.id}, {headers: {UserSession: Session.getUserObject().session}})
                    .then((response: any) => {
                        const data = response.data;
                        const success = data.success;
                        this.setState({loading: false, setAsVisible: success});
                    })
                    .catch(() => {
                        this.setState({loading: false, setAsVisible: false});
                    });
            });
        }
    };

}