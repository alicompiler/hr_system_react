import * as React from "react";
import Notification from "./Notification";
import {connect} from "react-redux";
import ReduxAction from "../../../lib/redux++/action/IAction";
import {Actions} from "../../../bootstrap/actions";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {LinearProgress, Typography} from "@material-ui/core";
import ErrorIcon from "@material-ui/core/SvgIcon/SvgIcon";
import EmptyList from "../../components/EmptyList";

interface Props {
    loading: boolean;
    error: boolean;
    notifications: any[];
    showLoading?: boolean;
    showError?: boolean;
    loadingMessage?: string;
    errorMessage?: string;
    showEmpty?: boolean;
    emptyMessage?: string;

    dispatch: (action: any) => void;
}

class NotificationList extends React.Component<Props, any> {

    static defaultProps = {
        showLoading: false,
        showError: false,
        loadingMessage: 'جاري تحميل البيانات...',
        errorMessage: 'حصلت مشكلة خلال تحميل البيانات',
        showEmpty: false,
        emptyMessage: 'لا توجد اي نتائج'
    };

    componentWillMount() {
        this.props.dispatch(ReduxAction(Actions.NOTIFICATION_VIEW_OPEN));
        new HttpActionDispatcher(HttpMethod.GET, API_ROUTES.NOTIFICATION.ALL, Actions.FETCH_NOTIFICATIONS)
            .dispatch(this.props.dispatch);
    }

    componentWillUnmount() {
        this.props.dispatch(ReduxAction(Actions.NOTIFICATION_VIEW_CLOSE))
    }

    render() {
        if (this.props.loading && this.props.showLoading) {
            return <div>
                <LinearProgress color={"primary"}/>
                <Typography variant={"subheading"}>{this.props.loadingMessage}</Typography>
            </div>
        } else if (this.props.error && this.props.showError) {
            return <div>
                <div style={{textAlign: 'center'}}>
                    <ErrorIcon style={{fontSize: 24, color: '#E00'}}/>
                    <Typography variant={"subheading"}>{this.props.errorMessage}</Typography>
                </div>
            </div>
        } else {
            if (this.props.notifications.length == 0 && this.props.showEmpty) {
                return <EmptyList message={this.props.emptyMessage}/>
            }

            return this.renderContent()
        }
    }

    renderContent() {
        return <div>
            {
                this.props.notifications.map((item: any, index: any) => {
                    return <Notification key={index}
                                         id={item.id} title={item.title} detail={item.detail}
                                         date={item.date} viewed={item.viewed} goto={item.goto}
                                         type={item.type}
                    />
                })
            }
        </div>
    }
}

export default connect((store: any) => {
    return {
        loading: store.Notifications.loading,
        error: store.Notifications.error,
        notifications: store.Notifications.array
    }
})(NotificationList)