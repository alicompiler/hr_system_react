import * as React from "react";
import FileList from "../../../my_framework/ui/list/files_list/FileList";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import Session from "../../../utils/helpers/Session";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import {Actions} from "../../../bootstrap/actions";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {RemoveItemAtIndexFetchArrayAction} from "../../../lib/redux++/action/FetchArrayActions";

interface Props extends NetworkingState {
    files: any[];
    dispatch: (action: any) => void;
    success: boolean;
    onLengthZeroRender?: () => any;
}

class AttachmentsList extends React.Component<Props> {

    static defaultProps = {onLengthZeroRender: () => undefined};
    private deletedItemIndex: number = -1;

    componentDidUpdate() {

        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.DELETE_ATTACHMENT));
            this.props.dispatch(RemoveItemAtIndexFetchArrayAction(Actions.EMPLOYEE_ATTACHMENTS, this.deletedItemIndex));
            this.deletedItemIndex = -1;
        };
        handler.handleSuccessResponse();
    }

    render() {
        if (this.props.files.length === 0 && this.props.onLengthZeroRender) {
            return this.props.onLengthZeroRender();
        }
        
        return <FileList files={this.props.files} showDelete loading={this.props.loading}
                         onDownload={this.onDownload} onDelete={this.onDelete}/>;
    }

    private onDownload = (file: any) => {
        const session = Session.getUserObject().session;
        const url = URLs.getApiUrl(API_ROUTES.PROFILE.ATTACHMENT.DOWNLOAD.with(file.id, session));
        window.open(url, "_blank");
    };

    private onDelete = (file: any, index: number) => {
        const data = {fileId: file.id};
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.POST, API_ROUTES.PROFILE.ATTACHMENT.DELETE, Actions.DELETE_ATTACHMENT, data);
        this.deletedItemIndex = index;
    };
}

export default connect((store: ReduxStore) => ({
    loading: store.DeleteAttachment.loading,
    error: store.DeleteAttachment.error,
    success: store.DeleteAttachment.success
}))(AttachmentsList)