import * as React from "react";
import EmployeeAttachments from "./index";
import Session from "../../../utils/helpers/Session";


export default class MyAttachments extends React.Component {
    render() {
        const employeeId = Session.getUserObject().employeeId;
        return <EmployeeAttachments employeeId={employeeId}/>
    }
}
