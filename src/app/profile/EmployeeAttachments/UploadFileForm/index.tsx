import * as React from "react";
import UploadFileForm from "./UploadFileForm";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import {API_ROUTES} from "../../../../utils/helpers/URLs";
import {Actions} from "../../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../../lib/redux++/action/HttpResetAction";
import {connect} from "react-redux";
import {AddItemAtStartFetchArrayAction} from "../../../../lib/redux++/action/FetchArrayActions";
import HttpActionWithFileDispatcher from "../../../../my_framework/data/redux/HttpActionWithFileDispatcher";

interface Props extends NetworkingState {
    success: boolean;
    newFile: any;
    dispatch: (action: any) => void;
}

class UploadFileFormContainer extends React.Component<Props> {

    private form: UploadFileForm | null;

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.UPLOAD_EMPLOYEE_FILE));
            this.form && this.form.clearValues();
            const action = AddItemAtStartFetchArrayAction(Actions.EMPLOYEE_ATTACHMENTS, this.props.newFile);
            this.props.dispatch(action);
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <UploadFileForm ref={ref => this.form = ref} onUpload={this.onUpload} loading={this.props.loading}/>
        )
    }

    onUpload = (data: FormData) => {
        const dispatcher = new HttpActionWithFileDispatcher(API_ROUTES.PROFILE.ATTACHMENT.UPLOAD, Actions.UPLOAD_EMPLOYEE_FILE, data);
        dispatcher.dispatch(this.props.dispatch);
    }

}

export default connect((store: any) => {
    return {
        loading: store.UploadEmployeeFile.loading,
        error: store.UploadEmployeeFile.error,
        success: store.UploadEmployeeFile.object.success,
        newFile: store.UploadEmployeeFile.object.newFile
    }
})(UploadFileFormContainer)