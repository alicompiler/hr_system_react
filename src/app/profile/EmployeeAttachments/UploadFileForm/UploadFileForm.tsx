import * as React from "react";
import Form from "../../../../utils/ui/components/form/Form";
import FileInput from "../../../../utils/ui/components/form/components/FileInput";
import TextField from "../../../../utils/ui/components/form/components/TextField";
import {Button, LinearProgress} from "@material-ui/core";

interface Props {
    loading: boolean;
    onUpload: (data: FormData) => void;
}

export default class UploadFileForm extends Form<Props> {

    private file: any;

    render() {
        return (
            <div>
                <FileInput title={"اختر ملفاً"}
                           onChange={(file: any) => this.file = file}/>
                <TextField
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"filename"}
                    placeholder={"Filename"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{length: {minimum: 3}}}
                />
                <Button color={"primary"} disabled={this.props.loading} variant={"contained"} onClick={this.onSave}>
                    حفظ
                </Button>
                <br/><br/>
                {
                    this.props.loading && <LinearProgress/>
                }
                <br/><br/>
            </div>
        )
    }

    private onSave = () => {
        if (!this.validate() || !this.file) {
            return;
        }
        const values = this.getValues();
        const data = new FormData();
        data.append("filename", values["filename"]);
        data.append("file", this.file);
        this.props.onUpload(data);
    }


}