import * as React from "react";
import {Divider, Typography} from "@material-ui/core";
import {connect} from "react-redux";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import UploadFileFormContainer from "./UploadFileForm";
import {ReduxStore} from "../../../bootstrap/store";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import LEAComponent, {LEAComponentProps} from "../../../my_framework/component/networking/LEAComponent";
import AttachmentsList from "./AttachmentsList";
import EmptyMessageBox from "../../../my_framework/ui/utils_componenet/EmptyMessageBox";

interface Props extends LEAComponentProps {
    files: any[];
    displayUploadForm?: boolean;
    employeeId: number;
}

class EmployeeAttachments extends LEAComponent<Props> {

    static defaultProps = {displayUploadForm: true};

    protected dispatchComponentOnWillMount() {
        const params = {employeeId: this.props.employeeId};
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.GET, API_ROUTES.PROFILE.ATTACHMENT.EMPLOYEE_ATTACHMENTS, Actions.EMPLOYEE_ATTACHMENTS, params);
    }

    protected renderContent(): any {
        return (
            <div className={"attachments"}>
                {
                    this.props.displayUploadForm &&
                    <UploadFileFormContainer/>
                }
                <Typography variant={"subheading"}>الملفات المرفقة : </Typography>
                <Divider/>
                <br/>
                <AttachmentsList onLengthZeroRender={() => <EmptyMessageBox/>} files={this.props.files}/>
            </div>
        )
    }

}

export default connect(
    (store: ReduxStore) => ({
        loading: store.EmployeeAttachments.loading,
        error: store.EmployeeAttachments.error,
        files: store.EmployeeAttachments.array
    })
)(EmployeeAttachments);