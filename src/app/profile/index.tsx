import * as React from "react";
import ExactPathRouterComponent from "../../my_framework/navigation/ExactPathRouterComponent";
import MyProfile from "./ProfileInfo";
import MyAttachments from "./EmployeeAttachments/MyAttachments"

export default class ProfileRouter extends ExactPathRouterComponent {
    protected routes(): object {
        return {
            "/profile": <MyProfile/>,
            "/profile/attachments": <MyAttachments/>
        };
    }
}