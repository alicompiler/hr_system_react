import * as React from "react";
import {connect} from "react-redux";
import URLs, {API_ROUTES} from "./../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import ProfileForm from "./ProfileForm";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import LESComponent, {LESComponentProps} from "../../../my_framework/component/networking/LESComponent";
import {Divider} from "@material-ui/core";
import ChangeImageForm from "./ChangeImageForm";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";
import {objectConnectMapper, successConnectMapper} from "../../../my_framework/data/redux/ConnectMapper";
import {ReduxStore} from "../../../bootstrap/store";
import EmployeeExtraInfo from "../../employee_extra/EmployeeExtraInfo";

interface Props extends LESComponentProps {
    info: any;
    updating: boolean;
    errorUpdating: boolean;

    [propName: string]: any;
}

class MyProfile extends LESComponent<Props> {


    protected dispatchComponentOnWillMount(): void {
        const url = URLs.getApiUrl(API_ROUTES.PROFILE.MY_INFO);
        const action = HttpGetAction(Actions.FETCH_MY_PROFILE, url);
        this.props.dispatch(action);
    }

    protected getPropsForSuccessStateActionHandler(): any {
        return {
            error: this.props.errorUpdating,
            success: this.props.success,
            dispatch: this.props.dispatch
        };
    }

    protected setupSuccessStateActionHandler(handler: SuccessStateActionHandler) {
        handler.afterSuccess = () => this.props.dispatch(HttpResetAction(Actions.CHANGE_PROFILE_IMAGE));
    }

    protected renderContent(): any {
        return <div>
            <ProfileForm info={this.props.info} updating={this.props.updating} onSave={this.onFileChange}/>
            <br/><Divider/><br/>
            <ChangeImageForm loading={this.props.updating}
                             onChange={this.onFileChange}
                             image={this.props.info.profile.image}/>
            <br/>
            <EmployeeExtraInfo/>
        </div>
    }

    private onFileChange = (file: any) => {
        if (!file) return;
        const data = new FormData();
        data.append("image", file);
        this.dispatchChangeProfileImageAction(data);
    };

    private dispatchChangeProfileImageAction = (data: any) => {
        const dispatcher = new HttpActionWithFileDispatcher(API_ROUTES.PROFILE.CHANGE_PROFILE_IMAGE, Actions.CHANGE_PROFILE_IMAGE, data);
        dispatcher.dispatch(this.props.dispatch);
    };

}

export default connect((store: ReduxStore) => {
    return {
        ...objectConnectMapper(store.MyProfile, "info"),
        ...successConnectMapper(store.ChangeProfileImage, "success", "updating", "errorUpdating")
    }
})(MyProfile);