import * as React from "react";
import TextField from "./../../../utils/ui/components/form/components/TextField";

interface Props {
    info: any;
    updating: boolean;
    onSave: (file: any) => void;
}


export default class ProfileForm extends React.Component <Props> {


    render() {
        if (!this.props.info.profile) {
            return <div/>
        }
        return <div>
            <TextField
                label={"الاسم الكامل"}
                value={this.valueOrDash(this.props.info.profile.name)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"الهاتف"}
                value={this.valueOrDash(this.props.info.profile.phone)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"البريد الالكتروني"}
                value={this.valueOrDash(this.props.info.profile.email)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"الهاتف البديل"}
                value={this.valueOrDash(this.props.info.profile.otherPhone)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"البريد البديل"}
                value={this.valueOrDash(this.props.info.profile.otherEmail)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"العنوان الوظيفي"}
                value={this.valueOrDash(this.props.info.profile.jobTitle)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"القسم"}
                value={this.valueOrDash(this.props.info.profile.department)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"العنوان"}
                value={this.valueOrDash(this.props.info.profile.address)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"الراتب"}
                value={this.valueOrDash(this.props.info.profile.salary)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"تاريخ المباشرة في العمل"}
                value={this.valueOrDash(this.props.info.profile.joinDate)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            <TextField
                label={"تاريخ الميلاد"}
                value={this.valueOrDash(this.props.info.profile.birthDate)}
                xs={12} sm={12} md={6} lg={6}
                readOnly
            />
            {
                this.props.info.balance &&
                <>
                    <TextField
                        label={"رصيد الاجازات (الشفتات)"}
                        value={this.valueOrDash(this.props.info.balance.shiftBalance)}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                    <TextField
                        label={"رصيد الاجازات (الدقائق)"}
                        value={this.valueOrDash(this.props.info.balance.minuteBalance)}
                        xs={12} sm={12} md={6} lg={6}
                        readOnly
                    />
                </>
            }
        </div>;
    }


    private valueOrDash = (value: any) => {
        return value ? value : "-";
    };
}