import * as React from "react";
import {CircularProgress, Typography} from "@material-ui/core";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import URLs from "../../../utils/helpers/URLs";

interface Props {
    loading: boolean;
    onChange: (file: any) => void;
    image: string;
}

interface State {
    image: any;
}

export default class ChangeImageForm extends React.Component<Props, State> {


    constructor(props: Props) {
        super(props);
        const imageUrl = URLs.getImage("profile_images", props.image ? props.image : "default_image.jpg");
        this.state = {image: imageUrl};
    }

    render() {
        const image = this.state.image ? this.state.image : "";
        return (
            <div>
                <Typography style={{margin: "8px 0"}} color={"textPrimary"} variant={"caption"}>الصورة
                    الشخصية</Typography>
                {
                    image &&
                    <img src={image} width={"256px"} height={"256px"}/>
                }
                {this.props.loading && <CircularProgress color={"primary"}/>}
                <br/>
                <FileInput disabled={this.props.loading} validTypes={["image/jpeg", "image/jpg", "image/png"]}
                           title={"تغير الصورة"}
                           onChange={this.onFileChange}/>

            </div>
        )
    }

    private onFileChange = (file: any) => {
        if (!file) {
            return;
        }
        let reader = new FileReader();
        reader.onloadend = () => {
            this.setState({
                image: reader.result
            });
        };
        reader.readAsDataURL(file);
        this.props.onChange(file);
    };

}