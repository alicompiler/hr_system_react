import MUITableComponent from "../../../utils/ui/components/table/mui-table/MUITableComponent";
import {RouteComponentProps} from "react-router";
import {getStatusColor, getStatusText} from "../../leave/Types/Status";
import StatusCell from "../../components/StatusCell";
import * as React from "react";

interface Props {
    data: any[];
    route: RouteComponentProps;
    title: string;
    toManage?: boolean;
}

export default class Table extends MUITableComponent<Props> {

    getColumns(): any[] {
        return [
            "صاحب الطلب", "المبلغ", "تاريخ الطلب",
            {
                name: "الحالة", options: {
                    customBodyRender: (item: any) => {
                        const {color, background} = getStatusColor(item);
                        const text = getStatusText(item);
                        return <StatusCell text={text} color={color} background={background}/>
                    }
                }
            }

        ];
    }

    getData(): any[][] {
        return this.props.data.map(item => [item.employeeName, item.amount, item.date, item.status]);
    }

    getOptions() {
        const options = super.getOptions();
        options["onRowClick"] = (data: any, meta: any) => {
            const id = this.props.data[meta.rowIndex].id;
            this.props.route.history.push(`/loan/sponsor/${id}`);
        };
        return options;
    }

    getTitle(): string {
        return this.props.title;
    }

}