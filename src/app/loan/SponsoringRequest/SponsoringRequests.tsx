import * as React from "react";
import {Divider, Typography} from "@material-ui/core";
import Table from "./Table";
import {RouteComponentProps} from "react-router";

interface Props {
    sponsoringRequests: any[];
    route: RouteComponentProps;
}

export default class SponsoringRequests extends React.Component<Props> {

    render() {
        return (
            <div>
                <Typography variant={"headline"}>طلبات الكفالة : </Typography>
                <Divider/>
                <br/>
                <Table route={this.props.route} data={this.props.sponsoringRequests} title={"طلبات الكفالة"}/>
            </div>
        )
    }

}