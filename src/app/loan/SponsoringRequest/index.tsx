import * as React from "react";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {RouteComponentProps} from "react-router";
import {LinearProgress} from "@material-ui/core";
import ErrorLoading from "../../main/home/components/ErrorLoading";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import SponsoringRequests from "./SponsoringRequests";

interface Props extends NetworkingState {
    sponsoringRequests: any[];
    route: RouteComponentProps;
    nullWhenEmpty?: boolean;
    dispatch: (action: any) => void;
}

class SponsoringRequestsContainer extends React.Component<Props> {
    static defaultProps = {nullWhenEmpty: true};

    componentDidMount() {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.SPONSORING_REQUESTS);
        const action = HttpGetAction(Actions.FETCH_SPONSORING_REQUESTS, url);
        this.props.dispatch(action);
    }

    render() {
        if (this.props.loading) {
            return <LinearProgress color={"primary"}/>
        } else if (this.props.error) {
            return <ErrorLoading/>
        } else if (this.props.nullWhenEmpty && this.props.sponsoringRequests.length == 0) {
            return null;
        }

        return (
            <SponsoringRequests sponsoringRequests={this.props.sponsoringRequests} route={this.props.route}/>
        )
    }

}

export default connect((store: any) => {
    return {
        loading: store.SponsoringRequests.loading,
        error: store.SponsoringRequests.error,
        sponsoringRequests: store.SponsoringRequests.array,
    }
})(SponsoringRequestsContainer);