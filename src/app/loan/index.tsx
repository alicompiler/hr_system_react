import * as React from "react";
import {Route, Switch} from "react-router";
import LoanRequestFormContainer from "./LoanRequestForm";
import SponsoringRequestsContainer from "./SponsoringRequest";
import Main from "./Main";
import AcceptanceForm from "./SponsoringForm";
import ManageLoanRequest from "./ManageLoanRequest";
import CompletedLoans from "./CompletedLoans/CompletedLoans";
import AllLoans from "./Main/All";
import Session from "../../utils/helpers/Session";

interface Props {
}

export default class Loan extends React.Component<Props> {

    render() {
        const session = Session.getUserObject();
        return (
            <Switch>
                <Route exact path={"/loan"} component={(route: any) => <Main route={route}/>}/>
                <Route exact path={"/loan/sponsor/:id(\\d+)"}
                       component={(route: any) => <AcceptanceForm loanRequestId={route.match.params.id}
                                                                  route={route}/>}/>
                <Route exact path={"/loan/:id(\\d+)"}
                       component={(route: any) => <ManageLoanRequest loanRequestId={route.match.params.id}
                                                                     route={route}/>}/>
                {
                    ((session.isManager && session.departmentId == 8) || session.userType == "admin") &&
                    <Route exact path={"/loan/completed"} component={(route: any) => <CompletedLoans route={route}/>}/>
                }
                <Route exact path={"/loan/all"} component={(route: any) => <AllLoans route={route}/>}/>
                <Route exact path={"/loan/send"} component={() => <LoanRequestFormContainer/>}/>
                <Route exact path={"/loan/sponsoring-requests"}
                       component={(route: any) => <SponsoringRequestsContainer route={route}/>}/>
            </Switch>
        )
    }

}
