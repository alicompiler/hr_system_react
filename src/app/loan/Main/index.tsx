import * as React from "react";
import SponsoringRequestsContainer from "../SponsoringRequest";
import {RouteComponentProps} from "react-router";
import MyLoanRequestsContainer from "../LoanRequest/MyLoanRequests";
import WaitingLoanRequestsContainer from "../LoanRequest/WaitingLoanRequests";
import Header from "../../components/Header";
import ActionBar from "../../components/ActionBar";
import {Button} from "@material-ui/core";
import Session from "../../../utils/helpers/Session";

interface Props {
    route: RouteComponentProps;
}

export default class index extends React.Component<Props> {

    render() {
        const session = Session.getUserObject();
        return (
            <div>
                <Header title={"السلف / الرئيسية"}/>
                <ActionBar changeDisplayType={true}
                           action actionTitle={"ارسال طلب"} actionLink={"/loan/send"}
                />
                <br/>

                {
                    session.userType === "admin" &&
                    <div style={{textAlign: "left"}}>
                        <Button onClick={() => this.props.route.history.push("/loan/all")} color={"primary"}>
                            كل الطلبات
                        </Button>
                    </div>
                }
                <br/>

                <SponsoringRequestsContainer route={this.props.route}/>
                <br/><br/>
                <MyLoanRequestsContainer route={this.props.route}/>
                <br/><br/>
                <WaitingLoanRequestsContainer route={this.props.route}/>
            </div>
        )
    }

}