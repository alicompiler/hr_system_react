import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {IconButton, ListItem, ListItemText} from "@material-ui/core";
import {CloudDownload} from "@material-ui/icons";
import Session from "../../../utils/helpers/Session";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";

interface Props {
    values: any;
}

export default class ReadOnlyLoanRequestForm extends Form<Props> {

    render() {
        return (
            <div>
                <TextField
                    label={"صاحب الطلب"}
                    value={this.getValueOrDash("employeeName")}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                />
                <TextField
                    label={"الكفيل"}
                    value={this.getValueOrDash("sponsorName")}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                />
                <br/>
                <TextField
                    label={"المبلغ"}
                    value={this.getValueOrDash("amount")}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                />
                <br/>
                <TextField
                    label={"الاشهر"}
                    value={this.getValueOrDash("months")}
                    type={"months"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                />
                <br/>
                <TextField
                    value={this.getValueOrDash("purpose")}
                    label={"الغرض"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                    rows={5} maxRows={5} multiline={true}
                    validationRules={{length: {minimum: 6}}}
                />
                <div>
                    <ListItem style={{textAlign: "right", padding: 0}}>
                        <ListItemText>
                            <IconButton component={"a"}
                                        onClick={() => this.download()}><CloudDownload/></IconButton>
                            {this.props.values.file}
                        </ListItemText>
                    </ListItem>
                </div>
                <br/><br/>
            </div>
        )
    }

    private download = () => {
        const session = Session.getUserObject().session;
        const url = URLs.getApiUrl(API_ROUTES.LOAN.DOWNLOAD_FILE.with(this.props.values.id, session));
        window.open(url, "_blank");
    };

    private getValueOrDash(key: string) {
        if (this.props.values[key] !== null && this.props.values[key] !== undefined)
            return this.props.values[key];
        return "-";
    }


}