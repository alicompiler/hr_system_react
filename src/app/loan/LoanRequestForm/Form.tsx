import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import Select from "../../../utils/ui/components/form/components/Select";
import TextField from "../../../utils/ui/components/form/components/TextField";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import {objectToFormData} from "../../../utils/helpers/DataUtils";
import {Button} from "@material-ui/core";
import EmployeeListToSelectOptionsAdapter from "../../employee/utils/EmployeeListToSelectOptionsAdapter";

interface Props {
    onSave: (values: FormData) => void;
    employees: any[];
}

export default class LoanRequestForm extends Form<Props> {
    private file: any;

    render() {
        const employeesOptions = new EmployeeListToSelectOptionsAdapter(this.props.employees).toSelectOptions();
        return (
            <div>
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"sponsor_id"}
                    options={employeesOptions}
                    placeholder={"الكفيل"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                    validationRules={{numericality: {greaterThan: 0}}}
                />
                <br/>
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"amount"}
                    placeholder={"المبلغ"}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                    validationRules={{numericality: true}}
                />
                <br/>
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"months"}
                    placeholder={"عدد الاشهر"}
                    type={"number"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                    validationRules={{numericality: true}}
                />
                <br/>
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"purpose"}
                    placeholder={"الغرض من القرض"}
                    xs={12} sm={12} md={6} lg={6} xl={6}
                    rows={5} maxRows={5} multiline={true}
                    validationRules={{length: {minimum: 6}}}
                />
                <br/><br/>
                <FileInput title={"رجاءا ارفق بطاقتك الشخصية"} onChange={file => this.file = file}/>
                <br/><br/>
                <Button onClick={this.onSave} color={"primary"} variant={"contained"}>
                    ارسال
                </Button>
            </div>
        )
    }

    private onSave = () => {
        if (!this.validate() || !this.file)
            return;
        const values = this.getValues();
        const data = objectToFormData(values);
        data.append("file", this.file);
        this.props.onSave(data);
    }

}