import * as React from "react";
import LoanRequestForm from "./Form";
import {connect} from "react-redux";
import {dispatchFetchEmployeeListAction} from "../../employee/utils/employeeActions";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpPostAction} from "../../../utils/redux++wrapper/Actions";

interface Props extends NetworkingState {
    loadingEmployees: boolean;
    errorEmployees: boolean;
    employees: any[];
    dispatch: (action: any) => void;
}

class LoanRequestFormContainer extends React.Component<Props> {

    private form: LoanRequestForm | null;

    componentDidMount() {
        dispatchFetchEmployeeListAction(this.props.dispatch);
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.SEND_LOAN_REQUEST));
            this.form && this.form.clearValues();
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <LoanRequestForm ref={ref => this.form = ref} employees={this.props.employees} onSave={this.onSave}/>
            </div>
        )
    }

    private onSave = (data: FormData) => {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.SEND);
        const options = {headers: {'Content-Type': 'multipart/form-data'}};
        const action = HttpPostAction(Actions.SEND_LOAN_REQUEST, url, data, options);
        this.props.dispatch(action);
    }
}

export default connect((store: any) => {
    return {
        loading: store.SendLoanRequest.loading,
        error: store.SendLoanRequest.error,
        success: store.SendLoanRequest.success,
        loadingEmployees: store.EmployeeList.loading,
        errorEmployees: store.EmployeeList.error,
        employees: store.EmployeeList.array
    }
})(LoanRequestFormContainer)