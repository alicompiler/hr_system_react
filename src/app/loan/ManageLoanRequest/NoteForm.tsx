import * as React from "react";
import {Button, Divider, Grid, Typography} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {LeaveRequestStatus} from "../../leave/Types/Status";
import Form from "../../../utils/ui/components/form/Form";
import ReadOnlyLoanRequestForm from "../LoanRequestForm/ReadOnlyForm";
import NotesList from "../../leave/SingleRequest/NotesList";

interface Props {
    values: any;
    loading: boolean;
    canManage: boolean;
    loanRequestId: number;
    notes: any[];

    onSend: (values: any) => void;
}

export default class NoteForm extends Form<Props> {

    render() {
        return (
            <div>
                <ReadOnlyLoanRequestForm values={this.props.values}/>
                <br/><br/>
                <NotesList notes={this.props.notes}/>
                {
                    this.props.canManage &&
                    <>
                        <Typography variant={"subheading"} color={"primary"}>
                            ارسال الملاحظات
                        </Typography>
                        <Divider/>
                        <Grid container>
                            <TextField
                                ref={(ref: any) => this.pushElementRef(ref)} name={"note"}
                                placeholder={"الملاحظات"} xs={12} multiline rows={5} rowsMax={5}
                                validationRules={{length: {minimum: 5}}}
                            />
                        </Grid>
                        <Button color={"primary"} onClick={() => this.onSend(LeaveRequestStatus.DONE)}
                                style={{width: 120}}>قبول
                            الطلب</Button>
                        <Button color={"primary"} onClick={() => this.onSend(LeaveRequestStatus.REJECTED)}
                                style={{width: 120}}>رفض</Button>
                    </>
                }
            </div>
        )
    }

    private onSend = (status: number) => {
        if (!this.validate()) {
            return;
        }
        const note = this.getValues()["note"];
        const values = {note: note, status: status, loanRequestId: this.props.loanRequestId};
        this.props.onSend(values);
    }

}