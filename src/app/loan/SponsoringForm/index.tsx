import * as React from "react";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {LinearProgress, Typography} from "@material-ui/core";
import AcceptanceForm from "./AcceptanceForm";
import {connect} from "react-redux";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import {RouteComponentProps} from "react-router";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    success: boolean;
    loanRequestId: number;
    loadingLoanRequest: boolean;
    errorLoanRequest: boolean;
    loanRequest: any;
    canManage: boolean;
    route: RouteComponentProps;
    dispatch: (action: any) => void;
}

class SponsoringAcceptanceForm extends React.Component<Props> {

    componentWillMount() {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.SINGLE_LOAN_REQUEST.with(this.props.loanRequestId));
        const action = HttpGetAction(Actions.FETCH_LOAN_REQUEST, url);
        this.props.dispatch(action);
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.SEND_ACCEPTANCE_STATUS));
            this.props.route.history.push("/loan");
        };
        handler.handleSuccessResponse();
    }

    render() {
        if (this.props.loadingLoanRequest) {
            return <LinearProgress color={"primary"}/>
        } else if (this.props.errorLoanRequest) {
            return <Typography color={"error"}>Error Happened</Typography>
        } else if (!this.props.loanRequest) {
            return null;
        }

        return (
            <AcceptanceForm canManage={this.props.canManage} onSend={this.onSend} values={this.props.loanRequest}
                            loading={this.props.loading}/>
        )
    }

    private onSend = (status: number) => {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.SEND_SPONSORING_ACCEPTANCE_STATUS);
        const action = HttpPostAction(Actions.SEND_ACCEPTANCE_STATUS, url, {
            loanRequestId: this.props.loanRequestId,
            status: status
        });
        this.props.dispatch(action);
    }

}

export default connect((store: any) => {
    return {
        loadingLoanRequest: store.LoanRequest.loading,
        errorLoanRequest: store.LoanRequest.error,
        loanRequest: store.LoanRequest.object.loanRequest,
        canManage: store.LoanRequest.object.canManage,
        loading: store.SendSponsorAcceptanceStatus.loading,
        error: store.SendSponsorAcceptanceStatus.error,
        success: store.SendSponsorAcceptanceStatus.success,
    }
})(SponsoringAcceptanceForm);