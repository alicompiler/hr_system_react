import * as React from "react";
import ReadOnlyLoanRequestForm from "../LoanRequestForm/ReadOnlyForm";
import {Button, LinearProgress, Typography} from "@material-ui/core";
import {AcceptanceStatus} from "../../common/AcceptanceStatus";

interface Props {
    values: any;
    loading: boolean;
    canManage: boolean;
    onSend: (status: number) => void;
}

export default class AcceptanceForm extends React.Component<Props> {

    render() {
        return (
            <div>
                <ReadOnlyLoanRequestForm values={this.props.values}/>
                <br/>
                {
                    this.props.canManage &&
                    <>
                        <Typography color={"primary"}>هل توافق على قبول الكفالة :</Typography>
                        <div>
                            <Button onClick={() => this.props.onSend(AcceptanceStatus.DONE)}
                                    disabled={this.props.loading}
                                    color={"primary"}>نعم
                                اوافق</Button>
                            <Button onClick={() => this.props.onSend(AcceptanceStatus.REJECTED)}
                                    disabled={this.props.loading}
                                    color={"primary"}>لا
                                اوافق</Button>
                        </div>
                    </>
                }
                {
                    this.props.loading && <LinearProgress color={"primary"}/>
                }
                <br/>
            </div>
        )
    }

}