import * as React from "react";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {RouteComponentProps} from "react-router";
import {Divider, LinearProgress, Typography} from "@material-ui/core";
import ErrorLoading from "../../main/home/components/ErrorLoading";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import Table from "../LoanRequest/Table";


interface Props extends NetworkingState {
    requests: any[];
    route: RouteComponentProps;
    nullWhenEmpty: boolean;
    dispatch: (action: any) => void;
}

class CompletedLoans extends React.Component<Props> {

    static defaultProps = {nullWhenEmpty: true};

    componentDidMount() {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.COMPLETED_LOANS);
        const action = HttpGetAction(Actions.FETCH_COMPLETED_LOANS, url);
        this.props.dispatch(action);
    }

    render() {
        if (this.props.loading) {
            return <LinearProgress color={"primary"}/>
        } else if (this.props.error) {
            return <ErrorLoading/>
        } else if (this.props.nullWhenEmpty && this.props.requests.length == 0) {
            return null;
        }

        return (
            <div>
                <Typography variant={"headline"}>طلبات السلف المكتملة : </Typography>
                <Divider/>
                <br/>
                <Table route={this.props.route} data={this.props.requests} title={"طلبات السلف المكتملة"}/>
            </div>
        )
    }

}

export default connect((store: any) => {
    return {
        loading: store.CompletedLoans.loading,
        error: store.CompletedLoans.error,
        requests: store.CompletedLoans.array,
    }
})(CompletedLoans);