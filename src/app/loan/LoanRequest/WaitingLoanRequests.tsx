import * as React from "react";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {RouteComponentProps} from "react-router";
import {LinearProgress} from "@material-ui/core";
import ErrorLoading from "../../main/home/components/ErrorLoading";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import LoanRequests from "./LoanRequests";

interface Props extends NetworkingState {
    requests: any[];
    route: RouteComponentProps;
    nullWhenEmpty: boolean;
    dispatch: (action: any) => void;
}

class WaitingLoanRequestsContainer extends React.Component<Props> {

    static defaultProps = {nullWhenEmpty: true};

    componentDidMount() {
        const url = URLs.getApiUrl(API_ROUTES.LOAN.WAITING_LOAN_REQUEST);
        const action = HttpGetAction(Actions.FETCH_WAITING_LOAN_REQUESTS, url);
        this.props.dispatch(action);
    }

    render() {
        if (this.props.loading) {
            return <LinearProgress color={"primary"}/>
        } else if (this.props.error) {
            return <ErrorLoading/>
        } else if (this.props.nullWhenEmpty && this.props.requests.length == 0) {
            return null;
        }
        return (
            <div>
                <LoanRequests title={"طلبات السلف - في الانتضار"} requests={this.props.requests}
                              route={this.props.route}/>
            </div>
        )
    }

}

export default connect((store: any) => {
    return {
        loading: store.WaitingLoanRequests.loading,
        error: store.WaitingLoanRequests.error,
        requests: store.WaitingLoanRequests.array,
    }
})(WaitingLoanRequestsContainer);