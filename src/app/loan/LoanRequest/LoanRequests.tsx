import * as React from "react";
import {Divider, Typography} from "@material-ui/core";
import Table from "./Table";
import {RouteComponentProps} from "react-router";

interface Props {
    requests: any[];
    title: string;
    route: RouteComponentProps;
}

export default class LoanRequests extends React.Component<Props> {

    render() {
        return (
            <div>
                <Typography variant={"headline"}>{this.props.title} : </Typography>
                <Divider/>
                <br/>
                <Table route={this.props.route} data={this.props.requests} title={this.props.title}/>
            </div>
        )
    }

}