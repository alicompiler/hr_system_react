import {Divider, Typography} from "@material-ui/core";
import * as React from "react";

interface Props {
    title: string;
}

export default class Header extends React.Component<Props> {
    render() {
        return (
            <>
                <Typography variant={"subheading"}>{this.props.title}</Typography>
                <Divider/>
                <br/>
            </>
        )
    }
}