import * as React from "react";

interface Props {
    text: string;
    color?: string;
    background?: string;
}

export default class StatusCell extends React.Component<Props> {

    static defaultProps: Props = {text: "", background: "#607d8b", color: "#FFF"};

    render() {
        return (
            <div style={{
                width: 120, borderRadius: 20, fontSize: 12, textAlign: "center",
                background: this.props.background,
                color: this.props.color
            }}>
                {this.props.text}
            </div>
        )
    }

}