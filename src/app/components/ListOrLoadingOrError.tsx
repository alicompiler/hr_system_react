import * as React from "react";
import {ReactNode} from "react";
import Loading from "./Loading";
import Error from "./Error";
import EmptyList from "./EmptyList";

interface Props {
    listComponent: ReactNode;
    loadingMessage?: string;
    loading: boolean;
    errorMessage?: string;
    error: boolean | null;
    displayEmpty?: boolean;
    emptyComponent?: ReactNode;
}

export default class ListOrLoadingOrError extends React.Component<Props> {

    static defaultProps: Props = {
        listComponent: <></>,
        loading: false,
        error: false,
        emptyComponent: <EmptyList/>
    };

    render() {
        if (this.props.loading)
            return <Loading loading message={this.props.loadingMessage}/>;
        else if (this.props.error)
            return <Error error message={this.props.errorMessage}/>;
        if (this.props.displayEmpty) {
            return this.props.emptyComponent;
        }
        return this.props.listComponent
    }

}