import * as React from "react";
import TextField from "../../utils/ui/components/form/components/TextField";
import {Button, IconButton} from "@material-ui/core";
import {Link} from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import ViewListIcon from "@material-ui/icons/ViewList";

interface Props {
    changeDisplayType?: boolean;
    onChangeDisplayType?: (type: string) => void;
    onSearch?: (query: string) => void;
    action?: boolean;
    actionTitle? : string;
    actionLink?: string;
}

export default class ActionBar extends React.Component<Props> {
    static defaultProps : Props;
    render() {
        return (
            <div className={"search-bar"}>

                {
                    this.props.onSearch &&
                    <TextField
                        md={6} sm={12}
                        adornment={<SearchIcon/>} adornmentClassName={"ar-adornment"} adornmentPosition={"start"}
                        placeholder={"بحث..."} name={"search"} onEnter={this.props.onSearch}
                    />
                }

                <div className="search-bar-actions">
                    {
                        this.props.changeDisplayType &&
                        <>
                            <IconButton onClick={this.onSetDisplayTypeToList}><ViewListIcon/></IconButton>
                            <IconButton onClick={this.onSetDisplayTypeToGrid}><ViewModuleIcon/></IconButton>
                        </>
                    }
                    {
                        this.props.action &&
                        <Link to={(this.props.actionLink as string)}>
                            <Button variant={"contained"} color={"primary"}>{this.props.actionTitle}</Button>
                        </Link>
                    }
                </div>
            </div>
        );
    }

    private onSetDisplayTypeToGrid = () => {
        this.props.onChangeDisplayType && this.props.onChangeDisplayType("grid")
    };

    private onSetDisplayTypeToList = () => {
        this.props.onChangeDisplayType && this.props.onChangeDisplayType("list")
    };
}


ActionBar.defaultProps = {
    actionTitle: "اضافة جديد"
};