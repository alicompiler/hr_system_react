import * as React from "react";
import {ErrorOutline} from "@material-ui/icons";
import {Typography} from "@material-ui/core";

interface Props {
    message?: string;
}

export default class EmptyList extends React.Component<Props> {

    static defaultProps: Props = {
        message: "لا توجد نتائج"
    };

    render() {
        return (
            <div style={{padding: 16, textAlign: "center"}}>
                <ErrorOutline style={{fontSize: 28, color: "#111"}}/>
                <br/>
                <Typography variant={"title"}>{this.props.message}</Typography>
            </div>
        )
    }

}