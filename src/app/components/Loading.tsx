import * as React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Typography} from "@material-ui/core";

interface Props {
    message?: string;
    loading?: boolean;
}

export default class Loading extends React.Component<Props> {
    static defaultProps: Props = {loading: true};

    render() {
        if (!this.props.loading) return null;
        return (
            <div style={{padding: 40, textAlign: "center"}}>
                <CircularProgress color={"secondary"}/>
                {this.props.message && <Typography variant={"subheading"}>{this.props.message}</Typography>}
            </div>
        );
    }
}