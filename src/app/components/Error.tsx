import * as React from "react";
import ErrorIcon from "@material-ui/icons/Error";
import {Color} from "../../bootstrap/theme";
import {Typography} from "@material-ui/core";

interface Props {
    message?: string;
    error?: boolean;
}

export default class Error extends React.Component<Props> {

    static defaultProps: Props = {error: true, message: "توجد مشكلة"};

    render() {
        if (!this.props.error) return null;
        return (
            <div style={{padding: 40, textAlign: "center"}}>
                <ErrorIcon style={{fontSize: 44, color: Color.error}}/>
                {
                    this.props.message &&
                    <Typography variant={"subheading"}>{this.props.message}</Typography>
                }
            </div>
        )
    }
}