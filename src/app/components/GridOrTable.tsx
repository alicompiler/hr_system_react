import * as React from "react";
import {ReactNode} from "react";

interface Props {
    display? : "grid" | "table";
    grid : ReactNode;
    table: ReactNode;
}

export default class GridOrTable extends React.Component<Props> {

    render() {
        if(this.props.display === "grid"){
            return this.props.table;
        }else{
            return this.props.grid;
        }
    }

}