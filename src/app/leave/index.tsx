import * as React from "react";
import {Route, Switch} from "react-router";
import SendLeaveRequest from "./Send";
import Main from "./Main/index";
import SingleLeaveRequest from "./SingleRequest";
import All from "./Main/All";
import LeaveReport from "./LeaveReport";
import Session from "../../utils/helpers/Session";
import BalanceReport from "./BalanceReport/BalanceReport";

interface Props {
}

export default class index extends React.Component<Props> {

    render() {
        const session = Session.getUserObject();
        return (
            <Switch>
                <Route exact path={"/leave-requests"} component={(route: any) => <Main route={route}/>}/>
                <Route exact path={"/leave-requests/balance-report"}
                       component={() => <BalanceReport />}/>
                {
                    (session.isManager === "true" || session.userType === "admin") &&
                    <Route exact path={"/leave-requests/manager-report"}
                           component={(route: any) => <LeaveReport route={route}/>}/>
                }
                <Route exact path={"/leave-requests/all"} component={(route: any) => <All route={route}/>}/>
                <Route exact path={"/leave-requests/my"} component={() => <h1>MY LEAVE REQUESTS</h1>}/>
                <Route exact path={"/leave-requests/send"} component={() => <SendLeaveRequest/>}/>
                <Route exact path={"/leave-requests/to-manage"} component={() => <h1>LEAVE REQUESTS TO MANAGE</h1>}/>
                <Route exact path={"/leave-requests/:id(\\d+)"}
                       component={(route: any) => <SingleLeaveRequest requestId={route.match.params.id}/>}/>
            </Switch>
        )
    }

}
