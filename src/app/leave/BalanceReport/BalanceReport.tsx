import * as React from "react";
import Header from "../../components/Header";
import {connect} from "react-redux";
import Form from "../../../utils/ui/components/form/Form";
import {LinearProgress, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";

interface Props {
    loading: boolean;
    error: boolean;
    balance: any[];
    dispatch: (action: any) => void;
}

class BalanceReport extends Form<Props> {

    componentWillMount() {
        new HttpActionDispatcher(HttpMethod.GET,
            API_ROUTES.LEAVE_REQUEST.BALANCE_REPORT, Actions.FETCH_LEAVE_BALANCE)
            .dispatch(this.props.dispatch);
    }

    render() {
        return <div>
            <Header title={"رصيد الاجازات"}/>
            {
                this.props.loading && <LinearProgress color={"primary"}/>
            }
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>الموظف</TableCell>
                            <TableCell>رصيد الشفتات</TableCell>
                            <TableCell>رصيد الدقائق</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {
                            this.props.balance.map((item: any, index: any) => {
                                return <TableRow key={index}>
                                    <TableCell>{item.employeeName}</TableCell>
                                    <TableCell>{item.shiftBalance}</TableCell>
                                    <TableCell>{item.minuteBalance}</TableCell>
                                </TableRow>
                            })
                        }
                    </TableBody>
                </Table>
            </Paper>
        </div>
    }
}


export default connect((store: any) => {
    return {
        loading: store.LeaveBalance.loading,
        error: store.LeaveBalance.error,
        balance: store.LeaveBalance.array
    }
})(BalanceReport)