import * as React from "react";
import Header from "../../components/Header";
import {Button, Divider, Grid} from "@material-ui/core";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import {connect} from "react-redux";
import Form from "../../../utils/ui/components/form/Form";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import Table from "../Main/Table";
import {RouteComponentProps} from "react-router";

interface Props {
    loading: boolean;
    error: boolean;
    leaves: any[];
    dispatch: (action: any) => void;
    route: RouteComponentProps;
}

class LeaveReport extends Form<Props> {
    render() {
        return <div>
            <Header title={"تقارير الاجازات"}/>
            <div>
                <Grid container spacing={16}>
                    <DatePicker
                        ref={ref => this.pushElementRef(ref)}
                        name={"fromDate"}
                        placeholder={"من تاريخ"}
                        xs={12} sm={12} md={4} lg={4}
                        validationRules={{presence: true, datetime: {dateOnly: true}}}
                    />
                    <DatePicker
                        ref={ref => this.pushElementRef(ref)}
                        name={"toDate"}
                        placeholder={"الى تاريخ"}
                        xs={12} sm={12} md={4} lg={4}
                        validationRules={{presence: true, datetime: {dateOnly: true}}}
                    />
                </Grid>
                <Button onClick={this.search} variant={"contained"}>بحث</Button>
            </div>

            <br/>
            <Divider/>
            <br/>


            <Table route={this.props.route} data={this.props.leaves} title={"تقرير الاجازات"}/>

        </div>
    }

    private search = () => {
        if (!this.validate()) {
            return;
        }
        const data = this.getValues();
        new HttpActionDispatcher(HttpMethod.GET,
            API_ROUTES.LEAVE_REQUEST.MANAGER_REPORT, Actions.LEAVE_REQUEST_MANAGER_REPORT,
            data).dispatch(this.props.dispatch);
    }
}


export default connect((store: any) => {
    return {
        loading: store.ManagerLeaveRequest.loading,
        error: store.ManagerLeaveRequest.error,
        leaves: store.ManagerLeaveRequest.array
    }
})(LeaveReport)