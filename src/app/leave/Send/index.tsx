import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import {Button, Grid, LinearProgress} from "@material-ui/core";
import Select from "../../../utils/ui/components/form/components/Select";
import {LEAVE_TYPE_OPTIONS} from "../Types/Type";
import FieldsBasedOnType from "./FieldsBasedOnType";
import TextField from "../../../utils/ui/components/form/components/TextField";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {Actions} from "../../../bootstrap/actions";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {connect} from "react-redux";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import {objectToFormData} from "../../../utils/helpers/DataUtils";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";

interface Props extends NetworkingState {
    success: boolean;
    dispatch: (action: any) => void;
}

interface State {
    type?: number;
    file: any;
}

class SendLeaveRequest extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {file: null};
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => this.props.dispatch(HttpResetAction(Actions.SEND_LEAVE_REQUEST));
        handler.afterSuccess = () => this.clearValues();
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <Grid container spacing={16}>
                    <Select
                        afterChange={e => this.setState({type: e.target.value})}
                        ref={ref => this.pushElementRef(ref)}
                        name={"type"}
                        placeholder={"نوع الاجازة"}
                        xs={12} sm={12} md={6} lg={6}
                        options={LEAVE_TYPE_OPTIONS}
                        style={{marginTop: 16}}
                        validationRules={{numericality: true}}
                    />
                </Grid>
                <br/>
                <Grid container spacing={16}>
                    <FieldsBasedOnType type={this.state.type} form={this}/>
                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        name={"reason"}
                        placeholder={"السبب"}
                        xs={12}
                        multiline rows={5} rowsMax={5}
                        validationRules={{length: {minimum: 5}}}
                    />
                </Grid>
                <br/>
                <FileInput title={"المرفقات"} onChange={(file: any) => this.setState({file: file})}/>
                <br/>
                <Button onClick={this.onSend} variant={"contained"} color={"primary"}>ارسال</Button>
                <br/>
                <br/>
                {this.props.loading && <LinearProgress color={"primary"}/>}
            </div>
        )
    }

    onSend = () => {
        if (this.hasErrors()) {
            this.validate();
            return;
        }

        const value = this.getValues();
        const formValues = objectToFormData(value);
        if (this.state.file)
            formValues.append("attachment", this.state.file);
        new HttpActionWithFileDispatcher(API_ROUTES.LEAVE_REQUEST.SEND, Actions.SEND_LEAVE_REQUEST,
            formValues).dispatch(this.props.dispatch);

    }

}

export default connect((store: any) => ({
    loading: store.SendLeaveRequest.loading,
    error: store.SendLeaveRequest.error,
    success: store.SendLeaveRequest.success
}))(SendLeaveRequest);