import * as React from "react";
import {ReactNode} from "react";
import Form from "../../../utils/ui/components/form/Form";
import {Grid, Typography} from "@material-ui/core";
import {LeaveRequestType} from "../Types/Type";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import TimePicker from "../../../utils/ui/components/form/components/TimePicker";
import Select from "../../../utils/ui/components/form/components/Select";
import {SHIFT_TYPE_OPTIONS} from "../Types/ShiftType";

interface Props {
    form: Form<any, any>;
    type?: number;
}

export default class FieldsBasedOnType extends React.Component<Props> {

    render() {
        return this.renderFieldsBasedOnType()
    }

    private renderFieldsBasedOnType = (): ReactNode => {
        switch (this.props.type) {
            case LeaveRequestType.ONE_DAY:
                return this.renderOneDayField();
            case LeaveRequestType.TIMELY:
                return this.renderTimelyFields();
            case LeaveRequestType.ONE_SHIFT:
                return this.renderOneShiftElement();
            case LeaveRequestType.SHORT:
            case LeaveRequestType.LONG:
                return this.renderBetweenDates();
            default:
                return this.renderNoOption();
        }
    };

    private renderNoOption() {
        return <Grid item xs={12}>
            <Typography variant={"headline"} color={"error"}>
                عليك اختيار نوع الاجازة
            </Typography>
        </Grid>
    }

    private renderBetweenDates() {
        return <>
            <DatePicker
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"fromDate"}
                placeholder={"من التاريخ"}
                xs={12} sm={12} md={6} lg={6}
                validationRules={{presence: true, datetime: {dateOnly: true}}}
            />
            <DatePicker
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"toDate"}
                placeholder={"الى التاريخ"}
                xs={12} sm={12} md={6} lg={6}
                validationRules={{presence: true, datetime: {dateOnly: true}}}
            />
        </>
    }

    private renderOneShiftElement() {
        return <>
            <DatePicker
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"date"}
                placeholder={"التاريخ"}
                xs={12} sm={12} md={6} lg={6}
                validationRules={{presence: true, datetime: {dateOnly: true}}}
            />
            <Select
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"shiftType"}
                placeholder={"نوع الشفت"}
                xs={12} sm={12} md={6} lg={6}
                options={SHIFT_TYPE_OPTIONS}
                style={{marginTop: 16}}
                validationRules={{presence: true, numericality: true}}
            />
        </>
    }

    private renderTimelyFields() {
        return <>
            <Grid xs={12}>
                <DatePicker
                    ref={ref => this.props.form.pushElementRef(ref)}
                    name={"date"}
                    placeholder={"التاريخ"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />
            </Grid>
            <TimePicker
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"fromTime"}
                placeholder={"من الساعة"}
                xs={12} sm={12} md={6} lg={6}
                validationRules={{presence: true, timeFromTimePicker: true}}
            />

            <TimePicker
                ref={ref => this.props.form.pushElementRef(ref)}
                name={"toTime"}
                placeholder={"الى الساعة"}
                xs={12} sm={12} md={6} lg={6}
                validationRules={{presence: true, timeFromTimePicker: true}}
            />
        </>
    }

    private renderOneDayField() {
        return <DatePicker
            ref={ref => this.props.form.pushElementRef(ref)}
            name={"date"}
            placeholder={"التاريخ"}
            xs={12} sm={12} md={6} lg={6}
            validationRules={{presence: true, datetime: {dateOnly: true}}}
        />
    }
}