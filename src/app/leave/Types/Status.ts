export enum LeaveRequestStatus {
    PROCESSING = 1,
    DONE,
    REJECTED
}

export function getStatusText(status: LeaveRequestStatus) {
    switch (status) {
        case LeaveRequestStatus.PROCESSING:
            return 'تحت المراجعة';
        case LeaveRequestStatus.DONE:
            return 'تمت الموافقة';
        case LeaveRequestStatus.REJECTED:
            return 'تم الرفض';
        default :
            return 'توجد مشكلة';
    }
}

export function getStatusColor(status: LeaveRequestStatus): { color: string, background: string } {
    switch (status) {
        case LeaveRequestStatus.PROCESSING:
            return {color: "#111", background: "#ffc107"};
        case LeaveRequestStatus.REJECTED:
            return {color: "#FFF", background: "#f44336"};
        case LeaveRequestStatus.DONE:
            return {color: "#FFF", background: "#4caf50"};
        default:
            return {color: "#FFF", background: "#424242"}
    }
}