import {ISelectOption} from "../../../utils/ui/components/form/components/Select";

export enum LeaveRequestType {
    ONE_DAY = 1,
    ONE_SHIFT,
    TIMELY,
    SHORT,
    LONG,
}

export function leaveRequestTypeAsText(type: LeaveRequestType) {
    switch (type) {
        case LeaveRequestType.ONE_DAY:
            return 'يوم واحد';
        case LeaveRequestType.ONE_SHIFT:
            return 'شفت واحد';
        case LeaveRequestType.TIMELY:
            return 'زمنية';
        case LeaveRequestType.SHORT:
            return 'قصيرة';
        case LeaveRequestType.LONG:
            return 'طويلة';
        default :
            return 'توجد مشكلة';
    }
}


export const LEAVE_TYPE_OPTIONS: ISelectOption[] = [
    {text: 'يوم واحد', value: LeaveRequestType.ONE_DAY},
    {text: 'شفت واحد', value: LeaveRequestType.ONE_SHIFT},
    {text: 'زمنية', value: LeaveRequestType.TIMELY},
    {text: 'اجازة قصيرة', value: LeaveRequestType.SHORT},
    {text: 'اجازة طويلة', value: LeaveRequestType.LONG},
];