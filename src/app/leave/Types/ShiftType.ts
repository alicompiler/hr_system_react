import {ISelectOption} from "../../../utils/ui/components/form/components/Select";

export enum ShiftType {
    DAY = 1,
    NIGHT,
}


export function getShiftTypeText(type: ShiftType) {
    switch (type) {
        case ShiftType.DAY:
            return 'صباحي';
        case ShiftType.NIGHT:
            return 'مسائي';
        default :
            return 'توجد مشكلة';
    }
}

export const SHIFT_TYPE_OPTIONS: ISelectOption[] = [
    {text: 'الصباحي', value: ShiftType.DAY},
    {text: 'المسائي', value: ShiftType.NIGHT},
];
