import * as React from "react";
import MUITableComponent from "../../../utils/ui/components/table/mui-table/MUITableComponent";
import StatusCell from "../../components/StatusCell";
import {getStatusColor, getStatusText} from "../Types/Status";
import {RouteComponentProps} from "react-router";
import {leaveRequestTypeAsText} from "../Types/Type";

interface Props {
    data: any[];
    route: RouteComponentProps;
    title: string;
    toManage?: boolean;
}

export default class Table extends MUITableComponent<Props> {

    getColumns(): any[] {
        return [
            {
                name: "الموظف",
                options: {
                    customBodyRender: (item: any) => {
                        return item.name;
                    }
                }
            }, {
                name: "نوع الاجازة",
                options: {
                    customBodyRender: (item: any) => {
                        return <span>{leaveRequestTypeAsText(item)}</span>
                    }
                }
            }
            , "تاريخ الطلب", "من", "الى",
            {
                name: "الحالة", options: {
                    customBodyRender: (item: any) => {
                        const {color, background} = getStatusColor(item);
                        const text = getStatusText(item);
                        return <StatusCell text={text} color={color} background={background}/>
                    }
                }
            }
        ];
    }

    getData(): any[][] {
        return this.props.data.map(item => [
            {id: item.id, name: item.name}, item.type, item.date, item.from, item.to, item.status
        ]);
    }

    getOptions() {
        const options = super.getOptions();
        options["onRowClick"] = (data: any, meta: any) => {
            const id = this.props.data[meta.rowIndex].id;
            this.props.route.history.push(`/leave-requests/${id}`);
        };
        return options;
    }

    getTitle(): string {
        return this.props.title;
    }

}