import * as React from "react";
import Table from "./Table";
import {RouteComponentProps} from "react-router";
import {connect} from "react-redux";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import URLs from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import Grid from "./Grid";
import ActionBar from "../../components/ActionBar";
import GridOrTable from "../../components/GridOrTable";
import Header from "../../components/Header";
import {Button} from "@material-ui/core";
import Session from "../../../utils/helpers/Session";

interface Props {
    myLeaveRequests: any[];
    route: RouteComponentProps;
    myRequestsLoading: boolean;
    myRequestsError: boolean;

    toManageRequestsLoading: boolean;
    toManageRequestsError: boolean;
    toManageRequests: any[];

    dispatch: (action: any) => void;
}

interface State {
    displayType: "grid" | "table";
}

class LeaveRequestMain extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {displayType: "table"};
    }


    componentDidMount() {
        const myRequestsUrl = URLs.getApiUrl("leave-requests/my");
        this.props.dispatch(HttpGetAction(Actions.FETCH_MY_LEAVE_REQUESTS, myRequestsUrl));
        const toManageRequestsUrl = URLs.getApiUrl("leave-requests/to-manage");
        this.props.dispatch(HttpGetAction(Actions.FETCH_LEAVE_REQUESTS_TO_MANAGE, toManageRequestsUrl));
    }

    render() {
        const session = Session.getUserObject();
        return (
            <div>
                <Header title={"الاجازات / الرئيسية"}/>
                <ActionBar changeDisplayType={true}
                           action actionTitle={"ارسال طلب"} actionLink={"/leave-requests/send"}
                           onChangeDisplayType={(type: "table" | "grid") => this.setState({displayType: type})}/>
                <br/>

                <div style={{textAlign: "left"}}>
                    {
                        session.userType === "admin" &&
                        <>
                            <Button color={"primary"}
                                    onClick={() => this.props.route.history.push("/leave-requests/all")}>كل
                                الطلبات</Button>
                            <Button color={"primary"}
                                    onClick={() => this.props.route.history.push("/leave-requests/balance-report")}>
                                رصيد الاجازات
                            </Button>
                        </>

                    }
                    {
                        session.isManager === "true" &&
                        <Button color={"primary"}
                                onClick={() => this.props.route.history.push("/leave-requests/manager-report")}>تقرير
                            الاجازات</Button>
                    }
                </div>
                <br/>

                <GridOrTable display={this.state.displayType}
                             grid={<Table title="طلبات انت ارسلتها" data={this.props.myLeaveRequests}
                                          route={this.props.route}/>}
                             table={<Grid data={this.props.myLeaveRequests}/>}/>
                <br/>
                <GridOrTable display={this.state.displayType}
                             grid={<Table title={"طلبات في انتظارك"} data={this.props.toManageRequests}
                                          route={this.props.route}/>}
                             table={<Grid data={this.props.toManageRequests}/>}/>
            </div>
        )
    }

}


export default connect((store: any) => ({
    myRequestsLoading: store.MyLeaveRequests.loading,
    myRequestsError: store.MyLeaveRequests.error,
    myLeaveRequests: store.MyLeaveRequests.array,

    toManageRequestsLoading: store.ToManageRequests.loading,
    toManageRequestsError: store.ToManageRequests.error,
    toManageRequests: store.ToManageRequests.array,

}))(LeaveRequestMain)