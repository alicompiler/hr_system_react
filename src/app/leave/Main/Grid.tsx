import * as React from "react";
import {Button, CardActions, Divider, Paper, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import StatusCell from "../../components/StatusCell";
import {getStatusColor, getStatusText} from "../Types/Status";
import MaterialGrid from "@material-ui/core/Grid";
import {leaveRequestTypeAsText} from "../Types/Type";

interface Props {
    data: any[];
}

export default class Grid extends React.Component<Props> {

    render() {
        return (
            <MaterialGrid container spacing={16}>
                {
                    this.props.data.map((item) => {
                        return <MaterialGrid item xs={12} md={4} lg={3}>
                            <GridItem item={item}/>
                        </MaterialGrid>
                    })
                }
            </MaterialGrid>
        )
    }

}

interface Props2 {
    item: any;
    manage?: boolean;
}

class GridItem extends React.Component<Props2> {
    render() {
        const typename = leaveRequestTypeAsText(this.props.item.type);
        const {color: statusColor, background: statusBackground} = getStatusColor(this.props.item.status);
        return (
            <Paper className="grid-item">
                <Typography variant={"subheading"} align={"center"}>{this.props.item.name}</Typography>
                <div style={{padding: 8, textAlign: "center", display: "flex", justifyContent: "center"}}>
                    <StatusCell text={getStatusText(this.props.item.status)} color={statusColor}
                                background={statusBackground}/>
                </div>
                <Divider/>
                <div className="details">
                    <Typography variant={"body1"}>النوع : {typename}</Typography>
                    <Typography variant={"body1"}>من : {this.props.item.from}</Typography>
                    <Typography variant={"body1"}>الى : {this.props.item.to}</Typography>
                </div>
                <Divider/>
                <CardActions>
                    <Link to={"/leave-requests/" + this.props.item.id}>
                        <Button size="small" color="primary">
                            عرض التفاصيل
                        </Button>
                    </Link>
                </CardActions>
            </Paper>
        )
    }
}