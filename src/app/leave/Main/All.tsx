import * as React from "react";
import Table from "./Table";
import {RouteComponentProps} from "react-router";
import {connect} from "react-redux";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import URLs from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import Grid from "./Grid";
import ActionBar from "../../components/ActionBar";
import GridOrTable from "../../components/GridOrTable";
import Header from "../../components/Header";
import Session from "../../../utils/helpers/Session";

interface Props {
    requests: any[];
    loading: boolean;
    error: boolean;
    route: RouteComponentProps;
    dispatch: (action: any) => void;
}

interface State {
    displayType: "grid" | "table";
}

class All extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {displayType: "table"};
    }


    componentDidMount() {
        const allLeaveRequestsURL = URLs.getApiUrl("leave-requests/all");
        this.props.dispatch(HttpGetAction(Actions.FETCH_ALL_LEAVE_REQUESTS, allLeaveRequestsURL));
    }

    render() {
        const session = Session.getUserObject();
        return (
            <div>
                <Header title={"الاجازات / الرئيسية"}/>
                <ActionBar changeDisplayType={true}
                           action actionTitle={"ارسال طلب"} actionLink={"/leave-requests/send"}
                           onChangeDisplayType={(type: "table" | "grid") => this.setState({displayType: type})}/>
                <br/>
                {
                    session.userType === "admin" &&
                    <GridOrTable display={this.state.displayType}
                                 grid={<Table title="كل طلبات الاجازة" data={this.props.requests}
                                              route={this.props.route}/>}
                                 table={<Grid data={this.props.requests}/>}/>
                }
            </div>
        )
    }

}


export default connect((store: any) => ({
    loading: store.AllLeaveRequests.loading,
    error: store.AllLeaveRequests.error,
    requests: store.AllLeaveRequests.array,
}))(All)