import * as React from "react";
import Header from "../../components/Header";
import For from "../../../lib/react-lang/For";
import If from "../../../lib/react-lang/If";
import {Divider, Paper, Typography} from "@material-ui/core";

interface Props {
    notes: any[];
}

export default class NotesList extends React.Component<Props> {
    render() {
        return (
            <If condition={this.props.notes && this.props.notes.length > 0}>
                <Header title={"ملاحظات"}/>
                <div>
                    <For collection={this.props.notes || []}
                         renderItem={(item: any, index: number) =>
                             <If key={index} condition={item.note.trim().length > 0}>
                                 <Paper key={index} style={{padding: 16, margin: "16px 0"}}>
                                     <Typography
                                         variant={"subheading"}>{item.departmentName} / {item.employeeName}</Typography>
                                     <Typography variant={"caption"} color={"primary"}>{item.time}</Typography>
                                     <Divider/>
                                     <div style={{height: 12}}/>
                                     <Typography variant={"body2"}>{item.note}</Typography>
                                 </Paper>
                             </If>
                         }/>
                </div>
            </If>
        )
    }
}