import * as React from "react";
import {Button, Grid, Typography} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import If from "../../../lib/react-lang/If";
import {LeaveRequestType, leaveRequestTypeAsText} from "../Types/Type";
import {ILeaveRequest} from "./ILeaveRequest";
import {getShiftTypeText} from "../Types/ShiftType";
import {AcceptanceStatus} from "../../common/AcceptanceStatus";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import Session from "../../../utils/helpers/Session";
import URLs from "../../../utils/helpers/URLs";

interface Props {
    leaveRequest: ILeaveRequest;
    balance: any;
    dispatch: (action: any) => void;
    deleting: boolean;
    errorDeleting: boolean;
    lastLeaveRequest: any;
    successDeleting: boolean;
}

class SingleRequestForm extends React.Component<Props> {
    componentDidUpdate() {
        const props = {
            dispatch: this.props.dispatch,
            loading: this.props.deleting,
            error: this.props.errorDeleting, success: this.props.successDeleting
        };
        const handler = new SuccessStateActionHandler(props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.DELETE_LEAVE_REQUEST))
        };
        handler.handleSuccessResponse();
    }

    render() {
        const typename = leaveRequestTypeAsText(this.props.leaveRequest.type);
        const session = Session.getUserObject();
        return (
            <>
                <Grid container spacing={16}>
                    <Grid item xs={12}>
                        <Typography variant={"headline"}>{this.props.leaveRequest.employeeName}</Typography>
                    </Grid>

                    <TextField
                        value={typename}
                        placeholder={"النوع"} label={"النوع"}
                        xs={12} md={6} lg={4}
                        readOnly={true}
                    />
                    <Grid item xs="auto" md={6} lg={8}/>

                    <TextField
                        value={this.props.leaveRequest.fromDate}
                        placeholder={"من تاريخ"} label={"من تاريخ"}
                        xs={12} md={6} lg={4}
                        readOnly={true}
                    />
                    <TextField
                        value={this.props.leaveRequest.toDate}
                        placeholder={"الى تاريخ"} label={"الى تاريخ"}
                        xs={12} md={6} lg={4}
                        readOnly={true}
                    />
                    <Grid item xs="auto" md={"auto"} lg={4}/>

                    {
                        this.props.leaveRequest.type === LeaveRequestType.TIMELY &&
                        <>
                            <TextField
                                value={this.props.leaveRequest.from}
                                placeholder={"من"} label={"من"}
                                xs={12} md={6} lg={4}
                                readOnly={true}
                            />
                            <TextField
                                value={this.props.leaveRequest.to}
                                placeholder={"الى"} label={"الى"}
                                xs={12} md={6} lg={4}
                                readOnly={true}
                            />
                            <Grid item xs="auto" md={"auto"} lg={4}/>
                        </>
                    }


                    <If condition={(this.props.leaveRequest || {}).type === LeaveRequestType.ONE_SHIFT}>
                        <TextField
                            value={getShiftTypeText(this.props.leaveRequest.shiftType)}
                            placeholder={"نوع الشفت"}
                            xs={12} md={6} lg={4}
                            readOnly={true} label={"نوع الشفت"}
                        />
                    </If>
                    <TextField
                        value={this.props.leaveRequest.date}
                        placeholder={"تاريخ الطلب"}
                        xs={12} md={6} lg={4}
                        readOnly={true} label={"تاريخ الطلب"}
                    />

                    <Grid container spacing={16}>
                        <TextField
                            value={this.props.leaveRequest.minutes}
                            placeholder={"الدقاق"}
                            xs={12} md={6} lg={4}
                            readOnly={true} label={"الدقاق"}
                        />

                        <TextField
                            value={this.props.leaveRequest.shifts}
                            placeholder={"الشفتات"}
                            xs={12} md={6} lg={4}
                            readOnly={true} label={"الشفتات"}
                        />
                    </Grid>


                    <TextField
                        value={this.props.leaveRequest.reason}
                        multiline rows={5}
                        placeholder={"السبب"}
                        xs={12} md={6} lg={6}
                        readOnly={true} label={"السبب"}
                    />

                </Grid>
                <br/><br/>
                {
                    this.props.balance &&
                    <Grid container spacing={16}>
                        <TextField
                            value={this.props.balance.totalShifts}
                            placeholder={"رصيد الشفتات"} label={"رصيد الشفتات"}
                            xs={12} md={6} lg={4}
                            readOnly={true}
                        />
                        <TextField
                            value={this.props.balance.totalMinutes}
                            placeholder={"رصيد الدقائق"} label={"رصيد الدقائق"}
                            xs={12} md={6} lg={4}
                            readOnly={true}
                        />
                    </Grid>
                }

                {
                    this.props.lastLeaveRequest &&
                    <>
                        <br/>
                        <Typography variant={"subheading"}>اخر اجازة</Typography>
                        <Grid container spacing={16}>
                            <TextField
                                value={this.props.leaveRequest.fromDate}
                                placeholder={"من تاريخ"} label={"من تاريخ"}
                                xs={12} md={6} lg={4}
                                readOnly={true}
                            />
                            <TextField
                                value={this.props.leaveRequest.toDate}
                                placeholder={"الى تاريخ"} label={"الى تاريخ"}
                                xs={12} md={6} lg={4}
                                readOnly={true}
                            />
                            <Grid item xs="auto" md={"auto"} lg={4}/>
                        </Grid>
                        <br/>
                    </>
                }

                {
                    this.props.leaveRequest.attachment &&
                    <>
                        <br/>
                        <Button variant={"outlined"} onClick={() => {
                            const url = URLs.getApiUrl(API_ROUTES.LEAVE_REQUEST.ATTACHMENT.with(this.props.leaveRequest.attachment
                                , Session.getUserObject().session));
                            window.open(url, "_blank");
                        }}>
                            تحميل الملف المرفق
                        </Button>
                        <br/>
                    </>
                }


                <br/><br/>
                {
                    (
                        (this.props.leaveRequest.status == AcceptanceStatus.WAITING && session.employeeId == this.props.leaveRequest.employee_id) ||
                        (session.employeeId == 64 || session.employeeId == 1)
                    ) &&
                    <div>
                        <Button style={{background: "#EE6C6B", color: "#FFF"}} onClick={this.onDelete}
                                disabled={this.props.deleting}>حذف الطلب</Button>
                    </div>
                }
            </>
        )
    }

    private onDelete = () => {
        const data = {leaveRequestId: this.props.leaveRequest.id};
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.POST, API_ROUTES.LEAVE_REQUEST.delete, Actions.DELETE_LEAVE_REQUEST, data);
    }
}


export default connect((store: any) => {
    return {
        deleting: store.DeleteLeaveRequest.loading,
        errorDeleting: store.DeleteLeaveRequest.error,
        successDeleting: store.DeleteLeaveRequest.success
    }
})(SingleRequestForm)