import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import URLs from "../../../utils/helpers/URLs";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import Header from "../../components/Header";
import LeaveRequestNoteForm from "./LeaveRequestNoteForm";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import {ILeaveRequest} from "./ILeaveRequest";
import SingleRequestForm from "./SingleRequestForm";
import NotesList from "./NotesList";
import {ReduxStore} from "../../../bootstrap/store";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props extends NetworkingState {
    leaveRequest: { info: ILeaveRequest, notes: any[] };
    balance: any;
    requestId: number;
    success: boolean,
    sendLoading: boolean;
    sendError: boolean;
    canManage: boolean;
    lastLeaveRequest: any;
    dispatch: (action: any) => void;
}

interface State {
    noteSent: boolean;
}


class SingleLeaveRequest extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {noteSent: false};
    }

    private form: Form<any> | null;

    componentDidMount() {
        const url = URLs.getApiUrl(`leave-requests/${this.props.requestId}`);
        this.props.dispatch(HttpGetAction(Actions.FETCH_LEAVE_REQUEST, url));
    }

    componentDidUpdate() {
        const modifiedProps = {
            success: this.props.success,
            error: this.props.sendError,
            dispatch: this.props.dispatch
        };
        const handler = new SuccessStateActionHandler(modifiedProps);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.MANAGE_LEAVE_REQUEST));
            this.setState({noteSent: true});
            this.form && this.form.clearValues();
        };
        handler.handleSuccessResponse();
    }


    render() {

        if (this.props.loading) {
            return <div/>
        }

        return (
            <div>

                <Header title={"الاجازات / طلب اجازة"}/>

                {
                    this.props.leaveRequest!.info &&
                    <SingleRequestForm lastLeaveRequest={this.props.lastLeaveRequest}
                                       balance={this.props.canManage ? this.props.balance : undefined}
                                       leaveRequest={this.props.leaveRequest.info}/>
                }

                <br/><br/>

                <NotesList notes={this.props.leaveRequest.notes}/>

                {
                    (this.props.canManage && !this.state.noteSent) &&
                    <>
                        <br/>
                        <Header title={"ارسال الملاحظات"}/>
                        <LeaveRequestNoteForm ref={ref => this.form = ref} onSend={this.onSend}/>
                        <br/>
                    </>
                }

            </div>
        )
    }

    onSend = (values: any) => {
        values["requestId"] = this.props.requestId;
        const url = URLs.getApiUrl("leave-requests/manage");
        this.props.dispatch(HttpPostAction(Actions.MANAGE_LEAVE_REQUEST, url, values));
    };


}

export default connect((store: ReduxStore) => ({
    loading: store.SingleLeaveRequest.loading,
    error: store.SingleLeaveRequest.error,
    leaveRequest: store.SingleLeaveRequest.object,
    balance: store.SingleLeaveRequest.object.balance,
    sendLoading: store.SendLeaveRequestNote.loading,
    sendError: store.SendLeaveRequestNote.error,
    success: store.SendLeaveRequestNote.success,
    lastLeaveRequest: store.SingleLeaveRequest.object.lastLeaveRequest,
    canManage: store.SingleLeaveRequest.object.canManage,
}))(SingleLeaveRequest);