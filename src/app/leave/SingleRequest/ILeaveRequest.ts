export interface ILeaveRequest {
    id: number;
    employeeName: string;
    employee_id: number;
    type: number;
    typeName: string;
    from: string;
    to: string;
    fromDate: any;
    toDate: any;
    shiftType: number;
    shiftTypeName: string;
    date: string;
    minutes: number;
    shifts: number;
    status: any;
    reason: any;
    attachment: any;
}