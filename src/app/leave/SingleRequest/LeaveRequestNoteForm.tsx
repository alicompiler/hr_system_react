import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import {Button, Grid} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {LeaveRequestStatus} from "../Types/Status";

interface Props {
    onSend: (values: any) => void;
}

export default class LeaveRequestNoteForm extends Form<Props> {

    render() {
        return (
            <div>
                <Grid container>
                    <TextField
                        ref={(ref: any) => this.pushElementRef(ref)} name={"note"}
                        placeholder={"الملاحظات"} xs={12} multiline rows={5} rowsMax={5}
                        validationRules={{length: {minimum: 5}}}
                    />
                </Grid>
                <Button color={"primary"} onClick={() => this.onSend(LeaveRequestStatus.DONE)} style={{width: 120}}>قبول
                    الطلب</Button>
                <Button color={"primary"} onClick={() => this.onSend(LeaveRequestStatus.REJECTED)}
                        style={{width: 120}}>رفض</Button>
            </div>
        )
    }

    onSend = (status: LeaveRequestStatus) => {
        if (this.hasErrors()) {
            this.validate();
            return;
        }
        const values = this.getValues();
        values["status"] = status;
        this.props.onSend(values);
    }


}