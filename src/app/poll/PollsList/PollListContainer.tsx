import * as React from "react";
import {IAction} from "../../../lib/redux++/action/IAction";
import Poll from "./Poll";
import {LinearProgress, Typography} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/Error";
import PollList from "./PollList";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import EmptyList from "../../components/EmptyList";

interface Props {
    action: IAction;
    loading: boolean;
    error: boolean;
    polls: Poll[];
    showLoading?: boolean;
    showError?: boolean;
    loadingMessage?: string;
    errorMessage?: string;
    showEmpty?: boolean;
    emptyMessage?: string;
    reducer: any;
    dispatch: (action: any) => void;
}

class PollListContainer extends React.Component<Props> {

    static defaultProps = {
        showLoading: false,
        showError: false,
        loadingMessage: 'جاري تحميل البيانات...',
        errorMessage: 'حصلت مشكلة خلال تحميل البيانات',
        showEmpty: false,
        emptyMessage: 'لا توجد اي نتائج'
    };

    componentWillMount() {
        this.props.dispatch(this.props.action);
    }

    render() {
        if (this.props.loading && this.props.showLoading) {
            return <div>
                <LinearProgress color={"primary"}/>
                <Typography variant={"subheading"}>{this.props.loadingMessage}</Typography>
            </div>
        } else if (this.props.error && this.props.showError) {
            return <div>
                <div style={{textAlign: 'center'}}>
                    <ErrorIcon style={{fontSize: 24, color: '#E00'}}/>
                    <Typography variant={"subheading"}>{this.props.errorMessage}</Typography>
                </div>
            </div>
        } else {
            if (this.props.polls.length == 0 && this.props.showEmpty) {
                return <EmptyList message={this.props.emptyMessage}/>
            }
            return (
                <PollList polls={this.props.polls}/>
            )
        }
    }
}

export default connect((store: ReduxStore, props: any) => {
    return {
        loading: store[props.reducer].loading,
        error: store[props.reducer].error,
        polls: store[props.reducer].array
    }
})(PollListContainer);

