import * as React from "react";
import {
    Button,
    CircularProgress,
    FormControlLabel,
    IconButton,
    LinearProgress,
    Radio,
    Typography
} from "@material-ui/core";
import AvatarWithTitle from "../../components/AvatarWithTitle";
import URLs from "../../../utils/helpers/URLs";
import DeleteIcon from "@material-ui/icons/Delete";
import Session from "../../../utils/helpers/Session";

interface Props {
    title: string,
    description: string,
    toDate: string;
    employeeName: string;
    employeeImage: string;
    employeeId: number;
    options: any[];
    sending: boolean;
    isFinished: boolean;
    myVote: any;
    onDeleteClicked: () => void;
    deleted: boolean;
    onOptionSelected: (option: any) => void;
}

export default class PollItem extends React.Component<Props> {

    render() {
        if (this.props.deleted) {
            return null;
        }
        const session = Session.getUserObject();
        return (
            <div style={{padding: '20px 0'}}>
                <div className={"announcement-header"}>
                    <AvatarWithTitle image={URLs.getImage("profile_images", this.props.employeeImage)}
                                     title={this.props.employeeName}
                                     subtitle={`اخر يوم للتصويت هو : ${this.props.toDate}`}/>
                    <div className={"left-part"}>
                        {
                            (session.userType === "admin" || session.employeeId == this.props.employeeId) &&
                            <IconButton disabled={this.props.sending} onClick={this.props.onDeleteClicked}><DeleteIcon/></IconButton>
                        }
                    </div>
                </div>

                <Typography variant={"headline"}>{this.props.title}</Typography>
                <Typography variant={"body1"}>{this.props.description}</Typography>
                {
                    this.props.options.map(this.renderOption)
                }
                <br/><br/>
                <Button disabled={this.props.sending || (this.props.isFinished ? true : false)} variant={"contained"}
                        color={"primary"} onClick={() => this.props.onOptionSelected(-1)}>الغاء التصويت</Button>
                <br/><br/>
                {
                    this.props.sending && <CircularProgress/>
                }

            </div>
        )
    }

    private renderOption = (option: any, key: number) => {
        return <div key={key}>
            <FormControlLabel
                control={
                    <Radio
                        checked={option.id == this.props.myVote}
                        onChange={this.onOptionSelected}
                        value={option.id}
                        disabled={this.props.sending || (this.props.isFinished ? true : false)}
                        color={"primary"}
                        name="option"
                        aria-label={option.title}
                    />
                }
                label={option.title}
            />
            {
                (this.props.isFinished ? true : false) &&
                <LinearProgress variant="determinate" value={option.voteRatio ? option.voteRatio : 0}/>
            }
        </div>;
    };

    private onOptionSelected = (e: any) => {
        if (!e.target) {
            return;
        }
        this.props.onOptionSelected(e.target.value);
    }

}