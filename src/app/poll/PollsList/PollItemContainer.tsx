import * as React from "react";
import PollItem from "./PollItem";
import Poll from "./Poll";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {Actions} from "../../../bootstrap/actions";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {connect} from "react-redux";
import {ReduxStore} from "../../../bootstrap/store";
import SnackbarAction from "../../../bootstrap/main_components/snackbar/SnackbarAction";

interface Props {
    loading: boolean;
    error: boolean;
    success: boolean;
    deleting: boolean;
    deleted: boolean;
    errorDeleting: boolean;
    dispatch: (action: any) => void;
    poll: Poll
}

class PollItemContainer extends React.Component<Props, any> {

    private currentAction: string | null = null;
    private actionInProgress: boolean = false;
    private changingStateInComponentDidUpdate = false;

    state = {selectedOption: this.props.poll.myVote, sending: false, deleted: false};

    componentDidUpdate(prevProps: any, prevState: any) {
        if (this.actionInProgress && !this.changingStateInComponentDidUpdate) {
            this.setState({sending: this.props.loading || this.props.deleting, deleted: this.props.deleted},
                () => this.changingStateInComponentDidUpdate = false);
            this.changingStateInComponentDidUpdate = true;
        }
        if (this.currentAction) {
            const success = this.currentAction === Actions.DELETE_POLL ? this.props.deleted : this.props.success;
            const error = this.currentAction === Actions.DELETE_POLL ? this.props.errorDeleting : this.props.error;
            const handler = new SuccessStateActionHandler({...this.props, success: success, error: error});

            handler.afterAny = () => {
                if (this.actionInProgress) {
                    this.setState({sending: this.props.loading || this.props.deleting, deleted: this.props.deleted},
                        () => this.changingStateInComponentDidUpdate = false);
                }
                this.actionInProgress = false;
                this.props.dispatch(HttpResetAction(this.currentAction as any));
                this.currentAction = null;
            };
            handler.afterError = () => {
                this.changingStateInComponentDidUpdate = true;
                this.setState({selectedOption: this.props.poll.myVote}, () => this.changingStateInComponentDidUpdate = false);
            };
            handler.handleSuccessResponse();
        }
    }

    render() {
        return (
            <PollItem title={this.props.poll.title}
                      description={this.props.poll.description}
                      options={this.props.poll.options} sending={this.state.sending}
                      myVote={this.state.selectedOption}
                      employeeImage={this.props.poll.employeeImage}
                      employeeName={this.props.poll.employeeName}
                      employeeId={this.props.poll.employeeId}
                      toDate={this.props.poll.toDate}
                      isFinished={this.props.poll.isFinished}
                      onDeleteClicked={this.delete}
                      deleted={this.state.deleted}
                      onOptionSelected={this.onOptionSelected}/>
        )
    }

    private onOptionSelected = (option: any) => {
        if (this.props.loading) {
            this.props.dispatch(SnackbarAction("هناك عملية تصويت حاليا ، اعد المحاولة عند الاكتمال", true, "warning"));
            return;
        }
        this.currentAction = Actions.VOTE_ON_POLL;
        this.actionInProgress = true;
        this.setState({selectedOption: option});
        const data = {optionId: option, pollId: this.props.poll.id};
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.POLL.VOTE, Actions.VOTE_ON_POLL, data)
            .dispatch(this.props.dispatch);
    };

    private delete = () => {
        if (this.props.deleting) {
            this.props.dispatch(SnackbarAction("هناك عملية حذف حاليا ، اعد المحاولة عند الاكتمال", true, "warning"));
            return;
        }
        this.currentAction = Actions.DELETE_POLL;
        this.actionInProgress = true;
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.POLL.DELETE, Actions.DELETE_POLL, {pollId: this.props.poll.id})
            .dispatch(this.props.dispatch);
    };
}

export default connect((store: ReduxStore) => {
    return {
        loading: store.VoteOnPoll.loading,
        error: store.VoteOnPoll.error,
        success: store.VoteOnPoll.success,
        deleting: store.DeletePoll.loading,
        errorDeleting: store.DeletePoll.error,
        deleted: store.DeletePoll.success
    }
})(PollItemContainer);