export default interface Poll {
    id: number;
    title: string;
    description: string;
    toDate: string;
    options: any[];
    employeeName: string;
    employeeImage: string;
    myVote: string;
    isFinished: boolean;
    employeeId: number;
}