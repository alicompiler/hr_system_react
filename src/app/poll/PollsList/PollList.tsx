import * as React from "react";
import Poll from "./Poll";
import PollItemContainer from "./PollItemContainer";

interface Props {
    polls: Poll[],
}

export default class PollList extends React.Component<Props> {

    render() {
        return (
            <div>
                {
                    this.props.polls.map((poll: Poll, index: number) =>
                        <PollItemContainer key={index} poll={poll}/>
                    )
                }
            </div>
        )
    }

}