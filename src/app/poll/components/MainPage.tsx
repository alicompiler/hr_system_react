import * as React from "react";
import {Button, Divider, FormControlLabel, Radio, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import ActivePolls from "./ActivePolls";
import FinishedPolls from "./FinishedPolls";
import MyPolls from "./MyPolls";

interface Props {
}

export default class MainPage extends React.Component<Props, any> {

    state = {viewType: 'active-finished'};

    render() {
        return (
            <div>
                {this.renderViewTypeOptions()}
                <br/>
                <Link to={"/polls/create"}>
                    <Button variant={"contained"} color={"primary"}>انشاء استبيان جديد</Button>
                </Link>
                <br/><br/><Divider/><br/>
                {this.renderViewBasedOnViewType()}
            </div>
        )
    }

    private onViewTypeChange = (e: any) => {
        this.setState({viewType: e.target.value});
    };

    private renderViewTypeOptions = () => {
        return <>
            <FormControlLabel
                control={
                    <Radio
                        checked={this.state.viewType === "active-finished"}
                        onChange={this.onViewTypeChange}
                        value='active-finished'
                        color={"primary"}
                        name="option"
                    />
                }
                label='الاستبيانات النشطة و المنتهية'
            />
            <FormControlLabel
                control={
                    <Radio
                        checked={this.state.viewType === "active"}
                        onChange={this.onViewTypeChange}
                        value='active'
                        color={"primary"}
                        name="option"
                    />
                }
                label='الاستبيانات النشطة'
            />
            <FormControlLabel
                control={
                    <Radio
                        checked={this.state.viewType === "finished"}
                        onChange={this.onViewTypeChange}
                        value='finished'
                        color={"primary"}
                        name="option"
                    />
                }
                label='الاستبيانات المنتهية'
            />
            <FormControlLabel
                control={
                    <Radio
                        checked={this.state.viewType === "mine"}
                        onChange={this.onViewTypeChange}
                        value='mine'
                        color={"primary"}
                        name="option"
                    />
                }
                label='استبياناتي'
            />
        </>
    };

    private renderViewBasedOnViewType = () => {
        switch (this.state.viewType) {
            case 'active-finished':
                return <div>
                    {this.finishedPolls()}
                    <br/>
                    <Divider/>
                    <br/>
                    {this.activePolls()}
                </div>;
            case 'active' :
                return this.activePolls();
            case 'finished' :
                return this.finishedPolls();
            case 'mine' :
                return this.myPolls();
        }
        return null;
    };

    private activePolls = () => {
        return <>
            <br/>
            <Typography variant={"headline"}>الاستبيانات النشطة</Typography>
            <br/><br/>
            <ActivePolls/>
            <br/>
        </>
    };

    private finishedPolls = () => {
        return <>
            <br/>
            <Typography variant={"headline"}>الاستبيانات المنتهية</Typography>
            <br/><br/>
            <FinishedPolls/>
            <br/>
        </>
    };

    private myPolls = () => {
        return <>
            <br/>
            <Typography variant={"headline"}>استبياناتي</Typography>
            <br/><br/>
            <MyPolls/>
            <br/>
        </>
    }

}