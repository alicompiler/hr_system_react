import * as React from "react";
import PollListContainer from "../PollsList/PollListContainer";
import {IAction} from "../../../lib/redux++/action/IAction";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";

interface Props {
}

export default class ActivePolls extends React.Component<Props> {

    private action: IAction = HttpGetAction(Actions.FETCH_ACTIVE_POLLS,
        URLs.getApiUrl(API_ROUTES.POLL.ACTIVE_POLLS));

    render() {
        return (
            <PollListContainer showLoading={true} showError={true} showEmpty
                               action={this.action} reducer={"ActivePolls"}/>
        )
    }

}