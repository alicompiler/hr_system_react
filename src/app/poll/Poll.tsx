import * as React from "react";
import {Route, Switch} from "react-router";
import CreatePollContainer from "./CreateForm";
import ActivePolls from "./components/ActivePolls";
import FinishedPolls from "./components/FinishedPolls";
import MyPolls from "./components/MyPolls";
import MainPage from "./components/MainPage";

interface Props {
}

export default class Poll extends React.Component<Props> {
    render() {
        return (
            <Switch>
                <Route exact path={"/polls"} component={() => <MainPage/>}/>
                <Route exact path={"/polls/create"} component={() => <CreatePollContainer/>}/>
                <Route exact path={"/polls/active"} component={() => <ActivePolls/>}/>
                <Route exact path={"/polls/finished"} component={() => <FinishedPolls/>}/>
                <Route exact path={"/polls/mine"} component={() => <MyPolls/>}/>
            </Switch>
        )
    }
}