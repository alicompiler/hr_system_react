import * as React from "react";
import CreatePollForm from "./CreateForm";
import {connect} from "react-redux";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";

interface Props {
    loading: boolean;
    error: boolean;
    success: boolean;
    dispatch: (action: any) => void;
}

class CreatePollContainer extends React.Component<Props> {

    private form: any = null;

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.CREATE_POLL));
            this.form.clearValues();
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <CreatePollForm ref={ref => this.form = ref} onSubmit={this.submit} loading={this.props.loading}/>
        )
    }

    private submit = (values: any) => {
        new HttpActionDispatcher(HttpMethod.POST, API_ROUTES.POLL.CREATE, Actions.CREATE_POLL, values)
            .dispatch(this.props.dispatch);
    }
}

export default connect((store: any) => {
    return {
        loading: store.CreatePoll.loading,
        error: store.CreatePoll.error,
        success: store.CreatePoll.success
    }
})(CreatePollContainer);