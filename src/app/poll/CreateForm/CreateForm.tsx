import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import TextField from "../../../utils/ui/components/form/components/TextField";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import {Button, Checkbox, FormControlLabel, LinearProgress} from "@material-ui/core";

interface Props {
    loading: boolean;
    onSubmit: (values: any) => void;
}

export default class CreatePollForm extends Form<Props, any> {

    private options: string[] = [];

    constructor(props: Props) {
        super(props);
        this.state = {isResultPublic: false, optionsCount: 0};
    }


    render() {
        //TODO : hide finished posts that not public for results

        const optionsFields = this.generateOptionsFields();

        return (
            <div>
                <label>يجب عليك ملأ حقل الموضوع او التفاصيل على الاقل</label>
                <br/><br/>
                <TextField
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"title"}
                    label={"الموضوع"}
                    placeholder={"الموضوع (اختياري)"}
                    xs={12} md={6} lg={6}
                    validationRules={{length: {minimum: 0}}}
                />
                <TextField
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"description"}
                    label={"التفاصيل"}
                    placeholder={"التفاصيل (اختياري)"}
                    rows={5} multiline maxRows={5}
                    xs={12} md={6} lg={6}
                    validationRules={{length: {minimum: 0}}}
                />
                <DatePicker
                    ref={ref => this.pushElementRef(ref)}
                    name={"fromDate"}
                    placeholder={"من تاريخ"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />
                <DatePicker
                    ref={ref => this.pushElementRef(ref)}
                    name={"toDate"}
                    placeholder={"الى تاريخ"}
                    xs={12} sm={12} md={6} lg={6}
                    validationRules={{presence: true, datetime: {dateOnly: true}}}
                />

                <br/><br/>
                <FormControlLabel
                    label="اضهار النتائج لبقية الموضوفين"
                    control={
                        <Checkbox
                            style={{color: '#111'}}
                            checked={this.state.isResultPublic}
                            onChange={(e: any) => this.setState({isResultPublic: e.target.checked})}
                        />
                    }
                />

                <TextField
                    ref={(ref: any) => this.pushElementRef(ref)}
                    name={"optionsCount"}
                    label={"عدد الاختيارات"}
                    placeholder={"عدد الاختيارات"}
                    xs={12} md={6} lg={6}
                    type={"number"}
                    afterChange={this.onOptionsCountChange}
                />

                {optionsFields}

                <br/><br/><br/>
                <Button style={{minWidth: 120}} color={"primary"} variant={"contained"}
                        onClick={this.onSubmit}>انشاء</Button>
                <br/><br/>
                {
                    this.props.loading &&
                    <LinearProgress color={"primary"}/>
                }
            </div>
        )
    }

    private onOptionsCountChange = (e: any) => {
        const count = e.target.value;
        this.options = [];
        this.setState({optionsCount: count});
    };

    private generateOptionsFields() {
        const optionsFields = [];
        for (let i = 0; i < this.state.optionsCount; i++) {
            const name = "option_" + i;
            optionsFields.push(
                <TextField
                    key={i}
                    name={name}
                    label={"اختيار"}
                    placeholder={"اختيار"}
                    xs={12} md={6} lg={6}
                    validationRules={{length: {minimum: 1}}}
                    afterChange={(e: any) => this.options[name] = e.target.value}
                />
            )
        }
        return optionsFields;
    }

    private onSubmit = () => {
        if (!this.validate()) {
            return;
        }
        const values = this.getValues();
        if (!values["title"].trim() && !values["description"].trim()) {
            const titleElement = this.getFieldByName("title");
            const descriptionElement = this.getFieldByName("description");
            titleElement && titleElement.error(true);
            descriptionElement && descriptionElement.error(true);
            return;
        }
        values["isResultPublic"] = this.state.isResultPublic;
        values["options"] = Object.keys(this.options).map(key => this.options[key]);
        this.props.onSubmit(values);
    }
}