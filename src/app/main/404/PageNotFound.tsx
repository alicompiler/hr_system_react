import * as React from "react";
import {Button, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

export default class PageNotFound extends React.Component {
    render() {
        return (
            <div style={{padding: 24, textAlign: "center"}}>
                <Typography color={"error"} variant={"headline"}>PAGE NOT FOUND 404</Typography>
                <div style={{padding: 16, textAlign: 'center'}}>
                    <Link to={'/'}>
                        <Button color={"primary"}>GO TO HOME PAGE</Button>
                    </Link>
                </div>
            </div>
        )
    }
}