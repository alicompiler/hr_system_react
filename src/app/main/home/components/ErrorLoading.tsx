import * as React from "react";
import {Button, Typography} from "@material-ui/core";
import Session from "../../../../utils/helpers/Session";

export default class ErrorLoading extends React.Component {
    render() {
        return <div className="home-loading-error-container">
            <Typography variant={"headline"} color={"error"}>حصلت مشكلة ، تاكد من اتصالك بالانترنت</Typography>
            <br/>
            <br/>
            <Button onClick={this.logout} variant={"contained"} size={"medium"}
                    style={{background: "#EE6C6B", color: "#FFFFFF"}}>تسجيل
                خروج</Button>
        </div>
    }

    logout = () => {
        Session.logout();
        window.location.href = window.location.origin + "/login";
    };
}