import * as React from "react";
import AccountIcon from "@material-ui/icons/AccountBox"
import LeaveIcon from "@material-ui/icons/AirplanemodeActive";
import LoanIcon from "@material-ui/icons/MonetizationOn"
import EmployeeManagementIcon from "@material-ui/icons/FolderShared";
import AttachmentsIcon from "@material-ui/icons/Attachment";
import AnnouncementsIcon from "@material-ui/icons/Mail";
import WriteAnnouncementsIcon from "@material-ui/icons/Create";
import HomeIcon from "@material-ui/icons/Home";
import AbsenceIcon from "@material-ui/icons/Fingerprint";
import SalariesIcon from "@material-ui/icons/Money";
import SendIcon from "@material-ui/icons/Send";
import LogoutIcon from "@material-ui/icons/PowerOff";
import "../../../../../styles/home/home.css";
import PollIcon from "@material-ui/icons/Poll"
import DrawerItem from "./DrawerItem";
import { List } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import RemoveRedEye from "@material-ui/icons/RemoveRedEye";
import NotificationIcon from "@material-ui/icons/Notifications"

import Session from "../../../../../utils/helpers/Session";
import IdeaIcon from "@material-ui/icons/Widgets";
import { connect } from "react-redux";

export default class DrawerOptions extends React.Component {
    render() {
        const session = Session.getUserObject();
        return (
            <List>
                <DrawerItem text={"الرئيسية"} icon={<HomeIcon />} navigateTo={"/"} />
                <NotificationDrawerItemContainer />
                <DrawerItem text={"معلوماتي"} icon={<AccountIcon />} navigateTo={"/profile"} />
                <DrawerItem text={"الملفات المرفقة"} icon={<AttachmentsIcon />} navigateTo={"/profile/attachments"} />

                {
                    (session.isManager === "true" || session.userType === "admin") &&
                    <DrawerItem text={"ادارة الموظفين"} icon={<EmployeeManagementIcon />} navigateTo={"/employees"} />
                }
                {
                    session.userType === "admin" &&
                    <>
                        <DrawerItem text={"العلاوات"} icon={<EmployeeManagementIcon />}
                            navigateTo={"/salary-rise"} />
                        <DrawerItem text={"كتب الشكر"} icon={<EmployeeManagementIcon />}
                            navigateTo={"/appreciation-letter"} />
                        <DrawerItem text={"تقيمات الموظفين"} icon={<EmployeeManagementIcon />}
                            navigateTo={"/employee-rating"} />
                    </>
                }

                <DrawerItem text={"الاستبيان"} icon={<PollIcon />} navigateTo={"/polls"} />
                <DrawerItem text={"الاجازات"} icon={<LeaveIcon />} navigateTo={"/leave-requests"} />
                <DrawerItem text={"السلف"} icon={<LoanIcon />} navigateTo={"/loan"} />
                {
                    ((session.isManager && session.departmentId == 8) || session.userType == "admin") &&
                    <DrawerItem text={"السلف المكتملة"} icon={<LoanIcon />} navigateTo={"/loan/completed"} />
                }
                <DrawerItem text={"كتابة اعلان"} icon={<WriteAnnouncementsIcon />} navigateTo={"/announcements/new"} />
                <DrawerItem text={"الاعلانات"} icon={<AnnouncementsIcon />} navigateTo={"/announcements"} />
                <DrawerItem text={"الاعلانات الجديدة"} icon={<AnnouncementsIcon />}
                    navigateTo={"/announcements/unwatched"} />
                <DrawerItem text={"الاعلانات المحفوظة"} icon={<SaveIcon />}
                    navigateTo={"/announcements/saved"} />
                {
                    session.userType === "admin" &&
                    <DrawerItem text={"الاعلانات في الانتظار"} icon={<RemoveRedEye />}
                        navigateTo={"/announcements/unreviewed"} />
                }

                <DrawerItem text={"الحضور والانصراف"} icon={<AbsenceIcon />} navigateTo={"/audience"} />
                <DrawerItem text={"الرواتب"} icon={<SalariesIcon />} navigateTo={"/salary"} />
                <DrawerItem text={"افكار و مشاريع"} icon={<IdeaIcon />} navigateTo={"/project-idea"} />
                <DrawerItem text={"المقترحات و المعوقات"} icon={<SendIcon />} navigateTo={"/suggestion-issue"} />

                <DrawerItem text={"النظام الداخلي"} icon={<AttachmentsIcon />} navigateTo={"/rules"} />
                {/*<DrawerItem text={"اخرى"} icon={<OtherIcon/>} navigateTo={"/other"}/>*/}
                {/*<DrawerItem text={"اخرى"} icon={<OtherIcon/>} navigateTo={"/other"}/>*/}
                <DrawerItem text={"تسجيل خروج"} icon={<LogoutIcon />} navigateTo={"/logout"} />
            </List>
        )
    }
}

interface Props {
    notificationsCount: number;
}

class NotificationDrawerItem extends React.Component<Props> {
    render() {
        console.log(this.props.notificationsCount);
        return <DrawerItem text={"الاشعارات"}
            badge={this.props.notificationsCount}
            icon={<NotificationIcon />}
            navigateTo={"/notifications"} />
    }
}

const NotificationDrawerItemContainer = connect((store: any) => {
    const notifications = store.Notifications.array;
    return {
        notificationsCount: notifications ? notifications.length : 0
    }
})(NotificationDrawerItem);