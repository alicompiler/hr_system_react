import * as React from "react";
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import ReduxAction from "../../../../../lib/redux++/action/IAction";
import {Actions} from "../../../../../bootstrap/actions";

interface Props {
    text: string;
    icon: any;
    navigateTo: string;
    open: boolean;
    badge?: any;
    dispatch: (action: any) => void;
}

class DrawerItem extends React.Component<Props> {
    render() {
        return (
            <Link to={this.props.navigateTo} onClick={this.close}>
                <ListItem button>
                    <ListItemIcon style={{color: "#FFF"}}>
                        {this.props.icon}
                    </ListItemIcon>
                    <ListItemText classes={{primary: "drawer-item-primary"}} primary={this.props.text}/>
                    {
                        this.props.badge > 0 &&
                        <span style={{
                            color: '#FFF'
                        }}>
                            {this.props.badge}
                        </span>
                    }
                </ListItem>
            </Link>
        )
    }

    private close = () => {
        if (this.props.open) {
            this.props.dispatch(ReduxAction(Actions.CLOSE_DRAWER));
        }
    }
}

export default connect((store: any) => {
    return {
        open: store.Drawer.open
    }
})(DrawerItem)