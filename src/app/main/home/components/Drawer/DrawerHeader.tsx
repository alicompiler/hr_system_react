import * as React from "react";
import {Divider, Typography} from "@material-ui/core";
import URLs from "../../../../../utils/helpers/URLs";

interface Props {
    user: any
}

export default class DrawerHeader extends React.Component<Props> {
    render() {
        const imageUrl = URLs.getImage("profile_images",
            (this.props.user ?
                    (this.props.user.employee.image ? this.props.user.employee.image : "default_image.jpg")
                    : "default_image.jpg"
            )
        );
        return (
            <div>
                <br/>
                <div style={{textAlign: "center"}}>
                    <img src={imageUrl} width={"128px"} height={"128px"}
                         className="drawer-image"/>
                </div>
                <Typography variant={"subheading"} color={"textSecondary"} align={"center"}>
                    {this.props.user ? this.props.user.employee.name : "-"}
                </Typography>
                <Typography className="drawer-username" variant={"subheading"} color={"textSecondary"} align={"center"}>
                    @{this.props.user ? this.props.user.username : "-"}
                </Typography>
                <br/>
                <Divider style={{backgroundColor: "#F44950", width: "100%"}}/>
            </div>
        )
    }
}