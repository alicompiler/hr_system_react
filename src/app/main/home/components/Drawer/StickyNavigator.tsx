import * as React from "react";
import "../../../../../styles/home/home.css";
import DrawerOptions from "./DrawerOptions";
import DrawerHeader from "./DrawerHeader";
import {getScreenSizeType} from "../../../../../utils/helpers/ScreenSizeUtils";

interface Props {
    user: any;
    displayAlways?: boolean;
}

interface State {
    hidden: boolean;
}

export default class StickyNavigator extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {hidden: true};
    }

    componentDidMount() {
        window.addEventListener("resize", this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize = () => {
        const screenType = getScreenSizeType();
        this.setState({hidden: screenType !== "large-desktop"})
    };

    render() {
        if (this.state.hidden && !this.props.displayAlways) return null;
        return (
            <div className="drawer">
                <DrawerHeader user={this.props.user}/>
                <DrawerOptions/>
            </div>
        )
    }
}