import * as React from "react";
import MaterialDrawer from "@material-ui/core/Drawer";
import StickyNavigator from "./StickyNavigator";
import {connect} from "react-redux";
import {default as ReduxAction} from "../../../../../lib/redux++/action/IAction";
import {Actions} from "../../../../../bootstrap/actions";

interface Props {
    open: boolean;
    user: any;
    dispatch: (action: any) => void;
}

class Drawer extends React.Component<Props> {
    render() {
        return (
            <MaterialDrawer anchor="left" open={this.props.open} onClose={this.onClose}>
                <StickyNavigator displayAlways={true} user={this.props.user}/>
            </MaterialDrawer>
        )
    }

    onClose = () => {
        this.props.dispatch(ReduxAction(Actions.CLOSE_DRAWER));
    }
}

export default connect((store: any) => ({
    open: store.Drawer.open
}))(Drawer);