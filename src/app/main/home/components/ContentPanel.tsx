import * as React from "react";
import Header from "./Header";
import {Divider} from "@material-ui/core";
import Routes from "../../Routes";


export default class ContentPanel extends React.Component {
    render() {
        return (
            <div className="main">
                <Header/>
                <Divider/>
                <br/>
                <div className="main-panel">
                    <Routes/>
                </div>
            </div>
        )
    }
}