import * as React from "react";
import {Button, IconButton, Typography} from "@material-ui/core";
import {Menu as MenuIcon} from "@material-ui/icons";
import {connect} from "react-redux";
import {Actions} from "../../../../bootstrap/actions";
import ReduxAction from "../../../../lib/redux++/action/IAction";
import {getScreenSizeType} from "../../../../utils/helpers/ScreenSizeUtils";
import Session from "../../../../utils/helpers/Session";

interface Props {
    dispatch: (action: any) => void;
}

interface State {
    displayDrawerButton: boolean;
}

class Header extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {displayDrawerButton: false};
    }

    componentDidMount() {
        window.addEventListener("resize", this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize = () => {
        const screenType = getScreenSizeType();
        this.setState({displayDrawerButton: screenType !== "large-desktop"})
    };

    render() {
        return (
            <div className="header">
                <div className="header-right-section">
                    {
                        this.state.displayDrawerButton &&
                        <IconButton color="inherit" aria-label="Menu" onClick={this.onDrawerButton}>
                            <MenuIcon/>
                        </IconButton>
                    }
                    <Typography style={{display: "inline-block"}} variant={"headline"}>نظام ادارة الموظفين</Typography>
                </div>

                <div className="header-left-section">
                    <Button onClick={this.logout} variant={"contained"} size={"medium"} className="logout-button">
                        تسجيل الخروج
                    </Button>
                </div>
            </div>
        )
    }

    logout = () => {
        Session.logout();
        window.location.href = window.location.origin + "/login";
    };

    onDrawerButton = () => {
        this.props.dispatch(ReduxAction(Actions.OPEN_DRAWER));
    }
}

export default connect(() => ({}))(Header);