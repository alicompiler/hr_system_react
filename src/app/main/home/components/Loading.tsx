import * as React from "react";
import {CircularProgress} from "@material-ui/core";

export default class Loading extends React.Component {
    render() {
        return <div className="home-loading-error-container">
            <CircularProgress color="secondary"/>
        </div>
    }
}