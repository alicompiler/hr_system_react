import * as React from "react"
import {Route, Switch} from "react-router-dom";
import PageNotFound from "./404/PageNotFound";
import Employee from "../employee/index";
import Leave from "./../leave/index";
import Announcement from "../announcement";
import Audience from "../audience";

import Salary from "../salary";
import ProfileRouter from "../profile";
import MyAttachments from "../profile/EmployeeAttachments/MyAttachments";
import Home from "./../home";
import Loan from "../loan";
import Session from "../../utils/helpers/Session";
import SuggestionAndIssue from "./../suggestion_and_issue/index"
import Poll from "../poll/Poll";
import ProjectIdea from "../project_idea/ProjectIdea";
import SalaryRiseMain from "../employee_extra/salary_rise/Main";
import AppreciationLetterMain from "../employee_extra/appreciation_letter/Main";
import EmployeeRatingMain from "../employee_extra/rating/Main";
import NotificationPage from "../notification/NotificationPage";
import RulesMain from "../Rules/RulesMain";


export default class Routes extends React.Component {

    render() {
        const session = Session.getUserObject();
        
        return (
            <Switch>
                <Route exact path="/" component={() => <Home/>}/>
                {
                    (session.userType === "admin" || session.isManager === "true") &&
                    <Route path="/employees" component={() => <Employee/>}/>
                }
                <Route path="/leave-requests" component={() => <Leave/>}/>
                <Route path={"/loan"} component={() => <Loan/>}/>
                <Route path="/announcements" component={() => <Announcement/>}/>

                <Route path="/audience" component={() => <Audience/>}/>
                <Route path="/salary" component={(route: any) => <Salary route={route}/>}/>

                <Route path="/audience" component={() => <Audience/>}/>
                <Route path={"/profile"} component={() => <ProfileRouter/>}/>
                <Route path={"/my-files"} component={() => <MyAttachments/>}/>
                <Route path={"/suggestion-issue"} component={() => <SuggestionAndIssue/>}/>

                <Route path={"/project-idea"} component={() => <ProjectIdea/>}/>
                <Route path={"/polls"} component={() => <Poll/>}/>

                <Route path={"/salary-rise"} component={() => <SalaryRiseMain/>}/>
                <Route path={"/appreciation-letter"} component={() => <AppreciationLetterMain/>}/>
                <Route path={"/employee-rating"} component={() => <EmployeeRatingMain/>}/>
                <Route path={"/notifications"} component={() => <NotificationPage/>}/>

                <Route path={'/rules'} component={() => <RulesMain />}/>

                <Route component={() => <PageNotFound/>}/>
            </Switch>
        );
    }
}