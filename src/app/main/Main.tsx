import * as React from "react";
import StickyNavigator from "./home/components/Drawer/StickyNavigator";
import {connect} from "react-redux";
import {HttpPostAction} from "../../utils/redux++wrapper/Actions";
import {Actions} from "../../bootstrap/actions";
import URLs from "../../utils/helpers/URLs";
import Drawer from "./home/components/Drawer/Drawer";
import ContentPanel from "./home/components/ContentPanel";
import Loading from "./home/components/Loading";
import ErrorLoading from "./home/components/ErrorLoading";
import SessionEndError from "./home/components/SessionEndError";

interface Props {
    user: any;
    loading: boolean;
    error: boolean;
    firstTime: boolean;

    dispatch: (action: any) => void;
}

class Main extends React.Component<Props> {
    componentDidMount() {
        if (!this.props.firstTime) {
            let url = URLs.getApiUrl("current-user-info");
            this.props.dispatch(HttpPostAction(Actions.FETCH_CURRENT_USER_INFO, url));
            this.props.dispatch({type: Actions.MAIN_DID_LOAD, payload: true})
        }
    }

    render() {
        if (this.props.loading && this.props.firstTime) {
            return <Loading/>
        } else if (this.props.error && this.props.firstTime) {
            return <ErrorLoading/>
        }
        
        if (this.props.user && this.props.user.auth_error) {
            return <SessionEndError/>
        }

        return (
            <div className="app">
                <StickyNavigator user={this.props.user}/>
                <Drawer user={this.props.user}/>
                <ContentPanel/>
            </div>
        );
    }
}

export default connect((store: any) => ({
    loading: store.UserInfo.loading,
    error: store.UserInfo.error,
    user: store.UserInfo.object,
    firstTime: store.FirstTime.firstTime
}))(Main);