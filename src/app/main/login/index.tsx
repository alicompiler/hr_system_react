import * as React from "react";
import "../../../styles/home/login.css";
import Form from "../../../utils/ui/components/form/Form";
import {Actions} from "../../../bootstrap/actions";
import URLs from "../../../utils/helpers/URLs";
import {HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {connect} from "react-redux";
import Session from "../../../utils/helpers/Session";
import {ReduxStore} from "../../../bootstrap/store";
import LoginForm from "./LoginForm";
import {RouteComponentProps} from "react-router";

interface Props {
    loading: boolean,
    error: boolean,
    login: any,
    route: RouteComponentProps;
    dispatch: (action: object) => void
}

class Login extends Form<Props> {

    render() {
        return <LoginForm loading={this.props.loading} error={this.props.error} onLogin={this.onLogin}/>;
    }

    componentDidUpdate() {
        if (this.props.login && this.props.login.success) {
            this.login();
        }
    }

    private login = () => {
        const {login} = this.props;
        Session.login(login.session, login.userId, login.employeeId, login.name, login.departmentId, login.isManager,
            login.userType);
        let path = this.props.route.location.pathname;
        if (path.startsWith("/login")) {
            path = "/";
        }
        window.location.href = window.location.origin + path;
    };

    onLogin = (values: any) => {
        let action = HttpPostAction(Actions.LOGIN, URLs.getApiUrl("login"), values);
        this.props.dispatch(action);
    };
}

export default connect((store: ReduxStore) => ({
    loading: store.LoginReducer.loading,
    error: store.LoginReducer.error,
    login: store.LoginReducer.object
}))(Login)