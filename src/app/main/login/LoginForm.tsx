import * as React from "react";
import Paper from "@material-ui/core/Paper/Paper";
import "../../../styles/home/login.css";
import Typography from "@material-ui/core/Typography/Typography";
import LockIcon from "@material-ui/icons/Lock";
import StackView from "../../../lib/layout/StackView";
import {Gravity} from "../../../lib/layout/Gravity";
import VerticalSpacing from "../../../utils/ui/components/spacing/VerticalSpacing";
import {Button, Grid, LinearProgress} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import PersonIcon from "@material-ui/icons/Person";
import Form from "../../../utils/ui/components/form/Form";

interface Props {
    loading: boolean,
    error: boolean,
    onLogin: (values: any) => void;
}

export default class LoginForm extends Form<Props> {
    render() {
        return (
            <div className="login_page">
                <Paper className="login_card">

                    <StackView layoutGravity={Gravity.CENTER} padding={8}>
                        <LockIcon style={{color: "#2B2B2B", fontSize: 44, margin: "auto"}}/>
                    </StackView>
                    <Typography align="center" variant="title">تسجيل الدخول</Typography>
                    <VerticalSpacing height={40}/>
                    <Grid container={true} style={{width: "100%"}}>
                        <TextField
                            ref={ref => this.pushElementRef(ref)}
                            name={"username"}
                            onEnter={this.onLogin}
                            adornmentClassName={"ar-adornment"}
                            adornment={<PersonIcon/>}
                            placeholder={"اسم المستخدم"}
                            xs={12}
                            validationRules={{length: {minimum: 3}}}
                        />
                        <TextField
                            ref={ref => this.pushElementRef(ref)}
                            name={"password"}
                            adornment={<LockIcon/>}
                            onEnter={this.onLogin}
                            adornmentClassName={"ar-adornment"}
                            type="password"
                            placeholder={"كلمة المرور"}
                            xs={12}
                            validationRules={{length: {minimum: 3}}}
                        />
                        <VerticalSpacing height={60}/>
                        <Button
                            disabled={this.props.loading}
                            onClick={this.onLogin} fullWidth color={"primary"} variant={"contained"} size={"large"}>
                            ارسال
                        </Button>
                    </Grid>

                    <div style={{marginTop: 16}}>
                        {
                            this.props.loading &&
                            <LinearProgress style={{width: "100%"}} color="primary"/>
                        }
                        {
                            this.props.error &&
                            <Typography variant={"subheading"} color={"error"}>
                                كلمة المرور او اسم المستخدم غير صحيح
                            </Typography>
                        }
                    </div>

                </Paper>
            </div>
        )
    }

    onLogin = () => {
        if (!this.validate()) {
            return;
        }
        let values = this.getValues();
        this.props.onLogin(values);
    };
}