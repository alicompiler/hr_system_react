import * as React from "react";
import {Route, Switch} from "react-router-dom";
import "./../../styles/employee/index.css";
import WriteAnnouncementContainer from "./Form/WriteAnnouncementContainer";
import AnnouncementsPage, {AnnouncementPageType} from "./AnnouncementList/AnnouncementsPage";

export default class Announcement extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path={"/announcements/new"} component={() => <WriteAnnouncementContainer/>}/>
                <Route exact path={"/announcements"}
                       component={(route: any) => <AnnouncementsPage
                           route={route}
                           usedReducer={"RecentAnnouncements"}
                           type={AnnouncementPageType.RECENT}/>}
                />

                <Route exact path={"/announcements/unreviewed"}
                       component={(route: any) => <AnnouncementsPage
                           route={route}
                           forReview={true}
                           usedReducer={"UnreviewedAnnouncements"}
                           type={AnnouncementPageType.UNREVIEWED}/>}
                />

                <Route exact path={"/announcements/saved"}
                       component={(route: any) => <AnnouncementsPage
                           route={route}
                           forReview={true}
                           usedReducer={"SavedAnnouncements"}
                           type={AnnouncementPageType.SAVED}/>}
                />


                <Route exact path={"/announcements/unwatched"}
                       component={(route: any) => <AnnouncementsPage
                           route={route}
                           usedReducer={"UnwatchedAnnouncements"}
                           type={AnnouncementPageType.UNWATCHED}
                       />}
                />
                <Route exact path={"/announcements/:date(\\d{4}-\\d{1,2}-\\d{1,2})"}
                       component={(route: any) => <AnnouncementsPage
                           route={route}
                           usedReducer={"AnnouncementsByDate"}
                           type={AnnouncementPageType.BY_DATE}
                           date={route.match.params.date}
                       />}
                />
            </Switch>
        );
    }
}