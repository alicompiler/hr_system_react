import * as React from "react";
import Announcement from "../Announcement/Announcement";
import IAnnouncement from "../IAnnouncement";
import {Grid} from "@material-ui/core";

interface Props {
    forReview?: boolean;
    announcements?: IAnnouncement[];
    onVisible: (announcementId: number, firstTime: boolean, visibility: boolean, alreadyWatched: boolean) => void;
}

export default class AnnouncementList extends React.Component<Props> {

    static defultProps = {
        forReview: false
    };

    render() {
        if (!this.props.announcements || this.props.announcements.length === 0)
            return null;
        return (
            <Grid container spacing={16}>
                {
                    this.props.announcements.map(
                        (item: IAnnouncement, index: number) =>
                            <Grid key={index} item sm={12} md={8} lg={8} xl={8}>
                                <Announcement forReview={this.props.forReview} announcement={item}
                                              onVisible={this.props.onVisible}/>
                            </Grid>
                    )
                }
            </Grid>
        )
    }

}