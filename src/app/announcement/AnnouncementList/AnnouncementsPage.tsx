import * as React from "react";
import {connect} from "react-redux";
import Loading from "../../components/Loading";
import Error from "../../components/Error";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import AnnouncementList from "./AnnouncementList";
import Header from "../../components/Header";
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import {getDateAsString} from "../../../utils/helpers/DateHelper";
import {RouteComponentProps} from "react-router";
import * as validate from "validate.js";
import {ErrorOutline} from "@material-ui/icons";
import {Typography} from "@material-ui/core";

interface Props extends NetworkingState {
    forReview?: boolean;
    announcements: any[];
    usedReducer: string;
    type: AnnouncementPageType;
    date?: string;
    route: RouteComponentProps;
    dispatch: (action: any) => void;
}

export enum AnnouncementPageType {
    RECENT,
    UNREVIEWED,
    SAVED,
    UNWATCHED,
    BY_DATE
}

class AnnouncementsPage extends React.Component<Props> {
    static defaultProps = {usedReducer: "Announcements", forReview: false};

    // private dateInput: DatePicker | null;

    componentDidMount() {
        this.dispatchActionBasedOnType();
    }

    private dispatchActionBasedOnType() {
        let url: string;
        switch (this.props.type) {
            case AnnouncementPageType.RECENT:
                url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.RECENT);
                this.props.dispatch(HttpGetAction(Actions.FETCH_RECENT_ANNOUNCEMENTS, url));
                break;
            case AnnouncementPageType.BY_DATE:
                url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.withDate(this.props.date!));
                this.props.dispatch(HttpGetAction(Actions.FETCH_ANNOUNCEMENTS_BY_DATE, url));
                break;
            case AnnouncementPageType.UNWATCHED:
                url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.UNWATCHED);
                this.props.dispatch(HttpGetAction(Actions.FETCH_UNWATCHED_ANNOUNCEMENTS, url));
                break;
            case AnnouncementPageType.UNREVIEWED:
                url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.UNREVIEWED);
                this.props.dispatch(HttpGetAction(Actions.FETCH_UNREVIEWED_ANNOUNCEMENTS, url));
                break;
            case AnnouncementPageType.SAVED:
                url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.SAVED_ANNOUNCEMENTS);
                this.props.dispatch(HttpGetAction(Actions.FETCH_SAVED_ANNOUNCEMENTS, url));
                break;
        }
    }

    render() {
        return (
            <div>
                <Header title={this.title()}/>
                <div className={"search-bar"}>
                    <DatePicker
                        // ref={ref => this.dateInput = ref}
                        name={"onDate"}
                        placeholder={"ابحث بتاريخ معين"}
                        xs={12} sm={12} md={6} lg={6}
                        afterChange={this.onSearch}
                        validationRules={{presence: true, datetime: {dateOnly: true}}}
                    />
                </div>
                <br/><br/>

                {
                    this.loadingOrErrorOrList()
                }
            </div>
        )
    }

    onSearch = (e: any) => {
        const date = getDateAsString(e);
        const error = validate.single(date, {datetime: {dateOnly: true}});
        if (!error)
            this.props.route.history.push(`/announcements/${date}`);
    };

    title = () => {
        switch (this.props.type) {
            case AnnouncementPageType.RECENT:
                return "الاعلانات / احدث الاعلانات";
            case AnnouncementPageType.BY_DATE:
                return "الاعلانات / باريخ معين";
            case AnnouncementPageType.SAVED:
                return "الاعلانات / المحفوظة";
            case AnnouncementPageType.UNREVIEWED:
                return "الاعلانات / تحت المراجعة";
            case AnnouncementPageType.UNWATCHED:
                return "الاعلانات / لم يتم مشاهدتها";
            default :
                return "الاعلانات";
        }
    };

    loadingOrErrorOrList = () => {
        if (this.props.loading)
            return <Loading loading message={"جاري تحميل الاعلانات"}/>;
        else if (this.props.error)
            return <Error error message={"حصلت مشكلة خلال تحميل البيانات"}/>;
        else if (this.props.announcements.length == 0) {
            return <div style={{padding: 8, textAlign: "center"}}>
                <ErrorOutline style={{fontSize: 28, color: "#111"}}/>
                <br/>
                <Typography variant={"title"}>لا توجد نتائج</Typography>
            </div>
        }

        return <AnnouncementList forReview={this.props.forReview}
                                 announcements={this.props.announcements}
                                 onVisible={this.onAnnouncementVisible}/>
    };

    onAnnouncementVisible = (announcementId: number, firstTime: boolean, visibility: boolean, alreadyWatched: boolean) => {
        if (firstTime && !alreadyWatched) {
            const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.WATCH);
            const data = {announcementId: announcementId};
            this.props.dispatch(HttpPostAction(Actions.SET_ANNOUNCEMENT_TO_BE_WATCHED, url, data));
        }
    }

}

export default connect((store: any, props: any) => {
    const reducer = props.usedReducer;
    return {
        loading: store[reducer].loading,
        error: store[reducer].error,
        announcements: store[reducer].array
    }
})(AnnouncementsPage)