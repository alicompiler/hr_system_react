export default interface IAnnouncement {
    id: number;
    author_id: number;
    employeeName: string;
    employeeImage: string;
    employeeDepartment: string;
    time: number;
    views: number;
    title?: string;
    description?: string;
    image?: string;
    files?: { filename: string, fileId: number }[];
    watched: number;
    saved: boolean;
}