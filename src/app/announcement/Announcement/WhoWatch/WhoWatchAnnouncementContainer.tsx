import * as React from "react";
import {connect} from "react-redux";
import WhoWatchAnnouncementDialog from "./WhoWatchAnnouncementDialog";
import NetworkingState from "../../../../lib/redux++/reducer/state/NetworkingState";
import URLs, {API_ROUTES} from "../../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../../bootstrap/actions";

interface Props extends NetworkingState {
    announcementId: number;
    open: boolean;
    list: any[];
    onClose: () => void;
    dispatch: (action: any) => void;
}

export class WhoWatchAnnouncementContainer extends React.Component<Props> {

    componentDidMount() {
        const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.WHO_WATCH.withAnnouncementId(this.props.announcementId));
        this.props.dispatch(HttpGetAction(Actions.FETCH_WHO_WATCH_ANNOUNCEMENT, url));
    }

    render() {
        return <WhoWatchAnnouncementDialog loading={this.props.loading} error={this.props.error} list={this.props.list}
                                           open={this.props.open}
                                           onClose={this.props.onClose}/>;
    }
}

export default connect((store: any) => ({
    list: store.WhoWatchAnnouncement.array,
    loading: store.WhoWatchAnnouncement.loading,
    error: store.WhoWatchAnnouncement.error,
}))(WhoWatchAnnouncementContainer);