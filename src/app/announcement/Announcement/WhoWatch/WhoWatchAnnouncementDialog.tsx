import * as React from "react";
import {Dialog, DialogContent, DialogTitle, Divider, List, ListItem} from "@material-ui/core";
import AvatarWithTitle from "../../../components/AvatarWithTitle";
import URLs from "../../../../utils/helpers/URLs";
import ListOrLoadingOrError from "../../../components/ListOrLoadingOrError";
import For from "../../../../lib/react-lang/For";
import DateHumanizer, {ARABIC_LOCAL} from "../../../../utils/helpers/DateHumanizer";

export default class WhoWatchAnnouncementDialog extends React.Component<Props> {

    render() {
        if (!this.props.list) return null;

        const humanizer = new DateHumanizer(ARABIC_LOCAL);
        const listComponent = <List>
            <For collection={this.props.list} renderItem={(item: any, index: number) => {
                return (<ListItem key={index}>
                    <AvatarWithTitle
                        image={URLs.getImage("profile_images", item.employeeImage ? item.employeeImage : 'default_image.jpg')}
                        title={item.employeeName} subtitle={humanizer.humanize(item.time)}/>
                </ListItem>)
            }}/>
        </List>;

        return (
            <Dialog open={this.props.open} onClose={this.props.onClose}>
                <DialogTitle>المشاهدات</DialogTitle>
                <Divider/>
                <DialogContent style={{minWidth: 400}}>
                    <ListOrLoadingOrError
                        listComponent={listComponent}
                        error={this.props.error}
                        loading={this.props.loading}/>
                </DialogContent>
            </Dialog>
        )
    }
}

interface Props {
    list?: any[];
    open: boolean;
    loading: boolean;
    error: boolean | null;
    onClose: () => void;
}
