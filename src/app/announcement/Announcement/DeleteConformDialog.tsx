import * as React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";

interface Props {
    open: boolean;
    message?: string;
    handleClose: () => void;
    onAgree: () => void;
}

export default class DeleteConformDialog extends React.Component<Props> {

    static defaultProps = {message: 'هل انت متاكد من عملية الحدف'};

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText>{this.props.message}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.handleClose} color="primary" autoFocus>
                        الغاء الامر
                    </Button>
                    <Button onClick={() => {
                        this.props.onAgree();
                        this.props.handleClose();
                    }} color="primary">
                        نعم متاكد
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

}