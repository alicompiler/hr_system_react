import * as React from "react";
import {Divider, IconButton, List, ListItem, ListItemText, Typography} from "@material-ui/core";
import {CloudDownload} from "@material-ui/icons";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import Session from "../../../utils/helpers/Session";

interface AnnouncementFileListProps {
    files?: any;
}

export default class AnnouncementFileList extends React.Component<AnnouncementFileListProps> {
    render() {
        if (!this.props.files || this.props.files.length === 0)
            return null;

        const files = JSON.parse(this.props.files);
        return (
            <>
                <Divider/>
                <br/>
                <Typography variant={"title"}>الملفات</Typography>
                <List className={"files"}>
                    {
                        files.map((file: { filename: string, fileId: number }, index: number) => {
                            return <ListItem key={index}>
                                <ListItemText>
                                    <IconButton component={"a"}
                                                onClick={() => this.download(file.fileId)}><CloudDownload/></IconButton>
                                    {file.filename}
                                </ListItemText>
                            </ListItem>
                        })
                    }
                </List>
            </>
        )
    }

    private download = (fileId: number) => {
        const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.DOWNLOAD.with(fileId, Session.getUserObject().session));
        window.open(url, "_blank");
    }

}