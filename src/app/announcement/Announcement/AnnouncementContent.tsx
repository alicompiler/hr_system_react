import * as React from "react";
import {Typography} from "@material-ui/core";
import URLs from "../../../utils/helpers/URLs";

interface AnnouncementContentProps {
    title?: string;
    description?: string;
    image?: string;
}

export default class AnnouncementContent extends React.Component<AnnouncementContentProps> {
    render() {
        return (
            <>
                {
                    this.props.title &&
                    <Typography style={{marginBottom: 8}} variant={"title"}>{this.props.title}</Typography>
                }
                {
                    this.props.description &&
                    <Typography style={{marginBottom: 8, whiteSpace: 'pre-wrap'}}
                                variant={"body1"}>{this.props.description}</Typography>
                }
                {
                    this.props.image &&
                    <img style={{marginBottom: 8, width: "100%", height: "auto"}}
                         src={URLs.getImage("announcements", this.props.image)}/>
                }
            </>
        )
    }
}
