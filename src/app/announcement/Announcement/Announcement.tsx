import * as React from "react";
import {Button, Divider, IconButton, Paper} from "@material-ui/core";
import AvatarWithTitle from "../../components/AvatarWithTitle";
import RemoveRedEye from "@material-ui/icons/RemoveRedEye";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import DateHumanizer, {ARABIC_LOCAL} from "../../../utils/helpers/DateHumanizer";
import WhoWatchAnnouncement from "./WhoWatch/WhoWatchAnnouncementContainer";
import VisibilitySensor from "react-visibility-sensor";
import IAnnouncement from "../IAnnouncement";
import AnnouncementFileList from "./AnnouncementFileList";
import AnnouncementContent from "./AnnouncementContent";
import {connect} from "react-redux";
import SaveIcon from "@material-ui/icons/Save";
import UnsaveIcon from "@material-ui/icons/RemoveCircleOutline";
import DeleteIcon from "@material-ui/icons/Delete";
import Session from "../../../utils/helpers/Session";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {Actions} from "../../../bootstrap/actions";
import DeleteConformDialog from "./DeleteConformDialog";

interface Props {

    deleting: boolean;
    errorDeleting: boolean;
    deleted: boolean;
    saving: boolean;
    errorSaving: boolean;
    saved: boolean;
    reviewing: boolean;
    errorReviewing: boolean;
    reviewed: boolean;
    dispatch: (action: any) => void;

    forReview?: boolean;
    announcement: IAnnouncement;
    onVisible: (announcementId: number, firstTime: boolean, visibility: boolean, alreadyWatched: boolean) => void;
}

interface State {
    showDialog: boolean;
    deleteDialog: boolean;
    show: boolean;
}

class Announcement extends React.Component<Props, State> {

    private currentAction: string = "";

    static defaultProps = {
        forReview: false
    };

    private firstTimeVisible: boolean = true;

    constructor(props: Props) {
        super(props);
        this.state = {showDialog: false, show: true, deleteDialog: false};
    }

    componentDidUpdate() {
        if (!this.currentAction) {
            return;
        }
        const shouldHide = this.props.deleted || this.props.reviewed;
        const props = {
            dispatch: this.props.dispatch,
            error: this.props.errorDeleting || this.props.errorReviewing || this.props.errorSaving,
            success: this.props.deleted || this.props.reviewed || this.props.saved
        };
        const message = this.props.saved ? "تم العملية ، اذا اردت روئية النتائج الجديدة يرجى التحديث" : undefined;
        const handler = new SuccessStateActionHandler(props);
        if (message)
            handler.successMessage = message;
        handler.afterAny = () => {
            this.props.dispatch(HttpResetAction(this.currentAction));
            this.currentAction = "";
        };
        handler.afterSuccess = () => {
            if (this.currentAction && shouldHide) {
                if (this.state.show)
                    this.setState({show: false});
            }
        };
        handler.handleSuccessResponse()
    }

    private onVisibilityChange = (isVisible: boolean) => {

        this.props.onVisible(this.props.announcement.id, this.firstTimeVisible, isVisible, this.props.announcement.watched > 0);
        this.firstTimeVisible = false;
    };

    render() {
        if (!this.state.show) {
            return null
        }
        const humanDate = new DateHumanizer(ARABIC_LOCAL).humanize(this.props.announcement.time);
        const session = Session.getUserObject();
        const loading = this.props.deleting || this.props.reviewing || this.props.saving;
        return (
            <VisibilitySensor onChange={this.onVisibilityChange}>
                <Paper className={"announcement"}>
                    <div className={"announcement-header"}>
                        <AvatarWithTitle image={URLs.getImage("profile_images", this.props.announcement.employeeImage)}
                                         title={this.props.announcement.employeeName}
                                         subtitle={this.props.announcement.employeeDepartment}/>
                        <div className={"left-part"}
                             style={{
                                 display: 'flex',
                                 flexDirection: 'row',
                                 justifyContent: 'center',
                                 alignItems: 'center'
                             }}>
                            {
                                this.props.announcement.saved ?
                                    <IconButton disabled={loading}
                                                onClick={() => this.unsave()}><UnsaveIcon/></IconButton>
                                    :
                                    <IconButton disabled={loading}
                                                onClick={() => this.save()}><SaveIcon/></IconButton>
                            }
                            {
                                session.employeeId == this.props.announcement.author_id &&
                                <IconButton disabled={loading}
                                            onClick={() => this.setState({deleteDialog: true})}><DeleteIcon/></IconButton>
                            }
                            <span style={{
                                display: 'flex',
                                justifyContent: 'center',
                                flexDirection: 'column',
                                alignItems: 'center'
                            }}>
                                <span>
                                    <span style={{padding: 6}}>{this.props.announcement.views}</span>
                                    <IconButton disabled={loading}
                                                onClick={this.openDialog}><RemoveRedEye/></IconButton>
                                </span>
                                <span className={"human-time"}>{humanDate}</span>
                            </span>

                        </div>
                    </div>
                    <AnnouncementContent title={this.props.announcement.title}
                                         description={this.props.announcement.description}
                                         image={this.props.announcement.image}/>
                    <br/>
                    <AnnouncementFileList files={this.props.announcement.files}/>
                    {
                        this.state.showDialog &&
                        <WhoWatchAnnouncement onClose={() => this.setState({showDialog: false})}
                                              open={this.state.showDialog}
                                              announcementId={this.props.announcement.id}/>
                    }

                    {
                        this.props.forReview &&
                        <>
                            <br/><br/>
                            <Divider/>
                            <br/><br/>
                            <Button disabled={loading} onClick={() => this.review(1)} color={"primary"}>قبول
                                الاعلان</Button>
                            <Button disabled={loading} onClick={() => this.review(-1)} color={"primary"}>رفض
                                الاعلان</Button>
                            <br/><br/>
                        </>
                    }
                    <DeleteConformDialog open={this.state.deleteDialog}
                                         handleClose={() => this.setState({deleteDialog: false})}
                                         onAgree={this.delete}/>
                </Paper>
            </VisibilitySensor>
        )
    }

    private openDialog = () => {
        this.setState({showDialog: true})
    };

    private review = (review: number) => {
        this.currentAction = Actions.REVIEW_ANNOUNCEMENT;
        const data = {reviewState: review, announcementId: this.props.announcement.id};
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.ANNOUNCEMENTS.REVIEW, Actions.REVIEW_ANNOUNCEMENT,
            data).dispatch(this.props.dispatch);
    };

    private delete = () => {
        this.currentAction = Actions.DELETE_ANNOUNCEMENT;
        const data = {announcementId: this.props.announcement.id};
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.ANNOUNCEMENTS.DELETE, Actions.DELETE_ANNOUNCEMENT,
            data).dispatch(this.props.dispatch);
    };

    private save = () => {
        this.currentAction = Actions.TOGGLE_SAVE_ANNOUNCEMENT;
        const data = {
            announcementId: this.props.announcement.id,
            saveState: 1
        };
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.ANNOUNCEMENTS.TOGGLE_SAVE, Actions.TOGGLE_SAVE_ANNOUNCEMENT,
            data).dispatch(this.props.dispatch);
    };

    private unsave = () => {
        this.currentAction = Actions.TOGGLE_SAVE_ANNOUNCEMENT;
        const data = {
            announcementId: this.props.announcement.id,
            saveState: 0
        };
        new HttpActionDispatcher(HttpMethod.POST,
            API_ROUTES.ANNOUNCEMENTS.TOGGLE_SAVE, Actions.TOGGLE_SAVE_ANNOUNCEMENT,
            data).dispatch(this.props.dispatch);
    };

}

export default connect((store: any) => {
    return {
        deleting: store.DeleteAnnouncement.loading,
        errorDeleting: store.DeleteAnnouncement.error,
        deleted: store.DeleteAnnouncement.success,
        saving: store.SaveUnsaveAnnouncement.loading,
        errorSaving: store.SaveUnsaveAnnouncement.error,
        saved: store.SaveUnsaveAnnouncement.success,
        reviewing: store.ReviewAnnouncement.loading,
        errorReviewing: store.ReviewAnnouncement.error,
        reviewed: store.ReviewAnnouncement.success
    }
})(Announcement)