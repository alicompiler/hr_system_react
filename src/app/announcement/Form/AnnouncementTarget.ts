export enum AnnouncementTarget {
    ALL = 1,
    MANAGERS,
    DEPARTMENT,
    EMPLOYEE
}