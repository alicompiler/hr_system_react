import * as React from "react";
import {connect} from "react-redux";
import WriteAnnouncement from "./WriteAnnouncement";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import {HttpGetAction, HttpPostAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";

interface Props extends NetworkingState {
    departments: any[],
    employees: any[],
    success: boolean;
    dispatch: (action: any) => void;
}

class WriteAnnouncementContainer extends React.Component<Props> {

    private form: WriteAnnouncement | null;

    componentDidMount() {
        const fetchEmployeeListUrl = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
        this.props.dispatch(HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, fetchEmployeeListUrl));
        const fetchDepartmentListUrl = URLs.getApiUrl(API_ROUTES.DEPARTMENTS.DEPARTMENT_LIST);
        this.props.dispatch(HttpGetAction(Actions.FETCH_DEPARTMENTS_LIST, fetchDepartmentListUrl));
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => this.form && this.form.clear(true);
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <WriteAnnouncement ref={ref => this.form = ref}
                               onSave={this.onSave}
                               departments={this.props.departments}
                               employees={this.props.employees}
                               loading={this.props.loading}/>
        )
    }

    onSave = (data: any) => {
        const url = URLs.getApiUrl(API_ROUTES.ANNOUNCEMENTS.NEW);
        this.props.dispatch(HttpPostAction(Actions.WRITE_ANNOUNCEMENT, url, data));
    }
}

export default connect((store: any) => {
    return {
        loading: store.WriteAnnouncement.loading,
        error: store.WriteAnnouncement.error,
        success: store.WriteAnnouncement.success,
        departments: store.DepartmentList.array,
        employees: store.EmployeeList.array,
    }
})(WriteAnnouncementContainer)