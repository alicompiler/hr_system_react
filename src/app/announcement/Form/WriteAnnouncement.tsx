import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import TextField from "../../../utils/ui/components/form/components/TextField";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import {AnnouncementTarget} from "./AnnouncementTarget";
import Select, {ISelectOption} from "../../../utils/ui/components/form/components/Select";
import FilesInput from "../../../utils/ui/components/form/components/FilesInput";
import {Button, List, ListItem, ListItemText, Typography} from "@material-ui/core";
import Header from "../../components/Header";
import {objectToFormData} from "../../../utils/helpers/DataUtils";

interface Props {
    departments: any[];
    employees: any[];
    loading: boolean;
    onSave: (values: any) => void;
}

interface State {
    image: any;
    attachedFiles: FileList | null;
    targetValueOptions: ISelectOption[];
    error: boolean;
}

export default class WriteAnnouncement extends Form<Props, State> {

    private imageInput: FileInput | null;
    private attachedFilesInput: FilesInput | null;

    constructor(props: Props) {
        super(props);
        this.state = {error: false, image: null, attachedFiles: null, targetValueOptions: []}
    }

    render() {
        return (
            <div>
                <Header title={"الاعلانات / كتابة اعلان"}/>

                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"targetType"}
                    placeholder={"نوع التوجيه"}
                    xs={12} sm={12} md={6} lg={6}
                    options={TARGET_TYPE_OPTIONS}
                    afterChange={(e: any) => this.changeTargetValueOptions(e.target.value)}
                    style={{marginTop: 16}}
                />
                <Select
                    ref={ref => this.pushElementRef(ref)}
                    name={"targetValue"}
                    placeholder={"موجه الى"}
                    disabled={this.state.targetValueOptions.length === 0}
                    xs={12} sm={12} md={6} lg={6}
                    options={this.state.targetValueOptions}
                    style={{marginTop: 16}}
                />
                <br/>

                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"title"}
                    placeholder={"العنوان (اختياري)"}
                    xs={12} md={6}
                />
                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"description"}
                    placeholder={"التفاصيل (اختياري)"}
                    multiline rows={8} rowsMax={8}
                    xs={12} md={6}
                />
                <br/>
                <FileInput ref={ref => this.imageInput = ref} title={"تحميل صورة (اختياري)"}
                           onChange={(image: any) => this.setState({image: image})}/>
                <br/>
                <FilesInput ref={ref => this.attachedFilesInput = ref} title={"الملفات المرفقة(اختياري)"}
                            onChange={this.onFilesChanged}/>
                {
                    (this.state.attachedFiles && this.state.attachedFiles.length > 0) &&
                    <List>
                        {this.renderFiles(this.state.attachedFiles)}
                    </List>
                }
                <br/>
                <Button disabled={this.props.loading} variant={"contained"} size={"large"} color={"primary"}
                        onClick={this.onSave}>
                    حفظ
                </Button>
                {
                    this.state.error &&
                    <Typography variant={"subheading"} color={"error"}>
                        عليك كتابة تفاصيل او ارفاق صورة على الاقل
                    </Typography>
                }
                <br/>
                <br/>
            </div>
        )
    }

    onSave = () => {
        const values = this.getValues();
        if (!values["description"] && !this.state.image) {
            this.setState({error: true});
            return;
        }
        this.setState({error: false});
        const data = objectToFormData(values);
        if (this.state.image)
            data.append("image", this.state.image, "image");
        if (this.state.attachedFiles) {
            for (let i = 0; i < this.state.attachedFiles.length; i++) {
                const file: any = this.state.attachedFiles.item(i);
                data.append(`file${i}`, file, file.name);
            }
        }
        this.props.onSave(data);
    };

    renderFiles = (files: FileList) => {
        let filesComponents = [];
        for (let i = 0; i < files.length; i++) {
            const file = files.item(i);
            const component = <ListItem key={i} style={{textAlign: "right"}}>
                <ListItemText>{file && file.name}</ListItemText>
            </ListItem>;
            filesComponents.push(component);
        }
        return filesComponents;
    };

    onFilesChanged = (files: FileList) => {
        this.setState({attachedFiles: files});
    };

    private changeTargetValueOptions = (targetType: number) => {
        let options: ISelectOption[] = [];
        if (targetType === AnnouncementTarget.DEPARTMENT) {
            options = this.props.departments.map((item: any) => ({text: item.name, value: item.id}));
        } else if (targetType === AnnouncementTarget.EMPLOYEE) {
            options = this.props.employees.map((item: any) => ({text: item.name, value: item.id}));
        }
        this.setState({targetValueOptions: options});
    };

    public clear = (forceUpdate: boolean = false) => {
        this.clearValues();
        this.setState({image: null, attachedFiles: null});
        this.attachedFilesInput && this.attachedFilesInput.clear();
        this.imageInput && this.imageInput.clear();
        forceUpdate && this.forceUpdate();
    }

}

const TARGET_TYPE_OPTIONS: ISelectOption[] = [
    {text: "الكل", value: AnnouncementTarget.ALL},
    {text: "المدراء", value: AnnouncementTarget.MANAGERS},
    {text: "القسم", value: AnnouncementTarget.DEPARTMENT},
    {text: "موظف", value: AnnouncementTarget.EMPLOYEE},
];