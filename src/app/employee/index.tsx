import * as React from "react";
import {Route, Switch} from "react-router-dom";
import EditForm from "./Form/EditForm";
import DeleteForm from "./Form/DeleteForm";
import CreateForm from "./Form/CreateForm";
import ChangeLoginCredentialsForm from "./Form/ChangeLoginCredentialsForm";
import SingleEmployeeForm from "./Form/SingleEmployeeForm";
import "./../../styles/employee/index.css"
import EmployeesList from "./EmployeeList";
import Session from "../../utils/helpers/Session";

export default class Employee extends React.Component {
    render() {
        const session = Session.getUserObject();
        return (
            <Switch>
                <Route exact path={"/employees"} component={(route: any) => <EmployeesList route={route}/>}/>
                <Route exact path={"/employees/:id(\\d+)"} component={
                    (route: any) => <SingleEmployeeForm employeeId={route.match.params.id}/>
                }/>
                {
                    session.userType === "admin" &&
                    <>
                        <Route exact path={"/employees/:id(\\d+)/delete/:name"} component={
                            (route: any) => <DeleteForm
                                employeeId={route.match.params.id}
                                employeeName={route.match.params.name}/>
                        }/>
                        <Route
                            exact path={"/employees/:id(\\d+)/change-credentials/:name"} component={
                            (route: any) => <ChangeLoginCredentialsForm
                                employeeId={route.match.params.id}
                                employeeName={route.match.params.name}/>
                        }/>

                        <Route exact path={"/employees/new"} component={() => <CreateForm/>}/>

                        <Route exact path={"/employees/:id(\\d+)/edit"} component={
                            (route: any) => <EditForm employeeId={route.match.params.id}/>
                        }/>
                    </>
                }
            </Switch>
        );
    }
}