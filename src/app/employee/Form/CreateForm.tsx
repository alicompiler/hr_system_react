import * as React from "react";
import Header from "../../components/Header";
import EmployeeForm from "./EmployeeForm";
import {connect} from "react-redux";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";
import {ReduxStore} from "../../../bootstrap/store";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";

interface Props extends NetworkingState {
    success?: boolean;
    departments: any[]
    dispatch: (action: any) => void;
}

class CreateForm extends React.Component<Props> {

    private form: EmployeeForm | null = null;

    componentDidMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.GET, API_ROUTES.DEPARTMENTS.DEPARTMENT_LIST, Actions.FETCH_DEPARTMENTS);
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => {
            this.form && this.form.clearValues();
            this.props.dispatch(HttpResetAction(Actions.CREATE_EMPLOYEE))
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <Header title={"الموظفين / اضافة موضف جديد"}/>
                <EmployeeForm ref={ref => this.form = ref}
                              departments={this.props.departments}
                              loading={this.props.loading} buttonTitle={"حفظ"}
                              onSave={this.onSave} userForm/>
            </div>
        )
    }

    onSave = (data: any) => {
        HttpActionWithFileDispatcher.dispatchWithFile(this.props.dispatch, API_ROUTES.EMPLOYEES.CREATE, Actions.CREATE_EMPLOYEE, data);
    }
}

export default connect((store: ReduxStore) => ({
    loading: store.CreateEmployee.loading,
    error: store.CreateEmployee.error,
    success: store.CreateEmployee.success,
    departments: store.Departments.array
}))(CreateForm)