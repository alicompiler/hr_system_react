import * as React from "react";
import Form from "../../../utils/ui/components/form/Form";
import TextField from "../../../utils/ui/components/form/components/TextField";
import { Button, CircularProgress, Divider, Grid, Typography } from "@material-ui/core";
import { AccountCircle, AlternateEmail, LocationOn, Lock, Money, Person, SettingsPhone, Work } from "@material-ui/icons";
import "./../../../styles/common/form/material_form.css"
import DatePicker from "../../../utils/ui/components/form/components/DatePicker";
import FileInput from "../../../utils/ui/components/form/components/FileInput";
import { objectToFormData } from "../../../utils/helpers/DataUtils";
import URLs from "../../../utils/helpers/URLs";
import Select from "../../../utils/ui/components/form/components/Select";
import EmployeeAttachments from "../../profile/EmployeeAttachments";

interface Props {
    onSave: (values: object) => void;
    departments: any[];
    buttonTitle?: string;
    formValues?: any;
    departmentAsText?: boolean;
    readOnlyForm?: boolean;
    userForm?: boolean;
    displayImage?: boolean;
    loading?: boolean;
}

interface State {
    file: any
}

export default class EmployeeForm extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = { file: null }
    }

    componentDidUpdate() {
        if (this.props.formValues && Object.keys(this.props.formValues).length > 0) {
            this.setValues(this.props.formValues);
        }
    }

    render() {
        return (
            <div>

                {
                    this.renderProfileImageOrInputFile()
                }

                <Grid container spacing={16}>

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"name"}
                        placeholder={"اسم الموظف"}
                        xs={12} md={6} lg={4}
                        autocomplete={"false"}
                        adornment={<Person />}
                        adornmentClassName="ar-adornment"
                        validationRules={{ length: { minimum: 3 } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"phone"}
                        placeholder={"رقم الهاتف"}
                        adornment={<SettingsPhone />}
                        adornmentClassName="ar-adornment"
                        xs={12} md={6} lg={4}
                        validationRules={{ length: { minimum: 4 } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"email"}
                        placeholder={"البريد الالكتروني"}
                        xs={12} md={6} lg={4}
                        adornment={<AlternateEmail />}
                        adornmentClassName="ar-adornment"
                        validationRules={{ email: true }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"address"}
                        placeholder={"العنوان"}
                        adornment={<LocationOn />}
                        adornmentClassName="ar-adornment"
                        xs={12} md={12} lg={12}
                        validationRules={{ length: { minimum: 4 } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"otherPhone"}
                        placeholder={"رقم الهاتف البديل"}
                        adornment={<SettingsPhone />}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"otherEmail"}
                        placeholder={"البريد الالكتروني البديل"}
                        xs={12} sm={12} md={6} lg={6}
                        adornment={<AlternateEmail />}
                        adornmentClassName="ar-adornment"
                        validationRules={{ optionalEmail: true }}
                    />

                    {this.renderDepartment()}

                    <DatePicker
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"birthDate"}
                        placeholder={"تاريخ الميلاد"}
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{ presence: true, datetime: { dateOnly: true } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"jobTitle"}
                        placeholder={"العنوان الوضيفي"}
                        adornment={<Work />}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{ length: { minimum: 4 } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"salary"}
                        placeholder={"الراتب الاسمي"}
                        type={"number"}
                        xs={12} sm={12} md={6} lg={6}
                        adornment={<Money />}
                        adornmentClassName="ar-adornment"
                        validationRules={{ presence: true, numericality: true }}
                    />

                    <DatePicker
                        ref={ref => this.pushElementRef(ref)}
                        readOnly={this.props.readOnlyForm}
                        name={"joinDate"}
                        placeholder={"تاريخ المباشرة"}
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{ presence: true, datetime: { dateOnly: true } }}
                    />

                </Grid>

                <br />

                {
                    this.props.displayImage &&
                    <>
                        <br />
                        <EmployeeAttachments employeeId={this.props.formValues.id} displayUploadForm={false} />
                    </>
                }

                {
                    this.props.userForm && this.renderUserForm()
                }

                <br />
                {
                    this.props.buttonTitle &&
                    <Button disabled={this.props.loading} variant={"contained"} size={"large"} color={"primary"}
                        onClick={this.onSave}>
                        {
                            this.props.buttonTitle
                        }
                    </Button>
                }

                {
                    this.props.loading && <>
                        <br /><br />
                        <CircularProgress color={"secondary"} />
                    </>
                }

                <br />

            </div>
        )
    }

    private renderDepartment = () => {
        return this.props.departmentAsText ?
            <TextField
                ref={ref => this.pushElementRef(ref)}
                readOnly={this.props.readOnlyForm}
                placeholder={"القسم"}
                name={"department"}
                xs={12} sm={12} md={6} lg={6}
                adornment={<Work />}
                adornmentClassName="ar-adornment"
            />
            :
            <Select
                ref={ref => this.pushElementRef(ref)}
                readOnly={this.props.readOnlyForm}
                name={"department_id"}
                placeholder={"القسم"}
                xs={12} sm={12} md={6} lg={6}
                disabled={this.props.departments.length == 0}
                options={this.props.departments}
                optionExtractor={(item: any) => ({ value: item.id, text: item.name })}
                style={{ marginTop: 16 }}
                validationRules={{ presence: false, numericality: { greaterThan: -1 } }}
            />;
    };

    private renderProfileImageOrInputFile() {
        return this.props.displayImage ?
            <div>
                {
                    this.props.formValues.image &&
                    <img style={{ width: 144, height: 144 }}
                        src={URLs.getImage("profile_images", this.props.formValues.image)} />
                }
            </div>
            :
            <FileInput validTypes={["image/jpeg", "image/png"]} title={"الصورة الشخصية"}
                onChange={file => this.setState({ file: file })} />;
    }

    renderUserForm = () => {
        return (
            <div>
                <br /><br />
                <Typography variant={"headline"}>معلومات الحساب</Typography>
                <Divider />
                <br />
                <Grid>
                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        name={"username"}
                        placeholder={"اسم المستخدم"}
                        adornment={<AccountCircle />}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{ length: { minimum: 3 } }}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        name={"password"}
                        placeholder={"كلمة المرور"}
                        adornment={<Lock />}
                        type={"password"}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{ length: { minimum: 6 } }}
                    />
                </Grid>
                <br />
            </div>
        )
    };

    onSave = () => {
        if (this.hasErrors()) {
            this.validate();
            return;
        }

        const values: object = this.getValues();
        const data = objectToFormData(values);
        if (this.state.file)
            data.append("image", this.state.file);
        this.props.onSave(data);
    }

}