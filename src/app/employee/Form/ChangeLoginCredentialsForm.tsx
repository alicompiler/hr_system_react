import * as React from "react";
import Header from "../../components/Header";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import RandomUtils from "../../../utils/helpers/RandomUtils";
import {Button, Grid, LinearProgress, Typography} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {AccountCircle, Lock} from "@material-ui/icons";
import Form from "../../../utils/ui/components/form/Form";
import {Actions} from "../../../bootstrap/actions";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";

interface Props extends NetworkingState {
    success: boolean;
    employeeId: number;
    employeeName: string;
    dispatch: (action: any) => void;
}

interface State {
    code: string;
}

class ChangeLoginCredentialsForm extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {code: ''}
    }

    componentDidMount() {
        let randomCode = RandomUtils.randomCode();
        this.setState({code: randomCode});
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.CHANGE_LOGIN_CREDENTIALS));
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <Header title={"الموظفين / تغير بيانات المستخدم"}/>

                <Typography variant={"headline"}>تغير معلومات حساب الموظف : {this.props.employeeName}</Typography>
                <br/><br/>

                <Grid>
                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        name={"username"}
                        placeholder={"اسم المستخدم"}
                        adornment={<AccountCircle/>}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{length: {minimum: 3}}}
                    />

                    <TextField
                        ref={ref => this.pushElementRef(ref)}
                        name={"password"}
                        placeholder={"كلمة المرور"}
                        adornment={<Lock/>}
                        type={"password"}
                        adornmentClassName="ar-adornment"
                        xs={12} sm={12} md={6} lg={6}
                        validationRules={{length: {minimum: 6}}}
                    />
                </Grid>

                <br/><br/>

                <Button variant={"contained"}
                        disabled={this.props.loading}
                        onClick={this.onSave} color={'primary'}>
                    حفظ
                </Button>

                {
                    this.props.loading &&
                    <div style={{padding: '16px 0'}}>
                        <LinearProgress color="primary" variant="query"/>
                    </div>
                }

            </div>
        )
    }

    onSave = () => {
        let data = this.getValues();
        data["id"] = this.props.employeeId;
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.POST, API_ROUTES.EMPLOYEES.EDIT_CERDENTIALS, Actions.CHANGE_LOGIN_CREDENTIALS, data);
    };

}

export default connect((store: any) => ({
    loading: store.ChangeLoginCredentials.loading,
    error: store.ChangeLoginCredentials.error,
    success: store.ChangeLoginCredentials.success
}))(ChangeLoginCredentialsForm);