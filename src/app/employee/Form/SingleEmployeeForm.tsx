import * as React from "react";
import Header from "../../components/Header";
import EmployeeForm from "./EmployeeForm";
import URLs from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {Button, LinearProgress} from "@material-ui/core";
import {Link} from "react-router-dom";
import EmployeeExtraInfo from "../../employee_extra/EmployeeExtraInfo";
import Session from "../../../utils/helpers/Session";

interface Props extends NetworkingState {
    employeeId: number;
    employee: any;
    dispatch: (action: any) => void;
}

class SingleEmployeeForm extends React.Component<Props> {

    componentDidMount() {
        const fetchEmployeeUrl = URLs.getApiUrl(`employees/${this.props.employeeId}`);
        this.props.dispatch(HttpGetAction(Actions.FETCH_EMPLOYEE, fetchEmployeeUrl));
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => {
            this.props.dispatch(HttpResetAction(Actions.UPDATE_EMPLOYEE))
        };
        handler.handleSuccessResponse();
    }

    render() {
        const session = Session.getUserObject();
        return (
            <div>
                <Header title={"الموظفين / معلومات موظف"}/>
                <EmployeeForm
                    readOnlyForm={true} departments={[]} departmentAsText displayImage
                    formValues={this.props.employee} loading={this.props.loading}
                    onSave={() => undefined}/>
                {
                    this.props.loading &&
                    <LinearProgress color={"secondary"}/>
                }

                {
                    (session.userType === "admin" && Object.keys(this.props.employee).length > 0) &&
                    <div>
                        <Header title={"العمليات"}/>
                        <Link
                            to={`/employees/${this.props.employee.id}/change-credentials/${this.props.employee.name}`}>
                            <Button style={{margin: "6px 0", minWidth: 120}} variant={"contained"} color={"primary"}
                                    size={"large"}>تعديل معلومات الدخول</Button>
                        </Link>
                        <Link to={`/employees/${this.props.employee.id}/edit`}>
                            <Button style={{margin: "6px 16px", minWidth: 120}} variant={"contained"} color={"primary"}
                                    size={"large"}>تعديل</Button>
                        </Link>
                        <Link to={`/employees/${this.props.employee.id}/delete/${this.props.employee.name}`}>
                            <Button style={{margin: "6px 0", minWidth: 120}} variant={"contained"} color={"primary"}
                                    size={"large"}>حذف</Button>
                        </Link>
                    </div>
                }

                <br/>
                <br/>

                <EmployeeExtraInfo/>
            </div>
        )
    }
}

export default connect((store: any) => ({
    employee: store.Employee.object,
    loading: store.Employee.loading,
    error: store.Employee.error
}))(SingleEmployeeForm)