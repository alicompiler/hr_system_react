import * as React from "react";
import Header from "../../components/Header";
import EmployeeForm from "./EmployeeForm";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import {connect} from "react-redux";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import HttpActionWithFileDispatcher from "../../../my_framework/data/redux/HttpActionWithFileDispatcher";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";

interface Props extends NetworkingState {
    employeeId: number;
    success?: boolean;
    departments?: any[];
    employee: any;
    employeeLoading?: boolean;
    employeeError?: boolean;
    dispatch: (action: any) => void;
}

class EditForm extends React.Component<Props> {

    componentDidMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET, API_ROUTES.DEPARTMENTS.DEPARTMENT_LIST, Actions.FETCH_DEPARTMENTS);
        HttpActionDispatcher.dispatch(this.props.dispatch, HttpMethod.GET,
            API_ROUTES.EMPLOYEES.SINGLE.with(this.props.employeeId), Actions.FETCH_EMPLOYEE);
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterSuccess = () => {
            this.props.dispatch(HttpResetAction(Actions.UPDATE_EMPLOYEE))
        };
        handler.handleSuccessResponse();
    }

    render() {
        return (
            <div>
                <Header title={"الموظفين / تعديل معلومات موظف"}/>
                <EmployeeForm
                    readOnlyForm={this.props.employeeError || this.props.employeeLoading}
                    departments={this.props.departments!}
                    formValues={this.props.employee}
                    loading={this.props.loading} buttonTitle={"تعديل"}
                    onSave={this.onSave}/>
            </div>
        )
    }

    onSave = (data: FormData) => {
        data.append("id", String(this.props.employeeId));
        HttpActionWithFileDispatcher.dispatchWithFile(this.props.dispatch,
            API_ROUTES.EMPLOYEES.EDIT, Actions.UPDATE_EMPLOYEE, data);
    }
}

export default connect((store: any) => ({
    loading: store.UpdateEmployee.loading,
    error: store.UpdateEmployee.error,
    success: store.UpdateEmployee.success,
    employee: store.Employee.object,
    loadingEmployee: store.Employee.loading,
    errorEmployee: store.Employee.error,
    departments: store.Departments.array
}))(EditForm)