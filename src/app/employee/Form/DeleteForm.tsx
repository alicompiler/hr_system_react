import * as React from "react";
import Header from "../../components/Header";
import NetworkingState from "../../../lib/redux++/reducer/state/NetworkingState";
import RandomUtils from "../../../utils/helpers/RandomUtils";
import {Button, LinearProgress, Typography} from "@material-ui/core";
import TextField from "../../../utils/ui/components/form/components/TextField";
import {Lock} from "@material-ui/icons";
import Form from "../../../utils/ui/components/form/Form";
import {Actions} from "../../../bootstrap/actions";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {connect} from "react-redux";
import SuccessStateActionHandler from "../../../utils/other/SuccessStateActionHandler";
import HttpResetAction from "../../../lib/redux++/action/HttpResetAction";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";

interface Props extends NetworkingState {
    success?: boolean;
    employeeId: number;
    employeeName: string;
    dispatch: (action: any) => void;
}

interface State {
    code: string;
}

class DeleteForm extends Form<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {code: ''}
    }

    componentDidMount() {
        let randomCode = RandomUtils.randomCode();
        this.setState({code: randomCode});
    }

    componentDidUpdate() {
        const handler = new SuccessStateActionHandler(this.props);
        handler.afterAny = () => {
            this.props.dispatch(HttpResetAction(Actions.DELETE_EMPLOYEE));
        };
        handler.handleSuccessResponse();
    }

    render() {
        const title = "هل انت متاكد من حذف الموظف : " + this.props.employeeName;
        const message = 'لكي تكمل عملية الحذف ، عليك ادخال الرمز التالي : ' + this.state.code;
        return (
            <div>
                <Header title={"الموظفين / حذف موظف"}/>
                <Typography variant='headline'>{title}</Typography>
                <br/>
                <Typography variant='subheading'>{message}</Typography>

                <TextField
                    ref={ref => this.pushElementRef(ref)}
                    name={"code"}
                    placeholder={"الرمز"}
                    readOnly={this.props.success}
                    adornment={<Lock/>}
                    adornmentClassName="ar-adornment"
                    xs={12} md={6} lg={4}
                    type={"number"}
                    validationRules={{matchValue: this.state.code}}
                />

                <br/><br/>

                <Button variant={"contained"}
                        disabled={this.props.loading || this.props.success}
                        onClick={this.onDelete} color={'primary'}>
                    حذف
                </Button>

                {
                    this.props.loading &&
                    <div style={{padding: '16px 0'}}>
                        <LinearProgress color="primary" variant="query"/>
                    </div>
                }

            </div>
        )
    }

    onDelete = () => {
        const data = {id: this.props.employeeId};
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.POST, API_ROUTES.EMPLOYEES.DELETE, Actions.DELETE_EMPLOYEE, data);
    };

}

export default connect((store: any) => ({
    loading: store.DeleteEmployee.loading,
    error: store.DeleteEmployee.error,
    success: store.DeleteEmployee.success
}))(DeleteForm);