import * as React from "react";
import {RouteComponentProps} from "react-router";
import LEAComponent, {LEAComponentProps} from "../../../my_framework/component/networking/LEAComponent";
import EmployeesCollection from "./EmployeesCollection";
import HttpActionDispatcher from "../../../my_framework/data/redux/HttpActionDispatcher";
import {HttpMethod} from "../../../lib/redux++/action/HttpMethod";
import {API_ROUTES} from "../../../utils/helpers/URLs";
import {Actions} from "../../../bootstrap/actions";
import {ReduxStore} from "../../../bootstrap/store";
import {connect} from "react-redux";

interface Props extends LEAComponentProps {
    route: RouteComponentProps;
    employees: any[];
}

class EmployeesList extends LEAComponent<Props> {

    protected dispatchComponentOnWillMount() {
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.GET, API_ROUTES.EMPLOYEES.ALL, Actions.FETCH_EMPLOYEES);
    }

    protected renderContent(): any {
        return <EmployeesCollection employees={this.props.employees}
                                    loading={this.props.loading} error={this.props.error}
                                    onSearch={this.onSearch} route={this.props.route}/>
    }

    protected isError() {
        return false;
    }

    protected isLoading() {
        return false;
    }

    private onSearch = (value: string) => {
        const data = {query: value};
        HttpActionDispatcher.dispatch(this.props.dispatch,
            HttpMethod.GET, API_ROUTES.EMPLOYEES.FIND, Actions.FETCH_EMPLOYEES, data);
    }
}

export default connect((store: ReduxStore) => ({
    loading: store.Employees.loading,
    error: store.Employees.error,
    employees: store.Employees.array,
}))(EmployeesList);