import * as React from "react";
import {Button, CardActions, Divider, Paper, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Color} from "../../../bootstrap/theme";
import "../../../styles/common/table_and_grid.css";
import URLs from "../../../utils/helpers/URLs";

interface Props {
    employee: any;
    isAdmin: boolean;
}

export default class EmployeeGridItem extends React.Component<Props> {
    render() {
        return (
            <Paper className="grid-item">
                <div className="image">
                    <img src={URLs.getImage("profile_images", this.props.employee.image)}/>
                </div>
                <Typography variant={"subheading"} align={"center"}>{this.props.employee.name}</Typography>
                <Typography style={{padding: 8, color: "#111"}} variant={"caption"} align={"center"}>
                    {this.props.employee.department ? this.props.employee.department : "غير محدد"}
                </Typography>
                <Divider/>
                <div className="details">
                    <Typography variant={"body1"}>{this.props.employee.email}</Typography>
                    <Typography variant={"body1"}>{this.props.employee.phone}</Typography>
                    <Typography variant={"body1"}>{this.props.employee.jobTitle}</Typography>
                </div>
                <Divider/>


                <CardActions>
                    <Link to={"/employees/" + this.props.employee.id}>
                        <Button size="small" color="primary">عرض</Button>
                    </Link>
                    {
                        this.props.isAdmin &&
                        <>
                            <Link to={`/employees/${this.props.employee.id}/edit`}>
                                <Button size="small" color="primary">تعديل</Button>
                            </Link>
                            <Link to={`/employees/${this.props.employee.id}/delete/${this.props.employee.name}`}>
                                <Button size="small" color="primary" style={{color: Color.error}}>حدف</Button>
                            </Link>
                        </>
                    }
                </CardActions>

            </Paper>
        )
    }
}