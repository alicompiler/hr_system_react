import * as React from "react";
import Table from "../../generic/model_manager/Main/components/Table";
import Grid from "../../generic/model_manager/Main/components/Grid";
import ActionBar from "../../../my_framework/ui/utils_componenet/ActionBar";
import {Routes} from "../../../bootstrap/routes";
import {RouteComponentProps} from "react-router";
import EmployeeGridItem from "./EmployeeGridItem";
// import URLs from "../../../utils/helpers/URLs";
// import AvatarWithTitle from "../../../my_framework/ui/utils_componenet/AvatarWithTitle";
import {getScreenSizeType} from "../../../utils/helpers/ScreenSizeUtils";
import {Divider, Typography} from "@material-ui/core";
import Session from "../../../utils/helpers/Session";

interface Props {
    employees: any[];
    loading: boolean;
    error: boolean;
    onSearch: (value: string) => void;
    route: RouteComponentProps;
}

interface State {
    displayType: "grid" | "table";
}

export default class EmployeesCollection extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {displayType: "table"}
    }

    componentWillMount() {
        window.addEventListener("resize", this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize = () => {
        this.changeDisplayType(this.state.displayType);
    };

    changeDisplayType = (type: "grid" | "table") => {
        if (getScreenSizeType() === "mobile" || getScreenSizeType() === "tablet") {
            type = "grid";
        }
        this.setState({displayType: type})
    };

    render() {
        return (<div>
                <ActionBar
                    action={true}
                    actionLink={"employees/new"}
                    changeDisplayType={true}
                    onChangeDisplayType={this.changeDisplayType}
                    onSearch={this.onSearch}
                />
                <br/><br/>
                {
                    this.state.displayType === "grid" ?
                        this.renderCollectionAsGrid()
                        :
                        this.renderCollectionAsTable()
                }
                {
                    (!this.props.loading && !this.props.error) &&
                    <>
                        <br/><Divider/><br/><br/>
                        <Typography variant={"subheading"}>
                            العدد الكلي :
                            {this.props.employees.length}
                        </Typography>
                    </>
                }
            </div>
        )
    }

    renderCollectionAsTable = () => {
        return (
            <Table onRowSelected={this.onRowSelected!} loading={this.props.loading}
                   error={this.props.error}
                   data={this.props.employees}
                   columns={columns}
                   itemToArrayCallback={this.itemToArray}
                   title={"قائمة الموظفين"}/>
        )
    };

    onRowSelected = (data: any) => {
        let id = data[0];
        this.props.route.history.push(Routes.Employee.singleEmployee(id));
    };

    itemToArray = (item: any) => {
        return [item.id, item.name
            , item.email, item.phone, item.jobTitle];
    };

    renderCollectionAsGrid = () => {
        const isAdmin = Session.getUserObject().userType === "admin";
        return (
            <Grid loading={this.props.loading}
                  error={this.props.error} data={this.props.employees}
                  gridItemRender={(employee: any) => <EmployeeGridItem isAdmin={isAdmin} employee={employee}/>}/>
        )
    };

    onSearch = (value: string) => {
        this.props.onSearch(value);
    }
}

const columns: any[] = [
    "ID",
    "الموظف",
    "البريد الالكتروني",
    "الهاتف",
    "العنوان الوظيفي"
];