import {ISelectOption} from "../../../utils/ui/components/form/components/Select";

export default class EmployeeListToSelectOptionsAdapter {
    private employees: any[];

    constructor(employees: any[]) {
        this.employees = employees;
    }

    public toSelectOptions(): ISelectOption[] {
        return this.employees.map((item: any) => {
            return {text: item.name, value: item.id};
        });
    }
}