import URLs, {API_ROUTES} from "../../../utils/helpers/URLs";
import {HttpGetAction} from "../../../utils/redux++wrapper/Actions";
import {Actions} from "../../../bootstrap/actions";

export function dispatchFetchEmployeeListAction(dispatch: (action: any) => void) {
    const url = URLs.getApiUrl(API_ROUTES.EMPLOYEES.EMPLOYEE_LIST);
    const action = HttpGetAction(Actions.FETCH_EMPLOYEES_LIST, url);
    dispatch(action);
}