export const Routes = {
    Main: {
        home: "/",
        login: "/login",
        logout: "/logout"
    },
    Employee: {
        find: "/employees/find",
        singleEmployee: (id: number): string => `/employees/${id}`,
        index: "/employees",
        createNew: "/employees/new",
        edit: "/employees/edit",
        delete: "/employees/delete"
    }
};