import {combineReducers} from "redux";
import {reducerWrapper} from "../lib/redux++/reducer/IReducer";
import FetchObjectReducer from "../lib/redux++/reducer/FetchObjectReducer";
import {Actions} from "./actions";
import Reducer from "../lib/redux++/reducer/Reducer";
import ActionState from "../lib/redux++/action/ActionState";
import FetchArrayReducer from "../lib/redux++/reducer/FetchArrayReducer";
import {routerReducer} from 'react-router-redux';
import SuccessStateHttpReducer from "../lib/redux++/reducer/SuccessStateHttpReducer";
import {ReduxStore} from "./store";

const drawerReducer = new Reducer({open: false}, [
    new ActionState(Actions.OPEN_DRAWER, {open: true}),
    new ActionState(Actions.CLOSE_DRAWER, {open: false})
]);

const notificationViewOpenState = new Reducer({open: false}, [
    new ActionState(Actions.NOTIFICATION_VIEW_OPEN, {open: true}),
    new ActionState(Actions.NOTIFICATION_VIEW_CLOSE, {open: false})
]);

const whoWatchAnnouncementReducer = new Reducer({open: false}, [
    new ActionState(Actions.OPEN_WHO_WATCH_ANNOUNCEMENT_DIALOG, {open: true}),
    new ActionState(Actions.CLOSE_WHO_WATCH_ANNOUNCEMENT_DIALOG, {open: false})
]);

const firstLoadReducer = new Reducer({firstTime: false}, [
    new ActionState(Actions.MAIN_DID_LOAD, {firstTime: true})
]);

const snackbarReducer = new Reducer({open: false, variant: "none", autoHideDuration: 4000, message: ""},
    [new ActionState(Actions.SNACKBAR_ACTION, {}, true, payload => payload)]
);


const reducers: ReduxStore = {
    SnackbarReducer: reducerWrapper(snackbarReducer),
    FirstTime: reducerWrapper(firstLoadReducer),
    Drawer: reducerWrapper(drawerReducer),
    LoginReducer: reducerWrapper(new FetchObjectReducer(Actions.LOGIN)),
    __GENERIC_REDUCER: reducerWrapper(new FetchArrayReducer("__GENERIC_MAIN_FETCH")),
    UserInfo: reducerWrapper(new FetchObjectReducer(Actions.FETCH_CURRENT_USER_INFO)),
    Employees: reducerWrapper(new FetchArrayReducer(Actions.FETCH_EMPLOYEES)),
    Employee: reducerWrapper(new FetchObjectReducer(Actions.FETCH_EMPLOYEE)),
    CreateEmployee: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_EMPLOYEE)),
    UpdateEmployee: reducerWrapper(new SuccessStateHttpReducer(Actions.UPDATE_EMPLOYEE)),
    DeleteEmployee: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_EMPLOYEE)),
    ChangeLoginCredentials: reducerWrapper(new SuccessStateHttpReducer(Actions.CHANGE_LOGIN_CREDENTIALS)),
    Departments: reducerWrapper(new FetchArrayReducer(Actions.FETCH_DEPARTMENTS, true)),

    // LEAVE REQUEST REDUCERS
    SendLeaveRequest: reducerWrapper(new SuccessStateHttpReducer(Actions.SEND_LEAVE_REQUEST)),
    MyLeaveRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_LEAVE_REQUESTS)),
    AllLeaveRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ALL_LEAVE_REQUESTS)),
    ManagerLeaveRequest: reducerWrapper(new FetchArrayReducer(Actions.LEAVE_REQUEST_MANAGER_REPORT)),

    ToManageRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_LEAVE_REQUESTS_TO_MANAGE)),
    SingleLeaveRequest: reducerWrapper(new FetchObjectReducer(Actions.FETCH_LEAVE_REQUEST)),
    SendLeaveRequestNote: reducerWrapper(new SuccessStateHttpReducer(Actions.MANAGE_LEAVE_REQUEST)),


    WhoWatchAnnouncementDialogReducer: reducerWrapper(whoWatchAnnouncementReducer),
    WriteAnnouncement: reducerWrapper(new SuccessStateHttpReducer(Actions.WRITE_ANNOUNCEMENT)),
    RecentAnnouncements: reducerWrapper(new FetchArrayReducer(Actions.FETCH_RECENT_ANNOUNCEMENTS)),
    UnwatchedAnnouncements: reducerWrapper(new FetchArrayReducer(Actions.FETCH_UNWATCHED_ANNOUNCEMENTS)),
    AnnouncementsByDate: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ANNOUNCEMENTS_BY_DATE)),
    WhoWatchAnnouncement: reducerWrapper(new FetchArrayReducer(Actions.FETCH_WHO_WATCH_ANNOUNCEMENT)),
    UnreviewedAnnouncements: reducerWrapper(new FetchArrayReducer(Actions.FETCH_UNREVIEWED_ANNOUNCEMENTS)),
    SavedAnnouncements: reducerWrapper(new FetchArrayReducer(Actions.FETCH_SAVED_ANNOUNCEMENTS)),
    DeleteAnnouncement: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_ANNOUNCEMENT)),
    ReviewAnnouncement: reducerWrapper(new SuccessStateHttpReducer(Actions.REVIEW_ANNOUNCEMENT)),
    SaveUnsaveAnnouncement: reducerWrapper(new SuccessStateHttpReducer(Actions.TOGGLE_SAVE_ANNOUNCEMENT)),

    DepartmentList: reducerWrapper(new FetchArrayReducer(Actions.FETCH_DEPARTMENTS_LIST)),
    EmployeeList: reducerWrapper(new FetchArrayReducer(Actions.FETCH_EMPLOYEES_LIST)),


    AddDayOff: reducerWrapper(new FetchObjectReducer(Actions.ADD_DAY_OFF)),
    FetchDayOff: reducerWrapper(new FetchArrayReducer(Actions.FETCH_DAYS_OFF)),
    DeleteDayOff: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_DAY_OFF)),

    AddShiftOff: reducerWrapper(new FetchObjectReducer(Actions.ADD_SHIFT_OFF)),
    FetchShiftOff: reducerWrapper(new FetchArrayReducer(Actions.FETCH_SHIFT_OFF)),
    DeleteShiftOff: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_SHIFT_OFF)),


    AddExceptionShiftOff: reducerWrapper(new FetchObjectReducer(Actions.ADD_EXCEPTION_SHIFT_OFF)),
    FetchExceptionShiftOff: reducerWrapper(new FetchArrayReducer(Actions.FETCH_EXCEPTION_SHIFT_OFF)),
    DeleteExceptionShiftOff: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_EXCEPTION_SHIFT_OFF)),

    FetchAudienceTypes: reducerWrapper(new FetchArrayReducer(Actions.FETCH_AUDIENCE_TYPES)),
    AudienceInfo: reducerWrapper(new FetchObjectReducer(Actions.FETCH_AUDIENCE)),
    EditAudience: reducerWrapper(new SuccessStateHttpReducer(Actions.EDIT_AUDIENCE)),


    SaveAddon: reducerWrapper(new SuccessStateHttpReducer(Actions.SAVE_ADDON)),
    FetchAddons: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ADD_ON)),
    DeleteAddon: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_ADDON)),

    MyAudience: reducerWrapper(new FetchObjectReducer(Actions.FETCH_MY_AUDIENCE)),
    HomeAnnouncements: reducerWrapper(new FetchArrayReducer(Actions.FETCH_RECENT_ANNOUNCEMENTS_FOR_HOME_PAGE)),
    MyProfile: reducerWrapper(new FetchObjectReducer(Actions.FETCH_MY_PROFILE)),
    ChangeProfileImage: reducerWrapper(new SuccessStateHttpReducer(Actions.CHANGE_PROFILE_IMAGE)),

    DeleteAttachment: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_ATTACHMENT)),
    EmployeeAttachments: reducerWrapper(new FetchArrayReducer(Actions.EMPLOYEE_ATTACHMENTS)),
    UploadEmployeeFile: reducerWrapper(new FetchObjectReducer(Actions.UPLOAD_EMPLOYEE_FILE)),

    SendLoanRequest: reducerWrapper(new SuccessStateHttpReducer(Actions.SEND_LOAN_REQUEST)),
    SponsoringRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_SPONSORING_REQUESTS)),
    MyLoanRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_LOAN_REQUESTS)),
    WaitingLoanRequests: reducerWrapper(new FetchArrayReducer(Actions.FETCH_WAITING_LOAN_REQUESTS)),
    LoanRequest: reducerWrapper(new FetchObjectReducer(Actions.FETCH_LOAN_REQUEST)),
    SendSponsorAcceptanceStatus: reducerWrapper(new SuccessStateHttpReducer(Actions.SEND_ACCEPTANCE_STATUS)),
    ManageLoanRequest: reducerWrapper(new SuccessStateHttpReducer(Actions.MANAGE_LOAN_REQUEST)),
    DailyAudienceReport: reducerWrapper(new FetchArrayReducer(Actions.FETCH_YESTERDAY_AUDIENCE_REPORT)),
    MonthlyAudienceReport: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MONTHLY_AUDIENCE_REPORT)),
    MonthlyEmployeeAudienceReport: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MONTHLY_EMPLOYEE_AUDIENCE_REPORT)),
    CompletedLoans: reducerWrapper(new FetchArrayReducer(Actions.FETCH_COMPLETED_LOANS)),
    AllLoans: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ALL_LOANS)),
    SendAudience: reducerWrapper(new SuccessStateHttpReducer(Actions.SEND_AUDIENCE_DATA)),
    AudienceByDate: reducerWrapper(new FetchArrayReducer(Actions.FETCH_AUDIENCE_BY_DATE)),
    MySalaryAddons: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_SALARY_ADDONS)),
    MySalaryAddonsBetweenDates: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_SALARY_ADDONS_BETWEEN_DATES)),
    AddonsForEmployee: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ADDONS_FOR_EMPLOYEE)),
    SalaryReport: reducerWrapper(new FetchArrayReducer(Actions.FETCH_SALARY_REPORT)),
    DeleteLeaveRequest: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_LEAVE_REQUEST)),

    SuggestionAndIssue: reducerWrapper(new SuccessStateHttpReducer(Actions.SEND_SUGGESTION_OR_ISSUE)),
    DeleteSuggestion: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_SUGGESTION)),
    RecentSuggestionAndIssue: reducerWrapper(new FetchArrayReducer(Actions.FETCH_RECENT_SUGGESTION_OR_ISSUE)),

    VoteOnPoll: reducerWrapper(new SuccessStateHttpReducer(Actions.VOTE_ON_POLL)),
    CreatePoll: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_POLL)),
    ActivePolls: reducerWrapper(new FetchArrayReducer(Actions.FETCH_ACTIVE_POLLS)),
    FinishedPolls: reducerWrapper(new FetchArrayReducer(Actions.FETCH_FINISHED_POLLS)),
    MyPolls: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_POLLS)),

    DeletePoll: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_POLL)),


    CreateProjectIdea: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_PROJECT_IDEA)),
    ProjectIdeas: reducerWrapper(new FetchArrayReducer(Actions.FETCH_PROJECT_IDEAS)),
    ReplayOnProjectIdea: reducerWrapper(new SuccessStateHttpReducer(Actions.REPLAY_ON_PROJECT_IDEA)),
    MyProjectIdeas: reducerWrapper(new FetchArrayReducer(Actions.FETCH_MY_PROJECT_IDEAS)),
    DeleteProjectIdea: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_PROJECT_IDEA)),

    CreateSalaryRise: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_SALARY_RISE)),
    SalaryRiseList: reducerWrapper(new FetchArrayReducer(Actions.FETCH_SALARY_RISE)),
    DeleteSalaryRise: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_SALARY_RISE)),

    CreateAppreciationLetter: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_APPRECIATION_LETTER)),
    AppreciationLetterList: reducerWrapper(new FetchArrayReducer(Actions.FETCH_APPRECIATION_LETTER)),
    DeleteAppreciationLetter: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_APPRECIATION_LETTER)),

    CreateEmployeeRating: reducerWrapper(new SuccessStateHttpReducer(Actions.CREATE_EMPLOYEE_RATING)),
    EmployeeRatingList: reducerWrapper(new FetchArrayReducer(Actions.FETCH_EMPLOYEE_RATING)),
    DeleteEmployeeRating: reducerWrapper(new SuccessStateHttpReducer(Actions.DELETE_EMPLOYEE_RATING)),

    Notifications: reducerWrapper(new FetchArrayReducer(Actions.FETCH_NOTIFICATIONS)),
    NotificationViewOpenState: reducerWrapper(notificationViewOpenState),

    LeaveBalance: reducerWrapper(new FetchArrayReducer(Actions.FETCH_LEAVE_BALANCE)),

    routing: routerReducer
};


export default combineReducers(reducers as any);