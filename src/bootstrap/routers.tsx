import * as React from "react"
import {Route, RouteComponentProps, Switch} from "react-router-dom";
import Login from "../app/main/login/index";
import Session from "../utils/helpers/Session";
import Main from "../app/main/Main";

interface Props {
    isLoggedIn: boolean
}

export default class MainApplicationRoutes extends React.Component<Props> {

    render() {
        if (!this.props.isLoggedIn) {
            return <Switch>
                <Route component={(route: RouteComponentProps) => <Login route={route}/>}/>
            </Switch>;
        }

        return (
            <Switch>
                <Route exact path="/logout" component={this.logout}/>
                <Route path="/" component={() => <Main/>}/>
            </Switch>
        );
    }

    logout = () => {
        Session.logout();
        window.location.href = window.location.origin + "/login";
        return null;
    }
}