import {createMuiTheme} from "@material-ui/core";

const theme = createMuiTheme({
    direction: 'rtl',
    palette: {
        primary: {
            light: '#6573c3',
            main: '#3f51b5',
            dark: '#2c387e',
            //contrastText: getContrastText('#3f51b5'),
        },
        secondary: {
            light: '#81C784',
            main: '#4CAF50',
            dark: '#388E3C',
            //contrastText: getContrastText('#ff1744'),
        },
        error: {
            light: '#F06292',
            main: '#E91E63',
            dark: '#880E4F',
            //contrastText: getContrastText('#E91E63'),
        },
        text: {
            primary: '#111',
            secondary: '#FFF'
        }
    },
    typography: {
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            'Droid Arabic Kufi',
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },

});


export default theme;


export const Color = {
    primary: "#3f51b5",
    secondary: "#4CAF50",
    error: "#E91E63",
};