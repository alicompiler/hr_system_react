import {applyMiddleware, createStore} from "redux";
import reducers from "./reducers";
import promiseMiddleware from "redux-promise-middleware"
import * as createLogger from "redux-logger"

const middleware = applyMiddleware(promiseMiddleware(), createLogger.logger);
export default createStore(reducers, middleware);


export interface ReduxStore {
    SnackbarReducer: any;
    FirstTime: any;
    Drawer: any;
    LoginReducer: any;
    __GENERIC_REDUCER: any;
    UserInfo: any;
    MyProfile: any;
    EmployeeAttachments: any;
    DeleteAttachment: any;
    Employees: any;
    CreateEmployee: any;
    EmployeeList: any;

    MonthlyEmployeeAudienceReport: any;
    MonthlyAudienceReport: any;
    DailyAudienceReport: any;

    SingleLeaveRequest: any;
    SendLeaveRequestNote: any;


    [property: string]: any
}
