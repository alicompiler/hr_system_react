import * as moment from "moment";
import * as validator from 'validate.js';

import {getTimeAsString} from "../../utils/helpers/DateHelper";

export default function setupValidator() {

    const dateExtendObject = {
        parse: function (value: any) {
            return +moment.utc(value);
        },
        format: function (value: any, options: any) {
            let format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
            return moment.utc(value).format(format);
        }
    };

    validator.validators.timeFromTimePicker = function (value: any) {
        let time = getTimeAsString(value);
        let pattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        return pattern.test(time) ? undefined : 'time is not valid';
    };


    validator.validators.optionalEmail = (value: string) => {
        if (!value) {
            return;
        }
        return validator.single(value, {email: true});
    };

    validator.validators.matchValue = function (value: string, match: any) {
        if (value == match)
            return;
        return "value not match";
    };

    validator.validators.timeHHmm = function (value: string, options: any) {
        if (options.allowEmpty === true) {
            if (value === "")
                return undefined;
        }

        if (!value)
            return "value not valid";
        if (value.length !== 5)
            return "time should be with format HH:mm";
        if (value.indexOf(":") !== 2)
            return "time should be with format HH:mm";
        let HH = Number(value.split(":")[0]);
        let mm = Number(value.split(":")[1]);
        if (HH < 0 || HH > 23)
            return "hours should be from 0 to 23";
        if (mm < 0 || mm > 59)
            return "minutes should be form 0 to 59";

        return undefined;
    };

    validator.extend(validator.validators.datetime, dateExtendObject);

}