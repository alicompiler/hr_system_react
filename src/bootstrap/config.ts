const DEBUG = false;

export const APP_CONFIG = {
    SERVER: {
        BASE_URL: DEBUG ? "http://localhost:8000/" : "http://api.tareekofuk.tech/",
    }
};
