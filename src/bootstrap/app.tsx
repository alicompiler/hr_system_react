import * as React from "react";
import {Provider} from "react-redux";
import store from "./store";
import {BrowserRouter} from "react-router-dom";
import MainApplicationRoutes from "./routers";
import {MuiThemeProvider, withTheme} from "@material-ui/core";
import "./styles_loader";
import theme from "./theme";
import Session from "../utils/helpers/Session";
import setupValidator from "./setup/validation";
import SnackbarContainer from "./main_components/snackbar/SnackbarContainer";
import 'typeface-roboto'

import JssProvider from 'react-jss/lib/JssProvider';
import {create} from 'jss';
import {createGenerateClassName, jssPreset} from '@material-ui/core/styles';
import {HttpGetAction} from "../utils/redux++wrapper/Actions";
import {Actions} from "./actions";
import URLs, {API_ROUTES} from "../utils/helpers/URLs";

const generateClassName = createGenerateClassName({dangerouslyUseGlobalCSS: true});
const jss = create(jssPreset());


const url = URLs.getApiUrl(API_ROUTES.NOTIFICATION.ALL);
store.dispatch(HttpGetAction(Actions.FETCH_NOTIFICATIONS, url));
setInterval(() => {
    const notificationViewOpen = (store.getState() as any).NotificationViewOpenState.open;
    if (notificationViewOpen) {
        return;
    }
    const url = URLs.getApiUrl(API_ROUTES.NOTIFICATION.ALL);
    store.dispatch(HttpGetAction(Actions.FETCH_NOTIFICATIONS, url));
}, 1000 * 60 * 5);

setupValidator();
const App = () => {
    const isLoggedIn = Session.isLoggedIn();
    return (
        <Provider store={store}>
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <MuiThemeProvider theme={theme}>
                    <SnackbarContainer/>
                    <BrowserRouter>
                        <MainApplicationRoutes isLoggedIn={isLoggedIn}/>
                    </BrowserRouter>
                </MuiThemeProvider>
            </JssProvider>
        </Provider>
    )
};

export default withTheme()(App);