import * as React from "react";
import {IconButton, Snackbar} from "@material-ui/core";
import {Close} from "@material-ui/icons";
import "../../../styles/controls/snackbar.css";
import {connect} from "react-redux";
import SnackbarAction from "./SnackbarAction";

interface Props {
    open: boolean;
    variant: string;
    message: string;
    autoHideDuration: number;
    dispatch: (action: any) => void;
}

class SnackbarContainer extends React.Component<Props> {

    render() {
        const autoHideDuration = this.props.autoHideDuration;
        return (
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'left',}}
                open={this.props.open}
                autoHideDuration={autoHideDuration}
                onClose={this.handleClose}
                className={`snackbar ${this.props.variant}`}
                message={<span id="message-id">{this.props.message}</span>}
                action={[
                    <IconButton key="close" aria-label="Close" color="inherit" onClick={this.handleClose}>
                        <Close/>
                    </IconButton>,
                ]}
            />

        )
    }

    handleClose = () => {
        this.props.dispatch(SnackbarAction("", false, "none", 5000))
    }

}

export default connect((store: any) => ({
    open: store.SnackbarReducer.open,
    message: store.SnackbarReducer.message,
    variant: store.SnackbarReducer.variant,
    autoHideDuration: store.SnackbarReducer.autoHideDuration
}))(SnackbarContainer);

