import {Actions} from "../../actions";

export default function SnackbarAction(message: string = "", open: boolean = true,
                                       variant: "success" | "error" | "warning" | "info" | "none" = "none",
                                       autoHideDuration: number = 4000) {
    return {
        type: Actions.SNACKBAR_ACTION,
        payload: {
            open: open,
            message: message,
            variant: variant,
            autoHideDuration: autoHideDuration
        }
    }
}