import GetAction from "../../lib/redux++/action/HttpGetAction";
import PostAction from "../../lib/redux++/action/HttpPostAction";
import {AxiosRequestConfig} from "axios";
import Session from "../helpers/Session";

function appendSessionToHeaders(options: AxiosRequestConfig) {
    let headers = options.headers;
    let newHeaders = {};
    const attachedHeader = {UserSession: Session.getUserObject().session};
    if (headers) {
        newHeaders = {...headers, ...attachedHeader};
    } else {
        newHeaders = attachedHeader;
    }
    options.headers = newHeaders;
}

export const HttpGetAction = function (type: string, url: string, params: object = {}, options: AxiosRequestConfig = {}) {
    appendSessionToHeaders(options);
    return GetAction(type, url, params, options);
};

export const HttpPostAction = function (type: string, url: string, data: object = {}, options: AxiosRequestConfig = {}) {
    appendSessionToHeaders(options);
    return PostAction(type, url, data, options);
};