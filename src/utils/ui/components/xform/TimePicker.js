import React from 'react'
import {TimePicker as MaterialTimePicker} from 'material-ui-pickers';
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import Wrapper from './Wrapper';
import {generateAdornmentStyle, generateStyle, generateVerticalMargin} from './helper';
import LabelOrNull from './LabelOrNull';
import FormFieldComponent, {FieldType} from "./FormFieldComponent";

export default class TimePicker extends FormFieldComponent
{

    constructor(props)
    {
        super(props, FieldType.TIME, props.validationRules);
        this.state = {value: '', error: false};
    }

    render()
    {
        let style = generateStyle(this.props);

        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <Wrapper asBlock={this.props.displayAsBlock} style={generateVerticalMargin(this.props)}>

                    <LabelOrNull label={this.props.label} spaceBetween={this.props.spaceBetween}
                                 inline={this.props.inline} width={this.props.labelWidth}/>

                    <MaterialTimePicker
                        style={style}
                        keyboard
                        clearable={this.props.clearable}
                        ampm={this.props.ampm}
                        invalidLabel={this.props.invalidLabel}
                        invalidDateMessage={this.props.invalidDateMessage}
                        adornmentPosition={this.props.adornmentPosition}
                        placeholder={this.props.placeholder}
                        InputAdornmentProps={{style: generateAdornmentStyle(this.props.rightToLeft)}}
                        mask={this.props.mask}
                        onChange={this.onChange}
                        value={this.state.value}
                        error={this.state.error}
                    />

                </Wrapper>

            </MuiPickersUtilsProvider>
        );
    }

}

TimePicker.defaultProps =
    {
        width: 250,
        ampm: false,
        placeholder: '',
        mask: value => (value ? [/\d/, /\d/, ':', /\d/, /\d/] : []),
        invalidDateMessage: null,
        adornmentPosition: 'end',
        clearable: true,
        vMargin: 12,
        spaceBetween: 10,
        rightToLeft: true,
        labelWidth: 'auto',
        inline: true,
        displayAsBlock: false,
        validationRules: null,
    };