import React from 'react'
import MaterialSelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import LabelOrNull from "./LabelOrNull";
import Wrapper from "./Wrapper";
import {generateStyle, generateVerticalMargin} from "./helper";
import FormFieldComponent, {FieldType} from "./FormFieldComponent";

export default class Select extends FormFieldComponent {
    constructor(props) {
        super(props, FieldType.SELECT, props.validationRules);
        this.state = {value: '', error: false};
    }

    render() {
        let style = generateStyle(this.props);

        return (
            <Wrapper asBlock={this.props.displayAsBlock} style={generateVerticalMargin(this.props)}>

                <LabelOrNull label={this.props.label} spaceBetween={this.props.spaceBetween}
                             inline={this.props.inline} width={this.props.labelWidth}/>

                <MaterialSelect
                    style={style}
                    placeholder={this.props.placeholder}
                    onChange={this.onChange}
                    error={this.state.error}
                    value={this.state.value}
                    disabled={this.props.disabled}
                    displayEmpty={this.props.displayEmpty}
                >
                    {
                        this.renderOptions()
                    }
                </MaterialSelect>
            </Wrapper>

        );
    }

    renderOptions = () => {
        let options = [];

        if (this.props.placeholder) {
            options.push(<MenuItem key='placeholder' value="" disabled>{this.props.placeholder}</MenuItem>);
        }

        let allOptions = this.props.options.map((option, index) => {
            return <MenuItem key={index} value={option.value}>
                {option.text}
            </MenuItem>
        });

        return options.concat(allOptions);
    };

}

Select.defaultProps = {
    width: 250,
    displayEmpty: true,
    vMargin: 12,
    marginLeft: 0,
    marginRight: 0,
    spaceBetween: 10,
    disabled: false,
    rightToLeft: true,
    labelWidth: 'auto',
    inline: true,
    displayAsBlock: false,
    validationRules: null,
};