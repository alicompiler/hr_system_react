import React from 'react';
import TextField from './TextField';


export default class TextArea extends React.Component {
    
    render() {
        return <TextField
            ref={ref => this.props.tRef && this.props.tRef(ref)}
            rightToLeft={this.props.rightToLeft}
            label={this.props.label}
            floatingLabel={this.props.floatingLabel}
            width={this.props.width}
            onChange={this.props.onChange}
            rows={this.props.rows}
            fullWidth={this.props.fullWidth}
            multiline={true}
            background={this.props.background}
            validationRules={this.props.validationRules}
            name={this.props.name}
            readOnly={this.props.readOnly}
            defaultValue={this.props.defaultValue}
            displayAsBlock
        />
    }
}

TextArea.defaultProps = {
    width: '100%',
    fullWidth: true,
    background: '#EEE',
    rows: 5,
    rightToLeft: true,
    validationRules: null,
    readOnly: false,
    defaultValue: ''
};