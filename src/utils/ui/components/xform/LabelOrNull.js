import React from 'react'
import { generateLabelStyle } from "./helper";
import HorizontalSpace from "./HorizontalSpace";


export default class LabelOrNull extends React.Component {
    render() {
        if (!this.props.label) {
            return null;
        }

        let labelStyle = generateLabelStyle(this.props);
        return [
            <label key={'label'} style={labelStyle}>{this.props.label}</label>,
            <HorizontalSpace key={'span'} space={this.props.inline ? this.props.spaceBetween : 0} />
        ];
    }
}

LabelOrNull.defaultProps = {
    label: null,
    width: 'auto',
    rightToLeft: true,
    inline: true
};