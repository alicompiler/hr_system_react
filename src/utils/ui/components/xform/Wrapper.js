import React from 'react'


export default class Wrapper extends React.Component {
    render() {
        let style = {display: this.props.asBlock ? 'block' : 'inline-block'};
        style = {...style, ...this.props.style};
        return (
            <div style={style}>
                {this.props.children}
            </div>
        );
    }
}

Wrapper.defaultProps = {
    style: {}
};