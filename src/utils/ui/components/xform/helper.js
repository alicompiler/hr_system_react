export function generateStyle(props) {
    let style = {};

    if (props.fullWidth === true) {
        style.width = '100%';
    }
    else {
        style.width = props.width;
    }

    if (props.inline) {
        style.marginTop = props.vMargin;
        style.marginBottom = props.vMargin;
    }

    style.marginLeft = props.marginLeft;
    style.marginRight = props.marginRight;
    return style;
}

export function generateLabelStyle(props) {
    let style = {};
    style.width = props.width;
    style.textAlign = props.rightToLeft && props.inline ? 'left' : 'right';
    style.display = 'inline-block';
    if (!props.inline) {
        style.display = 'block';
    }
    return style;
}

export function generateFloatingLabelStyle(props) {
    let style = {};
    style.textAlign = props.rightToLeft ? 'right' : 'left';
    style.zIndex = 1;
    style.padding = 8;
    if (props.rightToLeft) {
        style.left = 'unset';
        style.right = 0;
        style.transformOrigin = 'top right'
    }
    return style;
}


export function generateAdornmentStyle(rightToLeft) {
    return rightToLeft ? {marginRight: 8, marginLeft: 0} : {};
}


export function generateInputStyle(props) {
    let style = {
        textAlign: props.rightToLeft ? 'right' : 'left',
    };

    if (props.background) {
        style.backgroundColor = props.background;
        style.padding = 8
    }

    return style;
}

export function generateVerticalMargin(props) {
    let style = {};
    style.marginTop = props.vMargin;
    style.marginBottom = props.vMargin;
    return style;
}