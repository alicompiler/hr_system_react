import React from 'react';

import MaterialTextField from '@material-ui/core/TextField';

import {generateFloatingLabelStyle, generateInputStyle, generateStyle, generateVerticalMargin} from './helper';
import Wrapper from './Wrapper';
import {InputAdornment} from '@material-ui/core';
import LabelOrNull from './LabelOrNull';
import FormFieldComponent, {FieldType} from "./FormFieldComponent";

export default class TextField extends FormFieldComponent {
    constructor(props) {
        super(props, FieldType.TEXT, props.validationRules);
        this.state = {error: false, value: this.props.defaultValue};
    }

    componentWillReceiveProps(nextProps, nextContent) {
        if (this.state.value === '' && nextProps.defaultValue !== '') {
            this.setState({value: nextProps.defaultValue});
        }
    }

    render() {
        let style = generateStyle(this.props);
        return (
            <Wrapper asBlock={this.props.displayAsBlock} style={generateVerticalMargin(this.props)}>

                <LabelOrNull label={this.props.label} spaceBetween={this.props.spaceBetween}
                             inline={this.props.inline} width={this.props.labelWidth}/>

                <MaterialTextField
                    style={style}
                    placeholder={this.props.placeholder}
                    label={this.props.floatingLabel}
                    InputLabelProps={{style: generateFloatingLabelStyle(this.props)}}
                    inputProps={{style: generateInputStyle(this.props)}}
                    disabled={this.props.disabled}
                    error={this.state.error}
                    value={this.state.value}
                    onChange={this.onChange}
                    type={this.props.password ? 'password' : 'text'}
                    InputProps={this.getInputProps()}
                    fullWidth={this.props.fullWidth}
                    multiline={this.props.multiline}
                    rows={this.props.rows}
                />

            </Wrapper>
        );
    }


    getInputProps = () => {
        let props = {};

        if (this.props.adornment) {
            let adornmentKey = this.props.adornmentPosition + 'Adornment';
            props[adornmentKey] = this.getAdornment();
        }

        return props;
    };

    getAdornment = () => {
        return (
            <InputAdornment position={this.props.adornmentPosition}
                            style={{
                                marginRight: 4,
                                marginLeft: 4
                            }}>
                {this.props.adornment}
            </InputAdornment>
        )
    };
}


TextField.defaultProps = {
    width: 250,
    displayEmpty: true,
    vMargin: 12,
    marginLeft: 0,
    marginRight: 0,
    spaceBetween: 10,
    disabled: false,
    rightToLeft: true,
    labelWidth: 'auto',
    adornmentPosition: 'start',
    placeholder: '',
    inline: true,
    fullWidth: false,
    displayAsBlock: false,
    floatingLabel: '',
    rows: 1,
    multiline: false,
    validationRules: null,
    password: false,
    readOnly: false,
    defaultValue: ''
};