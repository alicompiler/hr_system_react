import React from 'react'


export default class FormComponent extends React.Component {

    constructor(props) {
        super(props);
        this.fields = [];
    }

    anyError = () => {

        let errors = 0;
        let values = {};

        this.fields.forEach(ref => {
            let component = ref[Object.keys(ref)[0]];
            component.validate();
            !component.isStateValid() && errors++;
            values[component.getName()] = component.getState();
        });

        return errors > 0;
    };


    getValues = () => {
        let errors = 0;
        let values = {};

        this.fields.forEach(ref => {
            let component = ref[Object.keys(ref)[0]];
            component.validate();
            !component.isStateValid() && errors++;
            values[component.getName()] = component.getState();
        });

        if (errors > 0) {
            return null;
        }

        return values;
    };

    clearForm = () => {
        this.fields.forEach(ref => {
            let component = ref[Object.keys(ref)[0]];
            component.clear();
        });
    };

    pushRef = (ref) => ref[Object.keys(ref)[0]] && this.fields.push(ref);


}