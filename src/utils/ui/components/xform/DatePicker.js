import React from 'react'
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import {DatePicker as MaterialDatePicker} from 'material-ui-pickers';
import {generateAdornmentStyle, generateStyle, generateVerticalMargin} from "./helper";
import LabelOrNull from "./LabelOrNull";
import Wrapper from "./Wrapper";
import FormFieldComponent, {FieldType} from "./FormFieldComponent";

export default class DatePicker extends FormFieldComponent {
    constructor(props) {
        super(props, FieldType.DATE, props.validationRules);
        this.state = {value: '', error: false};
    }

    render() {
        let style = generateStyle(this.props);

        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <Wrapper asBlock={this.props.displayAsBlock} style={generateVerticalMargin(this.props)}>

                    <LabelOrNull label={this.props.label} spaceBetween={this.props.spaceBetween}
                                 inline={this.props.inline} width={this.props.labelWidth}/>

                    <MaterialDatePicker
                        style={style}
                        keyboard
                        format={this.props.format}
                        placeholder={this.props.placeholder}
                        mask={this.props.mask}
                        invalidDateMessage={this.props.invalidDateMessage}
                        adornmentPosition={this.props.adornmentPosition}
                        clearable={this.props.clearable}
                        disableFuture={this.props.disableFuture}
                        animateYearScrolling={this.props.animateYearScrolling}
                        onChange={this.onChange}
                        value={this.state.value}
                        error={this.state.error}
                        InputAdornmentProps={{style: generateAdornmentStyle(this.props.rightToLeft)}}
                    />
                </Wrapper>

            </MuiPickersUtilsProvider>
        );
    }


}

DatePicker.defaultProps = {
    width: 250,
    format: 'YYYY-MM-DD',
    placeholder: '',
    mask: value => (value ? [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/] : []),
    invalidDateMessage: null,
    adornmentPosition: 'end',
    clearable: true,
    disableFuture: false,
    animateYearScrolling: false,
    vMargin: 12,
    spaceBetween: 10,
    rightToLeft: true,
    labelWidth: 'auto',
    inline: true,
    displayAsBlock: false,
    validationRules: null
};