import React from 'react'
import {getDateStringFromDatePicker, getTimeStringFromTimePicker} from "../../../../Utils/DateUtils";
import validator from 'validate.js';

export default class FormFieldComponent extends React.Component {
    constructor(props, type, validationRules = undefined) {
        super(props);
        this.type = type;
        this.validationRules = validationRules;
    }

    onChange = e => {

        if (this.props.readOnly) {
            return;
        }

        let value = this.getValue(e);
        let error = !this.isValid(value);
        this.setState({value: value, error: error});
        this.props.onChange && this.props.onChange(e);
    };


    getValue = e => {
        switch (this.type) {
            case FieldType.TEXT :
                return e.target.value;
            case FieldType.DATE:
                return getDateStringFromDatePicker(e);
            case FieldType.TIME:
                return e;
            case FieldType.SELECT :
                return e.target.value;
            default :
                return e.target.value;
        }
    };

    isValid = value => {
        if (this.validationRules === null) {
            return true;
        }
        let error = validator.single(value, this.validationRules);
        return error === undefined;
    };

    validate = () => {
        let value = this.state.value;
        let error = !this.isValid(value);
        this.setState({error: error});
    };

    isStateValid = () => {
        let value = this.state.value;
        return this.isValid(value);
    };

    getState = () => {
        if (this.type === FieldType.TIME) {
            return getTimeStringFromTimePicker(this.state.value);
        }

        return this.state.value;
    };

    getName = () => {
        return this.props.name;
    };

    shouldError = () => {
        this.setState({error: true});
    };

    clear = () => {
        this.setState({value: ''});
    }

}


export const FieldType = {
    TEXT: 1,
    DATE: 2,
    TIME: 3,
    SELECT: 4,
};

validator.validators.timeFromTimePicker = function (value) {
    let time = getTimeStringFromTimePicker(value);
    let pattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
    return pattern.test(time) ? undefined : 'time is not valid';
};