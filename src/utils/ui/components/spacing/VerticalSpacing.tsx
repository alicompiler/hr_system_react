import * as React from "react";
import {Component} from "react";

interface Props {
    height: number
}

export default class VerticalSpacing extends Component<Props> {
    render() {
        return (
            <div style={{height: this.props.height, width: 1}}/>
        )
    }
}