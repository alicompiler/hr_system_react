import * as React from "react";
import {Color} from "../../../../../bootstrap/theme";
import {Button} from "@material-ui/core";

interface Props {
    required?: boolean;
    errorMessage?: string;
    validTypes?: string[];
    title: string;
    onChange: (file: any, event: any) => void;
    style?: any;
    disabled?: boolean;
}

interface State {
    file: any;
}

export default class FileInput extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {file: null};
    }

    static defaultProps: Props;
    private uploadFile: any;

    public clear = () => {
        this.setState({file: null});
    };

    render() {
        return (
            <div style={this.props.style}>
                <Button disabled={this.props.disabled} variant={'outlined'} onClick={() => this.uploadFile.click()}>
                    {this.props.title}
                    <input ref={ref => this.uploadFile = ref} type={'file'} hidden
                           onChange={this.onFileSelected}/>
                </Button>
                <span style={{color: this.state.file ? "#111" : Color.error, marginRight: 16}}>
                {
                    this.state.file ? this.state.file.name
                        : (this.props.required ? this.props.errorMessage : "")
                }
                </span>
            </div>
        )
    }

    onFileSelected = (e: any) => {
        e.preventDefault();
        let file = e.target.files[0];
        if (!file) {
            this.setState({file: null});
            this.props.onChange(null, e);
            return;
        }
        let type = file.type;
        let validTypes = this.props.validTypes!; // ["image/jpeg", "image/png"];
        if (validTypes.length > 0 && validTypes.indexOf(type) === -1) {
            this.setState({file: null});
            this.props.onChange(null, e);
            return
        }
        this.setState({file: file});
        this.props.onChange(file, e);
    };

}

FileInput.defaultProps = {
    required: false,
    errorMessage: "عليك ارفاق الملف",
    validTypes: [],
    title: "",
    disabled: false,
    onChange: () => undefined
};