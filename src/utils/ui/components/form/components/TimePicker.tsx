import * as React from "react";
import {MuiPickersUtilsProvider, TimePicker as MaterialTimePicker} from 'material-ui-pickers';
import {default as MaterialField, MaterialFieldProps} from "./MaterialField";
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import {getTimeAsString} from "../../../../helpers/DateHelper";

interface Props extends MaterialFieldProps {
    [propName: string]: any
}

export default class TimePicker extends MaterialField<Props> {

    constructor(props: Props) {
        super(props, props.value != null && props.value != undefined ? props.value : "", props.tag !== undefined ? props.tag : null);
        this.setState.bind(this);
    }

    static defaultProps: Props;

    renderField() {
        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <MaterialTimePicker
                    keyboard
                    clearable={this.props.clearable}
                    ampm={this.props.ampm}
                    invalidLabel={this.props.invalidLabel}
                    invalidDateMessage={this.props.invalidDateMessage}
                    adornmentPosition={this.props.adornmentPosition}
                    placeholder={this.props.placeholder}
                    margin={this.props.margin}
                    mask={this.props.mask}
                    onChange={this.onValueChange}
                    value={this.state.value}
                    error={this.state.error}
                />
            </MuiPickersUtilsProvider>
        )
    }

    protected extractValueFromOnChangeEvent(event: any): any {
        return event;
    };

    public getState() {
        return getTimeAsString(this.state.value);
    }

}


TimePicker.defaultProps = {
    format: 'YYYY-MM-DD',
    placeholder: '',
    ampm: true,
    mask: (value: any) => (value ? [/\d/, /\d/, ':', /\d/, /\d/] : []),
    invalidDateMessage: null,
    adornmentPosition: 'end',
    clearable: true,
    disableFuture: false,
    animateYearScrolling: false,
    validationRules: null,
    margin: "normal",
};