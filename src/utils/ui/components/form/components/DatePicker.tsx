import * as React from "react";
import {DatePicker as MaterialDatePicker, MuiPickersUtilsProvider} from 'material-ui-pickers';
import {default as MaterialField, MaterialFieldProps} from "./MaterialField";
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import {getDateAsString} from "../../../../helpers/DateHelper";

interface Props extends MaterialFieldProps {
    [propName: string]: any
}

export default class DatePicker extends MaterialField<Props> {

    constructor(props: Props) {
        super(props, props.value != null && props.value != undefined ? props.value : "", props.tag !== undefined ? props.tag : null);
    }

    static defaultProps: Props;

    renderField() {
        return (
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <MaterialDatePicker
                    keyboard
                    format={this.props.format}
                    placeholder={this.props.placeholder}
                    mask={this.props.mask}
                    margin={this.props.margin}
                    invalidDateMessage={this.props.invalidDateMessage}
                    adornmentPosition={this.props.adornmentPosition}
                    clearable={this.props.clearable}
                    disableFuture={this.props.disableFuture}
                    animateYearScrolling={this.props.animateYearScrolling}
                    onChange={this.onValueChange}
                    value={this.state.value}
                    error={this.state.error}
                />
            </MuiPickersUtilsProvider>
        )
    }

    protected extractValueFromOnChangeEvent(date: any): any {
        return getDateAsString(date);
    }


}


DatePicker.defaultProps = {
    format: 'YYYY-MM-DD',
    placeholder: '',
    mask: (value: any) => (value ? [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/] : []),
    invalidDateMessage: null,
    adornmentPosition: 'end',
    clearable: true,
    disableFuture: false,
    animateYearScrolling: false,
    validationRules: null,
    margin: "normal",
};