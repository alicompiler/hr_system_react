import * as React from "react";
import MaterialTextField from '@material-ui/core/TextField';
import MaterialField, {MaterialFieldProps} from "./MaterialField";
import {InputAdornment} from "@material-ui/core";


interface Props extends MaterialFieldProps {
    adornment?: any;
    adornmentPosition?: "start" | "end";
    validationRules?: object | null;
    adornmentClassName?: string;
    onEnter?: (text: string) => void
}

export default class TextField extends MaterialField<Props> {

    constructor(props: Props) {
        super(props, props.value != null && props.value != undefined ? props.value : "", props.tag !== undefined ? props.tag : null);
    }

    static defaultProps: Props;

    renderField(): React.ReactElement<Props> {
        const className = this.props.className ? this.props.className + " " + "material-text-field" : "material-text-field";
        return (
            <MaterialTextField
                variant={this.props.variant}
                className={className}
                onChange={this.onValueChange}
                value={this.state.value}
                error={this.state.error}
                fullWidth={this.props.fullWidth}
                placeholder={this.props.placeholder}
                margin={this.props.margin}
                defaultValue={this.props.defaultValue}
                disabled={this.props.disabled}
                helperText={this.props.helperText}
                InputLabelProps={this.props.InputLabelProps}
                InputProps={this.getInputProps()}
                inputProps={this.props.inputProps}
                label={this.props.label}
                multiline={this.props.multiline}
                name={this.props.name}
                required={this.props.required}
                rows={this.props.rows}
                rowsMax={this.props.rowsMax}
                type={this.props.type}
                onKeyPress={this.onKeyPress}
                style={this.props.style}
            />
        )
    }

    private onKeyPress = (event: any) => {
        if (event.key === "Enter" && this.state.value !== "" && this.props.onEnter) {
            this.props.onEnter(this.state.value);
        }
    };


    getInputProps = () => {
        let props: any = {};
        if (this.props.adornment) {
            let adornmentKey = this.props.adornmentPosition + 'Adornment';
            props[adornmentKey] = this.getAdornment();
        }
        props = {...props, ...this.props.InputProps};
        return props;
    };

    getAdornment = () => {
        let position: any = this.props.adornmentPosition;
        return (
            <InputAdornment className={this.props.adornmentClassName} position={position}>
                {this.props.adornment}
            </InputAdornment>
        )
    };
}

TextField.defaultProps = {
    xs: false,
    sm: false,
    md: false,
    lg: false,
    xl: false,
    isGridContainer: false,
    withoutGrid: false,
    gridStyle: {},
    readOnly: false,
    disabled: false,
    fullWidth: true,
    rows: 1,
    margin: "normal",
    multiline: false,
    adornmentPosition: "start"
};




