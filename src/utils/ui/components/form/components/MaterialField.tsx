import FormElement, {IFormElementProps} from "../FormElement";
import * as React from "react";
import Grid from "@material-ui/core/Grid/Grid";

export type GridItemSizeOptions = false | true | "auto" | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12

export interface MaterialFieldPropsOnly {
    isGridContainer?: boolean;
    withoutGrid?: boolean;
    lg?: GridItemSizeOptions;
    md?: GridItemSizeOptions;
    sm?: GridItemSizeOptions;
    xl?: GridItemSizeOptions;
    xs?: GridItemSizeOptions;
    gridStyle?: object;
    rootClassName?: string;
}

export interface MaterialFieldProps extends IFormElementProps, MaterialFieldPropsOnly {
}

export default abstract class MaterialField<T extends MaterialFieldProps> extends FormElement<T> {

    public render() {
        const field = this.renderField();
        if (this.props.withoutGrid)
            return field;
        return this.wrapInGrid(field);
    }

    public wrapInGrid = (field: React.ReactElement<MaterialFieldProps>): any => {
        if (this.props.isGridContainer)
            return (
                <Grid className={this.props.rootClassName} container={true} style={this.props.gridStyle}>
                    {
                        field
                    }
                </Grid>
            );

        return (
            <Grid className={this.props.rootClassName} style={this.props.gridStyle} item={true}
                  xs={this.props.xs} sm={this.props.sm} md={this.props.md} lg={this.props.lg}
                  xl={this.props.xl}>
                {
                    field
                }
            </Grid>
        )
    };

    public abstract renderField(): React.ReactElement<T>

}