import * as React from "react";
import {Color} from "../../../../../bootstrap/theme";
import {Button} from "@material-ui/core";

interface Props {
    required?: boolean;
    errorMessage?: string;
    validTypes?: string[];
    title: string;
    onChange: (e: FileList | null) => void;
    style?: any;
    fileText?: string;
}

interface State {
    files: FileList | null;
}

export default class FilesInput extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {files: null};
    }

    static defaultProps: Props;
    private uploadFile: any;

    public clear = () => {
        this.setState({files: null});
    };

    render() {
        return (
            <div style={this.props.style}>
                <Button variant={'outlined'} onClick={() => this.uploadFile.click()}>
                    {this.props.title}
                    <input ref={ref => this.uploadFile = ref} multiple type={'file'} hidden
                           onChange={this.onFilesSelected}/>
                </Button>
                <span style={{
                    color: this.state.files && this.state.files.length ? "#111" : Color.error,
                    marginRight: 16
                }}>
                {
                    this.state.files ? this.state.files.length + " " + this.props.fileText
                        : (this.props.required ? this.props.errorMessage : "")
                }
                </span>
            </div>
        )
    }

    onFilesSelected = (e: any) => {
        let files: FileList = e.target.files;
        if (!files) {
            this.setState({files: null});
            this.props.onChange(null);
            return;
        }

        if (this.props.validTypes!.length > 0 && this.validateFiles(files)) {
            this.setState({files: null});
            this.props.onChange(null);
            return
        }
        this.setState({files: files});
        this.props.onChange(files);
    };


    validateFiles = (files: FileList) => {
        for (let i = 0; i < files.length; i++) {
            const type = files[i].type;
            const validTypes = this.props.validTypes!;
            if (validTypes.length > 0 && validTypes.indexOf(type) === -1)
                return false;
        }
        return true;
    }

}

FilesInput.defaultProps = {
    required: false,
    errorMessage: "عليك ارفاق الملفات",
    validTypes: [],
    title: "",
    fileText: "ملفات",
    onChange: () => undefined
};