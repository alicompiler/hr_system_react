import * as React from "react";
import MaterialSelect, {SelectProps} from '@material-ui/core/Select';
import MaterialField, {MaterialFieldProps} from "./MaterialField";
import {InputAdornment, MenuItem} from "@material-ui/core";

interface Props extends SelectProps, MaterialFieldProps {
    adornment?: any;
    adornmentPosition?: "start" | "end";
    validationRules?: object | null;
    adornmentClassName?: string;
    options: ISelectOption[];
    optionExtractor?: (item: any) => { value: any; text: string };
    onSearch?: (text: string) => void
}

export interface ISelectOption {
    text: string;
    value: any;
}

export default class Select extends MaterialField<Props> {

    constructor(props: Props) {
        super(props, props.value != null && props.value != undefined ? props.value : "", props.tag !== undefined ? props.tag : null);
    }

    static defaultProps: Props;

    renderField(): React.ReactElement<Props> {
        return (
            <MaterialSelect
                className={this.props.className}
                onChange={this.onValueChange}
                value={this.state.value}
                error={this.state.error}
                fullWidth={this.props.fullWidth}
                placeholder={this.props.placeholder}
                margin={this.props.margin}
                multiple={this.props.multiple}
                defaultValue={this.props.defaultValue}
                disabled={this.props.disabled}
                startAdornment={this.props.adornmentPosition === "start" ? this.getAdornment() : undefined}
                endAdornment={this.props.adornmentPosition === "end" ? this.getAdornment() : undefined}
                inputProps={this.props.inputProps}
                name={this.props.name}
                displayEmpty={this.props.displayEmpty}
                required={this.props.required}
                style={this.props.style}>
                {
                    this.props.placeholder && <MenuItem value="" disabled>{this.props.placeholder}</MenuItem>
                }
                {this.renderOptions()}
            </MaterialSelect>
        )
    }

    getAdornment = () => {
        let position: any = this.props.adornmentPosition;
        return (
            <InputAdornment className={this.props.adornmentClassName} position={position}>
                {this.props.adornment}
            </InputAdornment>
        )
    };

    renderOptions = () => {
        return this.props.options.map((option: any, index: number) => {
            const item = this.props.optionExtractor ? this.props.optionExtractor(option) : option;
            return <MenuItem key={index} value={item.value}>
                {item.text}
            </MenuItem>
        });
    };

}

Select.defaultProps = {
    xs: false,
    sm: false,
    md: false,
    lg: false,
    xl: false,
    isGridContainer: false,
    withoutGrid: false,
    gridStyle: {},
    readOnly: false,
    disabled: false,
    fullWidth: true,
    multiple: false,
    adornmentPosition: undefined,
    displayEmpty: true,
    options: [],
    defaultValue: undefined
};