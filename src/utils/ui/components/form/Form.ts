import * as React from "react";
import FormElement from "./FormElement";

export interface IForm {
    fields: FormElement[];

    hasErrors(): boolean;

    validate(): boolean;

    getValues(): object | null;

    clearValues(): void;

    pushElementRef(field: FormElement): void
}

export default class Form<Props, State = any> extends React.Component<Props, State> implements IForm {
    public fields: FormElement[] = [];

    public clearValues = (): void => {
        for (let i = 0; i < this.fields.length; i++) {
            let field: FormElement = this.fields[i];
            field.clear();
        }
    };

    public getValues = (): object => {
        if (this.hasErrors())
            return {};
        let values: any = {};
        for (let i = 0; i < this.fields.length; i++) {
            let field: FormElement = this.fields[i];
            values[field.props.name] = field.getState();
        }

        return values;
    };

    public getFieldByName(name: string): FormElement | null {
        for (let i = 0; i < this.fields.length; i++) {
            let field: FormElement = this.fields[i];
            if (field.props.name === name)
                return field;
        }
        return null;
    }

    public getTags = (): object => {
        let tags: any = {};
        for (let i = 0; i < this.fields.length; i++) {
            let field: FormElement = this.fields[i];
            tags[field.props.name] = field.getTag();
        }

        return tags;
    };

    public setValues = (values: any) => {
        const keys = Object.keys(values);
        keys.forEach(key => {
            for (let i = 0; i < this.fields.length; i++) {
                const field: FormElement = this.fields[i];
                if (field.getName() == key) {
                    const value = values[key];
                    if (value != null || value != undefined)
                        field.changeValue(value);
                    break;
                }
            }
        })
    };

    public hasErrors = (): boolean => {
        for (let i = 0; i < this.fields.length; i++)
            if (!this.fields[i].isValid())
                return true;
        return false;
    };

    public pushElementRef = (field: FormElement | null): void => {
        if (field)
            this.fields.push(field);
    };

    public validate = (): boolean => {
        let valid: boolean = true;
        for (let i = 0; i < this.fields.length; i++) {
            this.fields[i].validate();
            if (!this.fields[i].isValid()) {
                valid = false;
            }
        }
        return valid;
    };

}