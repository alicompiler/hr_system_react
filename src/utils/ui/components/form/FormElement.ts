import * as React from "react";
import * as validator from 'validate.js';

export interface IFormElement {
    onValueChange(e: any): void;

    changeValue(value: any): void

    getState(): any;

    clear(): void;

    error(): void;

    validate(): void;

    isValid(value: any): boolean
}

export interface IFormElementProps {
    afterChange?: (e: any) => void;
    validationRules?: any;

    [propName: string]: any;
}

export interface IFormElementState {
    error: boolean;
    value: any;
}

export default class FormElement<T extends IFormElementProps = any>
    extends React.Component<T, IFormElementState>
    implements IFormElement {

    private readonly tag: any;

    constructor(props: T, value: any = "", tag: any = null) {
        super(props);
        this.state = {value: value, error: false};
        this.getState.bind(this);
        this.tag = tag;
    }

    public clear = (): void => {
        this.setState({value: ""})
    };

    public error = (error: boolean = true): void => {
        this.setState({error: error})
    };

    public getState(): any {
        return this.state.value;
    };

    public getName = (): any => {
        return this.props.name;
    };

    public onValueChange = (e: any): void => {

        if (this.props.readOnly) return;

        if (this.props.onChange) {
            this.props.onChange(e);
            return;
        }

        let value = this.extractValueFromOnChangeEvent(e);
        let error = !this.isValid(value);
        this.error(error);
        this.setState({value: value});

        if (this.props.afterChange) {
            this.props.afterChange(e);
        }
    };

    public changeValue = (value: any): void => {
        this.setState({value: value})
    };

    public validate = (): void => {
        let value = this.state.value;
        let error = !this.isValid(value);
        this.setState({error: error});
    };

    public isValid = (value: any = null): boolean => {
        if (value === null) {
            value = this.state.value;
        }
        if (!this.props.validationRules) return true;
        let error = validator.single(value, this.props.validationRules);
        return error === undefined;
    };

    protected extractValueFromOnChangeEvent(e: any): any {
        if (e)
            return e.target.value;
        return "";
    };

    public getTag = () => {
        return this.tag;
    }

}

