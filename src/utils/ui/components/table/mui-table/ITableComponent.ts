export default interface ITableComponent {
    getTitle(): string;

    getColumns(): any[];

    getOptions(): object;

    getData(): any[][];
}