import ITableComponent from "./ITableComponent";
import MUIDataTable from "mui-datatables";
import * as React from "react";

export default abstract class MUITableComponent<Props = any, State = any>
    extends React.Component<Props, State>
    implements ITableComponent {


    public renderTable = () => {
        return <MUIDataTable
            title={this.getTitle()}
            data={this.getData()}
            columns={this.getColumns()}
            options={this.getOptions()}
        />
    };

    render() {
        return <div className={`table ${this.isPointerCursor() ? "selectable-row-table" : ""}`}>
            {this.renderTable()}
        </div>
    }

    abstract getColumns(): any[];

    abstract getData(): any[][];

    abstract getTitle(): string;

    protected isPointerCursor() {
        return true
    };


    getOptions(): object {
        return {
            filterType: false,
            textLabels: textLabels,
            selectableRows: false,
            pagination: false,
            rowHover: false,
            resizableColumns: true,
            filter: false,
            rowsPerPage: 500,
            search: false,
        };
    }

}

const textLabels: object = {
    body: {
        noMatch: "لاتوجد نتائج",
        toolTip: "ترتيب",
    },
    pagination: {
        next: "التالي",
        previous: "السابق",
        rowsPerPage: "صفوف كل صفحة : ",
        displayRows: "of",
    },
    toolbar: {
        search: "بحث...",
        downloadCsv: "Download CSV",
        print: "طباعة",
        viewColumns: "الاعمدة",
        filterTable: "Filter Table",
    },
    filter: {
        all: "الكل",
        title: "FILTERS",
        reset: "RESET",
    },
    viewColumns: {
        title: "اظهار الاعمدة",
        titleAria: "اظهار/اخفاء الاعمدة",
    },
    selectedRows: {
        text: "صفوف مختارة",
        delete: "حذف",
        deleteAria: "حذف الصفوف المختارة",
    },
};