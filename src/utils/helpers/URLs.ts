import {APP_CONFIG} from "../../bootstrap/config";

export default class URLs {
    static getApiUrl(route: string) {
        return APP_CONFIG.SERVER.BASE_URL + "api/" + route;
    }

    static getImage(prefix: string, image: any) {
        return APP_CONFIG.SERVER.BASE_URL + "storage/" + prefix + (prefix ? "/" : "") + image;
    }
}


export const API_ROUTES = {
    LEAVE_REQUEST: {
        SEND: "leave-requests/send",
        delete: "leave-request/delete",
        MANAGER_REPORT: "leave-requests/manager-report",
        BALANCE_REPORT: "leave-requests/balance-report",
        ATTACHMENT: {
            with: (file: string, session: string) => "leave-requests/download-attachment/" + file + "/" + session
        },
    },
    ANNOUNCEMENTS: {
        UNREVIEWED: "announcements/unreviewed",
        RECENT: "announcements",
        SAVED_ANNOUNCEMENTS: "announcements/saved",
        withDate: (date: string) => `announcements/${date}`,
        UNWATCHED: "announcements/unwatched",
        WATCH: "announcements/watch",
        WHO_WATCH: {
            withAnnouncementId: (id: number) => `announcements/who-watch/${id}`
        },
        NEW: "announcements/new",
        DOWNLOAD: {
            with: (fileId: number, session: string) => `announcements/download/${fileId}/${session}`
        },
        RECENT_FOR_HOME_PAGE: "announcements/recent-after-last-week",
        REVIEW: "announcements/review",
        DELETE: "announcements/delete",
        TOGGLE_SAVE: "announcements/toggle-save",
    },

    EMPLOYEES: {
        EMPLOYEE_LIST: "employees/simple-list",
        ALL: "employees",
        SINGLE: {
            with: (id: number) => `employees/${id}`
        },
        EDIT_CERDENTIALS: "employees/change-login-credentials",
        CREATE: "employees/create",
        FIND: "employees/find",
        DELETE: "employees/delete",
        EDIT: "employees/edit",
    },

    DEPARTMENTS: {
        DEPARTMENT_LIST: "departments"

    },
    DAY_OFF: {
        DELETE: "day-off/delete",
        DAYS_OFF: "day-off",
        CREATE: "day-off/create"
    },
    SHIFT_OFF: {
        DELETE: "shift-off/delete",
        SHIFT_OFF: "shift-off",
        CREATE: "shift-off/create"
    },
    EXCEPTION_SHIFT_OFF: {
        DELETE: "exception-shift-off/delete",
        EXCEPTION_SHIFT_OFF: "exception-shift-off",
        CREATE: "exception-shift-off/create"
    },
    AUDIENCE_TYPE: {
        ALL: "audience-types"
    },
    AUDIENCE: {
        single: (id: number) => `audience/${id}`,
        EDIT: "audience/edit",
        MY_AUDIENCE: "audience/yesterday",
        SEND_IMPORT_DATA: "audience/import",
        AUDIENCE_BY_DATE: {
            with: (date: string) => `audience/report/by-date/${date}`
        },
        MONTHLY: {
            with: (start: any, end: any) => `audience/report/monthly/${start}/${end}`
        },
        MONTHLY_EMPLOYEE: {
            with: (start: any, end: any) => `audience/report/monthly-employee/${start}/${end}`
        },
        YESTERDAY_REPORT: "audience/report/yesterday"
    },
    SALARY: {
        ADD_ON: {
            CREATE: "salary/add-on/create",
            ALL: "salary/add-on/all",
            DELETE: "salary/add-on/delete",
        },
        MY_ADDONS: "salary/add-on/my",
        MY_ADDONS_BETWEEN_DATE: {
            with: (from: any, to: any) => `salary/add-on/${from}/${to}`
        },
        ADDONS_FOR_EMPLOYEE: {
            with: (employeeId: any, from: any, to: any) => `salary/add-on/${employeeId}/${from}/${to}`
        },
        SALARY_REPORT: {
            with: (from: string, to: string) => `salary/report/${from}/${to}`
        }
    },
    PROFILE: {
        MY_INFO: "profile/my-info",
        ATTACHMENT: {
            EMPLOYEE_ATTACHMENTS: "profile/attachments",
            ALL: "profile/my-files",
            DELETE: "profile/delete-file",
            UPLOAD: "profile/upload-file",
            DOWNLOAD: {
                with: (fileId: any, session: any) => `profile/file/download/${fileId}/${session}`
            }
        },
        CHANGE_PROFILE_IMAGE: "profile/change-profile-image",
    },
    LOAN: {
        DOWNLOAD_FILE: {with: (file: string, session: string) => `loan/download/${file}/${session}`},
        SEND: "loan/send",
        SINGLE_LOAN_REQUEST: {
            with: (id: number) => `loan/${id}`
        },
        SPONSORING_REQUESTS: "loan/sponsoring-requests",
        SEND_SPONSORING_ACCEPTANCE_STATUS: "loan/set-sponsoring-status",
        MY_LOAN_REQUESTS: "loan/my-loan-requests",
        WAITING_LOAN_REQUEST: "loan/waiting-loan-requests",
        MANAGE: "loan/manage",
        COMPLETED_LOANS: "loan/completed",
        ALL: "loan/all"
    },
    SUGGESTION_AND_ISSUE: {
        send: "suggestion-issue/send",
        recent: "suggestion-issue",
        DELETE: "suggestion-issue/delete"
    },
    POLL: {
        VOTE: "polls/vote",
        ACTIVE_POLLS: "polls/active",
        CREATE: "polls/create",
        FINISHED_POLLS: "polls/finished",
        MY_POLLS: "polls/mine",
        DELETE: "polls/delete"
    },
    PROJECT_IDEA: {
        CREATE: "project-idea/create",
        REPLAY: "project-idea/replay",
        ALL: "project-idea",
        MINE: "project-idea/mine",
        DELETE: "project-idea/delete",
        DOWNLOAD: {
            with: (id: any, session: string) => `project-idea/download/${id}/${session}`
        },
    },
    SALARY_RISE: {
        CREATE: "salary-rise/create",
        DELETE: "salary-rise/delete",
        ALL: "salary-rise/all"
    },
    APPRECIATION_LETTER: {
        CREATE: "appreciation-letter/create",
        DELETE: "appreciation-letter/delete",
        ALL: "appreciation-letter/all",
        DOWNLOAD: {
            with: (id: any, session: string) => `appreciation-letter/download/${id}/${session}`
        },
    },
    EMPLOYEE_RATING: {
        CREATE: "employee-rating/create",
        DELETE: "employee-rating/delete",
        ALL: "employee-rating/all"
    },

    NOTIFICATION: {
        SET_VISIBLE: "notifications/set-visible",
        ALL: "notifications"
    }

};