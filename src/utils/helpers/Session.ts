import CookieManager from "./CookieManager";

interface ISession {
    session: any;
    userId: any;
    employeeId: any;
    name: any;
    departmentId: any;
    isManager: any;
    userType: any;
}

export default class Session {

    static SESSION_KEY: string = "SESSION";
    static USER_ID_KEY: string = "USER_ID";
    static EMPLOYEE_ID_KEY: string = "EMPLOYEE_ID";
    static EMPLOYEE_NAME_KEY: string = "EMPLOYEE_NAME";
    static IS_MANAGER_KEY: string = "IS_MANAGER";
    static DEPARTMENT_ID_KEY = "DEPARTMENT_ID";
    static USER_TYPE_KEY = "USER_TYPE_KEY";

    static isLoggedIn() {
        let session = CookieManager.getCookie(this.SESSION_KEY);
        return !!session;
    }

    static logout() {
        CookieManager.eraseCookie(this.SESSION_KEY);
        CookieManager.eraseCookie(this.USER_ID_KEY);
        CookieManager.eraseCookie(this.EMPLOYEE_ID_KEY);
        CookieManager.eraseCookie(this.EMPLOYEE_NAME_KEY);
        CookieManager.eraseCookie(this.USER_TYPE_KEY);
    }

    static login(session: any, userId: any, employeeId: any, name: any, departmentId: any, isManager: any, userType: any) {
        CookieManager.setCookie(this.SESSION_KEY, session, 60);
        CookieManager.setCookie(this.USER_ID_KEY, userId, 60);
        CookieManager.setCookie(this.EMPLOYEE_ID_KEY, employeeId, 60);
        CookieManager.setCookie(this.EMPLOYEE_NAME_KEY, name, 60);
        CookieManager.setCookie(this.DEPARTMENT_ID_KEY, departmentId, 60);
        CookieManager.setCookie(this.IS_MANAGER_KEY, isManager, 60);
        CookieManager.setCookie(this.USER_TYPE_KEY, userType, 60);
    }

    static getUserObject(): ISession {
        return {
            session: CookieManager.getCookie(this.SESSION_KEY),
            userId: CookieManager.getCookie(this.USER_ID_KEY),
            employeeId: CookieManager.getCookie(this.EMPLOYEE_ID_KEY),
            name: CookieManager.getCookie(this.EMPLOYEE_NAME_KEY),
            departmentId: CookieManager.getCookie(this.DEPARTMENT_ID_KEY),
            isManager: CookieManager.getCookie(this.IS_MANAGER_KEY),
            userType: CookieManager.getCookie(this.USER_TYPE_KEY),
        }
    }
}