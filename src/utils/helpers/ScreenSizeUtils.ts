export const getScreenSizeType = (): "mobile" | "tablet" | "small-desktop" | "large-desktop" => {
    let width = window.innerWidth;
    if (width < 680)
        return "mobile";
    else if (width < 1024)
        return "tablet";
    else if (width < 1200)
        return "small-desktop";
    else
        return "large-desktop";
};