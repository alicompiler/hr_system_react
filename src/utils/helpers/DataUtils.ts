export function objectToFormData(values: object) {
    const keys = Object.keys(values);
    let formData = new FormData();
    for (let i = 0; i < keys.length; i++) {
        formData.append(keys[i], values[keys[i]])
    }
    return formData;
}