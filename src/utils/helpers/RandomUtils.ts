export default class RandomUtils {
    static randomCode(length = 4) {
        let code = '';
        for (let i = 0; i < length; i++) {
            let n = Math.floor(Math.random() * 10);
            code = code + String(n);
        }
        return code;
    }
}