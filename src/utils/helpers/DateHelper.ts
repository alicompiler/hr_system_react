export function getTimeAsString(datetime: any) {
    if (!datetime) {
        return "";
    }

    let d = new Date(datetime);
    let minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
    let hours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
    return hours + ":" + minutes;
}

export function getDateAsString(datetime: any) {
    if (!datetime) {
        return "";
    }

    let d = new Date(datetime);
    let day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
    let month: any = d.getMonth() + 1;
    month = month < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1);
    let year = d.getFullYear();
    return year + "-" + month + "-" + day;
}

export function timestampToDateString(timestamp: any) {
    let a = new Date(timestamp * 1000);
    let year = a.getFullYear();
    let month = a.getMonth() + 1;
    let date = a.getDate();
    return year + "-" + (month > 9 ? month : ("0" + month)) + "-" + (date > 9 ? date : ("0" + date));
}

