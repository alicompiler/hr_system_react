export interface DateHumanizerLocal {
    seconds: string;
    minutes: string;
    hours: string;
    days: string;
    months: string;
    week: string;
    years: string;
    since: string;
}

export const ARABIC_LOCAL: DateHumanizerLocal = {
    seconds: "ثانية",
    minutes: "دقيقة",
    hours: "ساعة",
    days: "يوم",
    week: "اسبوع",
    months: "شهر",
    years: "سنة",
    since: "منذ"
};

export const ENGLISH_LOCAL: DateHumanizerLocal = {
    seconds: "second",
    minutes: "minute",
    hours: "hour",
    days: "day",
    week: "week",
    months: "month",
    years: "year",
    since: "since"
};

export default class DateHumanizer {

    private local: DateHumanizerLocal;

    constructor(local?: DateHumanizerLocal) {
        if (!local) {
            this.local = ENGLISH_LOCAL;
        } else {
            this.local = local;
        }
    }


    public humanize(timeInSeconds: number): string {
        const currentTimeInSeconds = new Date().getTime() / 1000;
        const diff = currentTimeInSeconds - timeInSeconds;
        if (diff > 60 * 60 * 24 * 7) {
            const weeks = Math.floor(diff / (60 * 60 * 24 * 7));
            return this.text(weeks, this.local.week);
        } else if (diff > 60 * 60 * 24) {
            const days = Math.floor(diff / (60 * 60 * 24));
            return this.text(days, this.local.days);
        } else if (diff > 60 * 60) {
            const hours = Math.floor(diff / (60 * 60));
            return this.text(hours, this.local.hours);
        } else if (diff > 60) {
            const minutes = Math.floor(diff / 60);
            return this.text(minutes, this.local.minutes);
        } else {
            return this.text(Math.floor(diff), this.local.seconds);
        }
    }


    private text(value: number, timeType: string) {
        return this.local.since + " " + value + " " + timeType;
    }
}
