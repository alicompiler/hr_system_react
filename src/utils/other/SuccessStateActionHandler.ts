import SnackbarAction from "../../bootstrap/main_components/snackbar/SnackbarAction";

export default class SuccessStateActionHandler {

    private props: any;
    private _afterError: () => void;
    private _afterNotSuccess: () => void;
    private _afterSuccess: () => void;
    private _afterAny: () => void;
    private _successMessage: string = "تمت العملية بنجاح";
    private _notSuccessMessage: string = "هناك مشكلة ، لم تكتمل العملية";
    private _errorMessage: string = "هناك مشكلة ، تحقق من الاتصال بالانترنت";

    constructor(props: any
        , afterError: () => void = () => undefined
        , afterNotSuccess: () => void = () => undefined
        , afterSuccess: () => void = () => undefined
        , afterAny: () => void = () => undefined) {

        this.props = props;
        this._afterError = afterError;
        this._afterNotSuccess = afterNotSuccess;
        this._afterSuccess = afterSuccess;
        this._afterAny = afterAny;
    }

    static handle(props: any
        , afterError: any = () => undefined
        , afterNotSuccess: () => void = () => undefined
        , afterSuccess: () => void = () => undefined) {
        const handler = new SuccessStateActionHandler(props, afterError, afterNotSuccess, afterSuccess);
        handler.handleSuccessResponse();
    }

    public handleSuccessResponse() {
        let any: boolean = false;
        if (this.props.error) {
            any = this.onError(any);
        }
        else if (this.props.success === false) {
            any = this.onNotSuccess(any);
        }
        else if (this.props.success) {
            any = this.onSuccess(any);
        }

        if (any) {
            this._afterAny();
        }
    }


    private onSuccess(any: boolean) {
        this.props.dispatch(SnackbarAction(this._successMessage, true, "success"));
        this._afterSuccess();
        any = true;
        return any;
    }

    private onNotSuccess(any: boolean) {
        this.props.dispatch(SnackbarAction(this._notSuccessMessage, true, "error"));
        this._afterNotSuccess();
        any = true;
        return any;
    }

    private onError(any: boolean) {
        this.props.dispatch(SnackbarAction(this._errorMessage, true, "error"));
        this._afterError();
        any = true;
        return any;
    }

    set afterError(value: () => void) {
        this._afterError = value;
    }

    set afterNotSuccess(value: () => void) {
        this._afterNotSuccess = value;
    }

    set afterSuccess(value: () => void) {
        this._afterSuccess = value;
    }

    set afterAny(value: () => void) {
        this._afterAny = value;
    }

    set successMessage(value: string) {
        this._successMessage = value;
    }

    set notSuccessMessage(value: string) {
        this._notSuccessMessage = value;
    }

    set errorMessage(value: string) {
        this._errorMessage = value;
    }
}