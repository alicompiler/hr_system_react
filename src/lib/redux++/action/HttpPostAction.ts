import NetworkingAction from "./NetworkingAction"
import {HttpMethod} from "./HttpMethod"

const HttpPostAction = function (type: string, url: string, data: any = {}, options: any = {}) {
    return NetworkingAction(type, url, HttpMethod.POST, data, options)
}


export default HttpPostAction