export interface IAction {
    type: string,
    payload: any
}

export default function ReduxAction(type: string, payload: any = {}): IAction {
    return {type: type, payload: payload};
}