import {HttpMethod} from "./HttpMethod"
import NetworkingAction from "./NetworkingAction"


const HttpGetAction = (type: string, url: string, params: object = {}, options: any = {}) => {
    options.params = params;
    return NetworkingAction(type, url, HttpMethod.GET, {}, options);
};


export default HttpGetAction