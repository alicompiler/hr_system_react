import FetchArrayReducer from "../reducer/FetchArrayReducer";

export function RemoveItemAtIndexFetchArrayAction(type: string, index: number) {
    const actionType = type + "_" + FetchArrayReducer.REMOVE_ITEM_AT_INDEX_CASE;
    return {type: actionType, payload: index};
}

export function AddItemAtStartFetchArrayAction(type: string, item: any) {
    const actionType = type + "_" + FetchArrayReducer.ADD_ITEM_AT_START_CASE;
    return {type: actionType, payload: item};
}

