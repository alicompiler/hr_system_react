import {IAction} from "./IAction";
import HttpReducer from "../reducer/HttpReducer";

export default function HttpResetAction(type: string): IAction {
    return {
        type: type + "_" + HttpReducer.RESET,
        payload: null
    }
}