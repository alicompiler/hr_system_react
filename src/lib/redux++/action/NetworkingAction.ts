import Axios, {AxiosPromise} from "axios"
import {HttpMethod} from "./HttpMethod"
import {IAction} from "./IAction"

export default function NetworkingAction(type: string, url: string, method: HttpMethod, data: object = {}, options: object = {}): IAction {
    return {
        type: type,
        payload: getHttpRequest(url, method, data, options)
    }
}

export function getHttpRequest(url: string, method: HttpMethod, data: any = {}, options: any = {}): AxiosPromise | undefined {
    if (method === HttpMethod.GET) {
        return Axios.get(url, options)
    } else if (method === HttpMethod.POST) {
        return Axios.post(url, data, options)
    }
    return Axios.get(url, options);
}