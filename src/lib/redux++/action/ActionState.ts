import PreActionResult from "../reducer/PreActionResult"

export default class ActionState {
    public type: string;
    public readonly beforeAction: (payload: any, returnedResult: any) => PreActionResult;
    private readonly newState: any;
    private readonly considerPayload: boolean = false;
    private readonly extractStateFromPayloadFunc: any;

    constructor(type: string, newState: any,
                considerPayload = false,
                extractStateFromPayloadFunc: (payload: any) => object = (payload: any) => payload,
                beforeAction: any = null) {
        this.type = type;
        this.newState = newState;
        this.extractStateFromPayloadFunc = extractStateFromPayloadFunc;
        this.considerPayload = considerPayload;
        this.beforeAction = beforeAction;
    }

    public getNewState(payload: any = null): any {
        if (this.considerPayload) {
            const state = this.extractStateFromPayloadFunc(payload);
            return {...this.newState, ...state}
        }
        return this.newState
    }

}