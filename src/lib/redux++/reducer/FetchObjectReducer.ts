import HttpReducer from "./HttpReducer"
import FetchObjectReducerState, {DEFAULT_FETCH_OBJECT_REDUCER_STATE} from "./state/FetchObjectReducerState"

export default class FetchObjectReducer extends HttpReducer<FetchObjectReducerState> {

    constructor(type: string, keepStateOnLoad: boolean = false, keepStateOnError: boolean = false) {
        super(DEFAULT_FETCH_OBJECT_REDUCER_STATE, type, keepStateOnLoad, keepStateOnError);
    }

    protected getExtractStateFromPayload(): (payload: any) => any {
        return payload => ({object: this.getExtraState(payload.data)})
    }

    protected getResetExtraState(): object {
        return {object: {}}
    }
}