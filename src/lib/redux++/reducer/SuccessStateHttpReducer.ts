import FetchObjectReducer from "./FetchObjectReducer"

export default class SuccessStateHttpReducer extends FetchObjectReducer {

    constructor(type: string) {
        super(type)
    }

    protected getResetExtraState(): object {
        return {success: null}
    }

    protected getExtractStateFromPayload(): (payload: any) => object {
        return payload => ({success: payload.data.success})
    }

}