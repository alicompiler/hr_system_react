import HttpReducer from "./HttpReducer"
import FetchArrayReducerState, {DEFAULT_FETCH_ARRAY_REDUCER_STATE} from "./state/FetchArrayReducerState"
import ActionState from "../action/ActionState";

export default class FetchArrayReducer extends HttpReducer<FetchArrayReducerState> {

    static readonly REMOVE_ITEM_AT_INDEX_CASE = "REMOVE_ITEM_AT_INDEX_CASE";
    static readonly ADD_ITEM_AT_START_CASE = "ADD_ITEM_AT_START_CASE";

    constructor(type: string, keepStateOnLoad: boolean = false, keepStateOnError: boolean = false) {
        super(DEFAULT_FETCH_ARRAY_REDUCER_STATE, type, keepStateOnLoad, keepStateOnError);
        this.addRemoveItemAtIndexCase();
        this.addItemAtStartCase();
    }

    private addRemoveItemAtIndexCase() {
        const type = this.type + "_" + FetchArrayReducer.REMOVE_ITEM_AT_INDEX_CASE;
        this.actionStateArr.push(new ActionState(type, {}, true, this.removeItemAtIndex));
    }

    protected removeItemAtIndex = (index: any): any => {
        const array = this.getCurrentState().array;
        array.splice(index, 1);
        return {array: [...array]};
    };

    private addItemAtStartCase() {
        const type = this.type + "_" + FetchArrayReducer.ADD_ITEM_AT_START_CASE;
        this.actionStateArr.push(new ActionState(type, {}, true, this.addItemAtStart));
    }

    protected addItemAtStart = (item: any): any => {
        const array = this.getCurrentState().array;
        array.unshift(item);
        return {array: [...array]};
    };

    protected getResetExtraState(): object {
        return {array: []};
    }

    protected getExtractStateFromPayload(): (payload: any) => any {
        return payload => ({array: this.getExtraState(payload.data)})
    }
}