export default interface NetworkingState {
    loading: boolean;
    error: boolean | null;
}


export const DEFAULT_NETWORKING_STATE: NetworkingState = {
    error: null,
    loading: false
};