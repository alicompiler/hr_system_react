import NetworkingState from "./NetworkingState"

export default interface FetchArrayReducerState extends NetworkingState {
    array: any[];
}

export const DEFAULT_FETCH_ARRAY_REDUCER_STATE: FetchArrayReducerState = {
    error: null,
    loading: false,
    array: []
};