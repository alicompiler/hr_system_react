import NetworkingState from "./NetworkingState"

export default interface FetchObjectReducerState extends NetworkingState {
    object: object;
}

export const DEFAULT_FETCH_OBJECT_REDUCER_STATE: FetchObjectReducerState = {
    error: null,
    loading: false,
    object: {}
};