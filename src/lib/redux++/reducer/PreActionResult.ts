export default interface PreActionResult {
    stop: boolean
    returnedValue: any
}