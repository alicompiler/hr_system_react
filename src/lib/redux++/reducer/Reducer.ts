import IReducer from "./IReducer"
import ActionState from "../action/ActionState"
import {IAction} from "../action/IAction"

export default class Reducer implements IReducer {

    public initialState: object;
    protected actionStateArr: ActionState[] = [];
    protected defaultAction: ActionState | null;
    private currentState: any;

    constructor(initialState: any, actionStateArr: ActionState[] = [], defaultAction: ActionState | null = null) {
        this.initialState = initialState;
        this.actionStateArr = actionStateArr;
        this.defaultAction = defaultAction;
    }

    public reduce(state: object = this.initialState, action: IAction) {
        this.currentState = state;
        for (let i = 0; i < this.actionStateArr.length; i++) {
            let actionState = this.actionStateArr[i];
            if (actionState.type === action.type) {
                if (actionState.beforeAction) {
                    let result = actionState.beforeAction(action.payload, this.initialState);
                    if (result.stop) {
                        return result.returnedValue
                    }
                }
                return this.newState(state, actionState.getNewState(action.payload))
            }
        }

        if (this.defaultAction)
            return this.newState(state, this.defaultAction.getNewState());
        return this.reduceMore(state, action);
    }

    private newState = (prevState: object, overrideState: object): object => {
        return {...prevState, ...overrideState}
    };

    protected getCurrentState = (): any => {
        return this.currentState;
    };

    // noinspection JSUnusedLocalSymbols
    protected reduceMore(state: object, action: IAction) {
        return {...state}
    }

}