import {IAction} from "../action/IAction"


export default interface IReducer {
    initialState: object,

    reduce(state: object, action: IAction): object
}

export function reducerWrapper(reducer: IReducer) {
    return (state: any = reducer.initialState, action: any) => reducer.reduce(state, action)
}