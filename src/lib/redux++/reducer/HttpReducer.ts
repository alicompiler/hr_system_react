import Reducer from "./Reducer"
import NetworkingState from "./state/NetworkingState"
import ActionState from "../action/ActionState"

export default class HttpReducer<T extends NetworkingState> extends Reducer {

    static readonly PENDING: string = "PENDING";
    static readonly FULFILLED: string = "FULFILLED";
    static readonly REJECTED: string = "REJECTED";
    static readonly RESET: string = "RESET";

    protected type: string;
    protected keepStateOnLoad: boolean;
    protected keepStateOnError: boolean;

    constructor(initialState: object, type: string, keepStateOnLoad: boolean = false, keepStateOnError: boolean = false) {
        super(initialState, []);
        this.type = type;
        this.keepStateOnLoad = keepStateOnLoad;
        this.keepStateOnError = keepStateOnError;

        this.addPendingCase();
        this.addFulfilledCase();
        this.addRejectedCase();
        this.addResetCase();
    }

    protected addPendingCase() {
        let actionType = this.type + "_" + HttpReducer.PENDING;
        this.actionStateArr.push(new ActionState(actionType, this.getPendingState()))
    }

    protected addFulfilledCase() {
        let actionType = this.type + "_" + HttpReducer.FULFILLED;
        let extractStateFromPayload = this.getExtractStateFromPayload();
        this.actionStateArr.push(new ActionState(actionType, this.getFulfilledState(), true, extractStateFromPayload));
    }

    protected addRejectedCase() {
        let actionType = this.type + "_" + HttpReducer.REJECTED;
        this.actionStateArr.push(new ActionState(actionType, this.getRejectedState()))
    }

    protected addResetCase() {
        const actionType = this.type + "_" + HttpReducer.RESET;
        const resetState = {...this.initialState, ...this.getResetExtraState()};
        this.actionStateArr.push(new ActionState(actionType, resetState));
    }

    protected getResetExtraState(): object {
        return {};
    }

    protected getPendingState(): object {
        if (this.keepStateOnLoad)
            return {loading: true, error: null};
        return {loading: true, error: null, ...this.getExtraState()}
    }

    protected getRejectedState(): object {
        if (this.keepStateOnError)
            return {loading: false, error: true};
        return {loading: false, error: true, ...this.getExtraState()}
    }

    protected getFulfilledState(): object {
        return {loading: false, error: null, ...this.getExtraState()}
    }

    protected getExtractStateFromPayload(): (payload: any) => object {
        return (payload) => payload.data
    }

    protected getExtraState(value: any = {}): object {
        return value
    }
}