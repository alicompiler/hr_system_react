import * as React from "react";
import {Component} from "react";
import {Gravity, toStyle as GS} from "./Gravity";
import {Orientation, toSytle as OS} from "./Orientation";

interface Props {
    width?: any;
    height?: any;
    padding?: number,
    margin?: number,
    orientation: Orientation;
    gravity: Gravity;
    layoutGravity: Gravity;
    style: object
}

export default class StackView extends Component<Props> {
    static defaultProps: Props;

    render() {
        return (
            <div style={this.getStyle()}>
                {
                    this.props.children
                }
            </div>
        )
    }

    private getStyle = (): object => {
        let style: any = {};
        style.display = "flex";
        style.boxSizing = "border-box";
        style.padding = this.props.padding;
        style.margin = this.props.margin;
        style.width = this.props.width;
        style.height = this.props.height;
        style = {...style, ...OS(this.props.orientation)};
        style = {...style, ...GS("justifyContent", this.props.gravity)};
        style = {...style, ...GS("alignItems", this.props.layoutGravity)};
        style = {...style, ...this.props.style};
        return style;
    }

}

StackView.defaultProps = {
    width: "100%",
    height: "auto",
    orientation: Orientation.VERTICAL,
    gravity: Gravity.START,
    layoutGravity: Gravity.START,
    padding: 0,
    margin: 0,
    style: {}
};