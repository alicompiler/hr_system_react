export enum Gravity {
    CENTER,
    START,
    END,
    SPACE_BETWEEN,
    SPACE_AROUND,
}

export function toStyle(property: string, gravity: Gravity): object {
    let style = {};
    style[property] = getStyleValue(gravity);
    return style
}

function getStyleValue(gravity: Gravity): string {
    switch (gravity) {
        case Gravity.CENTER:
            return "center";
        case Gravity.START:
            return "flex-start";
        case Gravity.END:
            return "flex-end";
        case Gravity.SPACE_BETWEEN:
            return "space-between";
        case Gravity.SPACE_AROUND:
            return "space-around";
    }

    return ""
}