import * as React from "react";

interface Props {
    fluid?: boolean;
    maxWidth?: number;
    width?: number | string;
}

export default class Container extends React.Component<Props> {

    static defaultProps: Props;

    componentDidMount() {
        window.addEventListener("resize", this.onSizeDidChanged)
    }

    onSizeDidChanged = () => {
        
    };

    render() {
        return (
            <div style={this.getStyle()}>
                {this.props.children}
            </div>
        )
    }

    getStyle = () => {
        return {
            maxWidth: this.props.fluid ? "100%" : this.props.maxWidth,
            width: this.props.width,
            margin: this.props.fluid ? 0 : "auto"
        };
    };
}


Container.defaultProps = {
    maxWidth: 1024,
    fluid: false,
    width: "100%",
};

/*
 * Document
 *
 *
 */