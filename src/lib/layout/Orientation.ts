export enum Orientation {
    VERTICAL,
    HORIZONTAL
}


export function toSytle(orientation: Orientation): object {
    let style: any = {};
    style.flexDirection = orientation === Orientation.VERTICAL ? "column" : "row";
    return style;
}