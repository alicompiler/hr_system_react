import * as React from "react";
import If from "./If";

interface Props {
    condition: boolean;
}

export default class Else extends React.Component<Props> {

    render() {
        return <If condition={!this.props.condition}/>
    }

}