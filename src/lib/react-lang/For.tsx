import * as React from "react";
import {ReactNode} from "react";

interface Props {
    collection: any[];
    container?: ReactNode;
    renderItem: (item: any, index: number) => ReactNode;
}

export default class For extends React.Component<Props> {

    render() {
        return this.props.collection.map(this.props.renderItem)
    }

}