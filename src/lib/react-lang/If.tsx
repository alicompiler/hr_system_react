import * as React from "react";
import {ReactNode} from "react";

interface Props {
    condition: boolean | undefined;
    else?: ReactNode;
}

export default class If extends React.Component<Props> {

    static defaultProps: Props = {condition: false, else: null};

    render() {
        if (this.props.condition) {
            return this.props.children;
        } else {
            return this.props.else;
        }
    }

}