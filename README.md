# HR System

### System Requirements :
* ✔ Login/Logout
* ➜ Manage Employee
    * ✔ Display employees
        * ✔ as list
        * ✔ as cards
    * ✔ Search for employee
    * ➜ Create new employee
    * ✔ Delete existing employee
    * ✔ Edit employee info
    * ✔ Change employee login info
    * ✔ Display single employee with
      detailed information
    * ⧖ download employee's documents

* ➜ Leave-Requests
    * Send leave request
    * display all leave requests
    * display my/to manage leave requests
    * manage leave request
    * display single leave request
    

* ➜ Announcements : 
    * fetch recent announcements
    * write an announcement
    * fetch unwatched announcements
    * fetch announcements by date
    * fetch today announcements
    * fetch who watched an announcement
    * download file
    
##

##
 CHARACTERS:
 ⟳ ✔ ➜ ⧖